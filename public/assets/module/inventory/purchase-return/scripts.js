
$( document ).ready(function( $ ) {

    currentUrl = window.location.href;
    var segments = currentUrl.split( '/' );
    var lang = segments[3];

    $('.datePicker').datepicker({
        dateFormat: 'dd-mm-yy'
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });


    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('form#postForm').on('change', '.quantity', function (e) {

        var quantity = $(this).val();
        var id = $(this).attr('data-id');
        var max = $(this).attr('max');
        if(parseFloat(quantity) > parseFloat(max) ){
            $.MessageBox("Return quantity must be less or equal from the remaining quantity");
            $('#quantity-'+id).val('').focusin();
            return false;
        }

    });

    $(document).on('change', '.purchaseId', function () {
        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function () {
                location.reload();
            }
        });

    });

  /*  $(document).on('change', '.purchase', function () {
        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function (response) {
                obj = JSON.parse(response);
                $('#subTotal').html(obj['subTotal']);
                $('#totalVat').html(obj['totalVat']);
                $('#total').html(obj['total']);
                $('#discount').html(obj['discount']);
                $('#netTotal').html(obj['netTotal']);
                $('#paymentTotal').val(obj['paymentTotal']);
                $('#duable').html(obj['due']);
                if(obj['discountMode'] === "percent"){
                    $('#discountPercent').html(obj['discountPercent']+'%');
                }
            }
        });

    });*/

    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });

    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });

    $(document).on('opened', '.remodal', function () {

        var id = $.urlParam('process');
        var check = $.urlParam('check');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url, function(){
            formCommonProcess();
            if(check === 'edit'){
                formEditSubmitProcess();
            }else{
                formSubmitProcess();
            }
        });
    });

});



