$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/admin/location/data-table", // ajax source
            'data': function(data){

                var name = $('#name').val();
                var district = $('#district').val();
                data.name = name;
                data.district = district;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'name' },
            { "name": 'district' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [3], "sClass":  "text-center"}
        ],
        "order": [
            [2, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 3,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#district').keyup(function(){
        dataTable.draw();
    });


});

