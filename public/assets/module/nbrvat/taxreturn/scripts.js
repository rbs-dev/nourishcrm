
$( document ).ready(function( $ ) {

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)')
            .exec(window.location.search);
        return (results !== null) ? results[1] || 0 : false;
    }

    $(document).on('opened', '.remodal', function () {
        var id = $.urlParam('process');
        var url = document.getElementById(id).getAttribute("data-action");
        $('#modal-container').load(url);
    });

    $('[data-remodal-id=modal]').remodal({
        modifier: 'with-red-theme',
        closeOnOutsideClick: true
    });


    $(document).on('change', '.action', function () {

        $.ajax({
            url:  $('form#postForm').attr('data-action'),
            type: $('form#postForm').attr('method'),
            data: new FormData($('form#postForm')[0]),
            processData: false,
            contentType: false,
            success: function (response) {
                location.reload();
            }

    });

    });

    $(document).on('click', '#addProduct', function() {

        var productId = $('#productId').val();
        var quantity = $('#quantity').val();
        var url = $('#addProduct').attr('data-action');
        if(productId === ''){
            $('#product').select2('open');
            return false;
        }
        if(quantity === ''){
            $.MessageBox("Please enter issue quantity");
            $('#quantity').focus();
            return false;
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: 'productId='+productId+'&quantity='+quantity,
            success: function (response) {
                $('#product').select2('open');
                $('#quantity').val('');
                $('#purchasePrice').val('');
                $('#remaining').val('');
                setTimeout( explode, 2000);
            }
        })
    });


    $(document).on('keypress', '.input', function (e) {

        if (e.keyCode === 13  ){
            var inputs = $(this).parents("form").eq(0).find("input,select");
            console.log(inputs);
            var idx = inputs.index(this);
            if (idx === inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {

                case 'product':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    $('#addProduct').click();
                    $('#product').select2('open');
                    break;
            }
            return false;
        }
    });


    var explode = function AutoReload()
    {
        $('#entityDatatable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    }

    $("#allCheck").change(function(){  //"select all" change
        $(".process").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
        $('#check-btn').toggle();
    });
    $(document).on('click', '#checkBtn', function(e) {
        var favorite = [];
        $.each($("input[name='process']:checked"), function(){
            favorite.push($(this).val());
        });
        ids = favorite.join(", ");
        var url = $('#checkBtn').attr('data-action');
        $.get( url , { ids:ids } )
            .done(function( data ) {
                dt = $(this).dataTable();
                dt.fnDraw();
            });

    });


});




