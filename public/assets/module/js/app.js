$(document).ready(function() {

    $('.select2-multiple').select2();
    $('.select2').select2();
    $('.mobileLocal').mask("00000-000000", {placeholder: "_____-______"});
    $('.phoneLocal').mask("0-00000000", {placeholder: "_____-______"});

    $('.form-submit').submit(function(){
        $("button[type='submit']", this)
            .html("Please Wait...")
            .attr('disabled', 'disabled');
        return true;
    });

    $("#startMonth, #endMonth , #startDate, #endDate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "-90:+00",
        showOn: "both",
        showButtonPanel: true,
        buttonImage: "/assets/images/icon-calendar-green.png",
        buttonImageOnly: true
       /* onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }*/
    });

    $('.datePicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "2020:" + new Date().getFullYear(),
        showOn: "both",
        showButtonPanel: true,
        buttonImage: "/assets/images/icon-calendar-green.png",
        buttonImageOnly: true
    });

    $('.date-picker').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $( ".dateCalendar" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "mm-yy",
        yearRange: "-50:+00",
        showOn: "both",
        showButtonPanel: true,
        buttonImage: "/assets/images/icon-calendar-green.png",
        buttonImageOnly: true,
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });

    $('.timePicker').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });

    $('.vat-report-preview').slimScroll({
        height: '350px',
        width: '100%'
    });


    $('.checkboxToggle').bootstrapToggle();

    $('.multi-select2').multiSelect({ selectableOptgroup: true });

    $('#optgroup').multiSelect({ selectableOptgroup: true });

    $(document).on('keypress', '.form-control', function(e) {
        value = $(this).val();
        $(e.target).closest('.form-group').find('.control-label').removeClass('required');
    });

    $(document).on('click', '.amount', function() {
        $(this).val("");
    });


    var explode = function AutoReload()
    {
        $('#entityDatatable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    }

    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }


    $(document).on('change', ".custom-file-input", function(e) {
        var id = $(this).attr('id');
        var fileName = document.getElementById(id).files[0].name;
        for (var i = 0; i < $("#"+id).get(0).files.length; ++i) {
            var file1=$("#"+id).get(0).files[i].name;
            if(file1){
                var file_size=$("#"+id).get(0).files[i].size;
                if(file_size < 2097152){
                    var ext = file1.split('.').pop().toLowerCase();
                    if($.inArray(ext,['jpg','jpeg','gif','png','pdf','xls','xlsx'])===-1){
                        alert("Invalid file extension");
                        return false;
                    }
                }else{
                    alert("Screenshot size is too large.");
                    return false;
                }
            }else{
                alert("fill all fields..");
                return false;
            }
        }
        var nextSibling = e.target.nextElementSibling;
        nextSibling.innerText = fileName;
    });

    $(document).on('change', '.transaction-method', function() {

        var transactionMethod = $(this).val();
        if(transactionMethod === "bank" ){
            $('.mobile-hide').hide(500).removeClass('show-method');
            $('.bank-hide').slideDown("slow");
        }else if(transactionMethod === 'mobile' ){
            $('.bank-hide').hide(500).removeClass('show-method');
            $('.mobile-hide').slideDown("slow");
        }else{
            $('.bank-hide,.mobile-hide').hide(500).removeClass('show-method');
        }

    });

    $(document).on('click',".remove", function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function( data ) {
                $(event.target).closest('tr').hide();
            });
        });
    });

    $(document).on('click',".removeWithMessage", function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function( data ) {
                $('.messages').append('');
                if(data.status==200){
                    $(event.target).closest('tr').hide();
                    var html = '<div class="alert alert-dismissible alert-success fade in show" role="alert">\n' +
                        '                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '                        <span aria-hidden="true">×</span>\n' +
                        '                    </button>\n' +
                        '                    Customer deleted successfully!\n' +
                        '                </div>';
                    $('.messages').append(html);
                }else {
                    var html = '<div class="alert alert-dismissible alert-danger fade in show" role="alert">\n' +
                        '                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '                        <span aria-hidden="true">×</span>\n' +
                        '                    </button>\n' +
                        '                    Ops! Something wrong.\n' +
                        '                </div>';
                    $('.messages').append(html);
                }
            });
        });
    });

    $(document).on('click',".removeReload", function (event) {

        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function(data) {
                location.reload();
            });
        });

    });

    $(document).on('click',".approveProcess", function (event) {

        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to continue this process?"
        }).done(function(){
            $.get(url, function(data) {
                location.reload();
            });
        });

    });

    $(document).on('click',".approve", function (event) {

        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to continue this process?"
        }).done(function(){
            $.get(url, function( data ) {
                setTimeout( explode, 1000);
            });
        });

    });

    $(document).on('click',".salesMatrix", function (event) {

        if ($(this).is(':checked')) {
            var status = 'true';
        } else {
            var status = 'false';
        }
        var url = $(this).attr('data-action');
        $.get(url,{'status':status}, function( data ) {
            setTimeout( explode, 1000);
        });

    });

    $(document).ready(function () {
        $('.customerDevelopment').bind('keyup mouseup', function () {
            let mark  = $(this).val();
            let attributeId = $(this).attr('data-id');
            let url = $(this).attr('data-action');

            $.ajax({
                url: url,
                type: 'get',
                data: {mark},
                success: function (response) {
                    console.log(response)
                    $('#markDistribution-'+attributeId).html(response);
                },
            });

        })
    })

//KPI markchart format attribute ordering
    $(document).on('change',".sortingValue", function (event) {
        var element = $(this);
        var position  = $(this).val();
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.get(url,{'position':position}, function( data ) {
            element.val(data.sotring);
        });

    });

    var explode = function AutoReload()
    {
        $('#entityDatatable').each(function() {
            dt = $(this).dataTable();
            dt.fnDraw();
        });
    }

    $("#allCheck").change(function(){  //"select all" change
        $(".process").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
        $('#check-btn').toggle();
    });
    $(document).on('click', '#checkBtn', function(e) {
        var favorite = [];
        $.each($("input[name='process']:checked"), function(){
            favorite.push($(this).val());
        });
        ids = favorite.join(", ");
        var url = $('#checkBtn').attr('data-action');
        $.get( url , { ids:ids } )
            .done(function( data ) {
                dt = $(this).dataTable();
                dt.fnDraw();
            });

    });

    var count = 0;

    $('.addmore').click(function(){

        var $el = $(this);
        var $cloneBlock = $('#clone-block');
        var $clone = $cloneBlock.find('.clone:eq(0)').clone();
        $clone.find('[id]').each(function(){this.id+='someotherpart'});
        $clone.find(':text,textarea' ).val("");
        $clone.attr('id', "added"+(++count));
        $clone.find('.row-remove').removeClass('hidden');
        $cloneBlock.prepend($clone);

    });

    $('#clone-block').on('click', '.row-remove', function(){
        $(this).closest('.clone').remove();
    });

    $(document).on('click', '.meta-remove', function(){
         var id = $(this).attr('data-id');
         var url = $(this).attr('data-action');
         $.MessageBox({
             buttonFail  : "No",
             buttonDone  : "Yes",
             message     : "Are you sure want to delete this record?"
         }).done(function(){
             $.get(url, function( data ) {
                 $('remove-'+id).remove();
                 $(this).closest('.clone-remove').remove();
             });
         });
    });

    // Sortable Code
    var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    };

    $(".table-sortable tbody").sortable({
        helper: fixHelperModified
    }).disableSelection();

    $(".table-sortable thead").disableSelection();

});
