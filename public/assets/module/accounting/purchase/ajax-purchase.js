$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/inventory/item/data-table", // ajax source
            'data': function(data){

                var productType = $('#productType').val();
                var groupName = $('#groupName').val();
                var categoryName = $('#categoryName').val();
                var name = $('#name').val();
                var unitName = $('#unitName').val();
                var barcode = $('#barcode').val();
                var sku = $('#sku').val();
                var purchasePrice = $('#purchasePrice').val();
                var salesPrice = $('#salesPrice').val();
                var openingQuantity = $('#openingQuantity').val();
                var purchaseQuantity = $('#purchaseQuantity').val();
                var purchaseReturnQuantity = $('#purchaseReturnQuantity').val();
                var salesQuantity = $('#salesQuantity').val();
                var salesReturnQuantity = $('#salesReturnQuantity').val();
                var damageQuantity = $('#damageQuantity').val();
                var ongoingQuantity = $('#ongoingQuantity').val();
                var remainingQuantity = $('#remainingQuantity').val();

                // Append to data
                //  data._token = CSRF_TOKEN;

                data.productType = productType;
                data.groupName = groupName;
                data.categoryName = categoryName;
                data.name = name;
                data.unitName = unitName;
                data.barcode = barcode;
                data.sku = sku;
                data.purchasePrice = purchasePrice;
                data.salesPrice = salesPrice;
                data.openingQuantity = openingQuantity;
                data.purchaseQuantity = purchaseQuantity;
                data.purchaseReturnQuantity = purchaseReturnQuantity;
                data.salesQuantity = salesQuantity;
                data.salesReturnQuantity = salesReturnQuantity;
                data.damageQuantity = damageQuantity;
                data.ongoingQuantity = ongoingQuantity;
                data.remainingQuantity = remainingQuantity;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'productType' },
            { "name": 'groupName' },
            { "name": 'categoryName' },
            { "name": 'name' },
            { "name": 'unitName' },
            { "name": 'barcode' },
            { "name": 'sku' },
            { "name": 'purchasePrice' },
            { "name": 'salesPrice' },
            { "name": 'openingQuantity' },
            { "name": 'purchaseQuantity' },
            { "name": 'purchaseReturnQuantity' },
            { "name": 'salesQuantity' },
            { "name": 'salesReturnQuantity' },
            { "name": 'damageQuantity' },
            { "name": 'ongoingQuantity' },
            { "name": 'remainingQuantity' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [6], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#category').keyup(function(){
        dataTable.draw();
    });
    $('#barcode').keyup(function(){
        dataTable.draw();
    });
    $('#productType').change(function(){
        dataTable.draw();
    });
    $('#groupName').change(function(){
        dataTable.draw();
    });



});

