$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/core/user/data-table", // ajax source
            'data': function(data){

                var userGroup = $('#userGroup').val();
                var name = $('#name').val();
                var username = $('#username').val();
                var designation = $('#designation').val();
                var mobile = $('#mobile').val();
                var email = $('#email').val();
                var department = $('#department').val();
                var branch = $('#branch').val();
                var status = $('#status').val();

                data.userGroup = userGroup;
                data.name = name;
                data.username = username;
                data.designation = designation;
                data.mobile = mobile;
                data.email = email;
                data.branch = branch;
                data.department = department;
                data.status = status;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'userGroup'},
            { "name": 'name' },
            { "name": 'username' },
            { "name": 'designation' },
            { "name": 'mobile' },
            { "name": 'email' },
            { "name": 'department' },
            { "name": 'branch' },
            { "name": 'status' },
            { "name": 'action' }

        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }],
        "buttons": [
            {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            },
            {
                text: 'Reload',
                className: 'btn default',
                action: function ( e, dt, node, config ) {
                    dt.ajax.reload();
                    alert('Datatable reloaded!');
                }
            }
        ]
    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#username').change(function(){
        dataTable.draw();
    });

    $('#mobile').keyup(function(){
        dataTable.draw();
    });

    $('#email').keyup(function(){
        dataTable.draw();
    });

    $('#status').change(function(){
        dataTable.draw();
    });

    $('#department').change(function(){
        dataTable.draw();
    });

    $('#designation').change(function(){
        dataTable.draw();
    });

    $('#branch').change(function(){
        dataTable.draw();
    });

    $("#csvBtn").on("click", function() {
        dataTable.button( '.buttons-csv' ).trigger();
    });

    $("#printBtn").on("click", function() {
        dataTable.button( '.buttons-print' ).trigger();
    });

    $("#excelBtn").on("click", function() {
        dataTable.button( '.buttons-excel' ).trigger();
    });

    $("#pdfBtn").on("click", function() {
        dataTable.button( '.buttons-pdf' ).trigger();
    });

});

