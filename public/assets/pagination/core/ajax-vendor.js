$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/core/vendor/data-table", // ajax source
            'data': function(data){

                var name = $('#name').val();
                var businessType = $('#businessType').val();
                var vendorType = $('#vendorType').val();
                var company = $('#company').val();
                var mobile = $('#mobile').val();
                var registrationNo = $('#registrationNo').val();
                var binNo = $('#binNo').val();
                var vatRegistrationNo = $('#vatRegistrationNo').val();

                // Append to data
                //  data._token = CSRF_TOKEN;
                data.name = name;
                data.businessType = businessType;
                data.vendorType = vendorType;
                data.company = company;
                data.mobile = mobile;
                data.registrationNo = registrationNo;
                data.binNo = binNo;
                data.vatRegistrationNo = vatRegistrationNo;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'name' },
            { "name": 'businessType' },
            { "name": 'vendorType' },
            { "name": 'company' },
            { "name": 'mobile' },
            { "name": 'registrationNo' },
            { "name": 'binNo' },
            { "name": 'vatRegistrationNo' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [7], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 7,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#businessType').change(function(){
        dataTable.draw();
    });

     $('#vendorType').change(function(){
        dataTable.draw();
    });

    $('#companyName').keyup(function(){
        dataTable.draw();
    });

    $('#mobile').keyup(function(){
        dataTable.draw();
    });

    $('#email').change(function(){
        dataTable.draw();
    });

});

