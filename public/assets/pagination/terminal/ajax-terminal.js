$(document).ready(function () {

    url = window.location.href;
    var segments = url.split( '/' );
    var lang = segments[3];

  //  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var dataTable = $('#entityDatatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': false,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": "/"+lang+"/admin/terminal/data-table", // ajax source
            'data': function(data){
                var created = $('#created').val();
                var organizationName = $('#organizationName').val();
                var name = $('#name').val();
                var mobile = $('#mobile').val();
                var location = $('#location').val();
                var mainApp = $('#mainApp').val();
                var syndicate = $('#syndicate').val();
                var status = $('#status').val();

                data.created = created;
                data.organizationName = organizationName;
                data.name = name;
                data.mobile = mobile;
                data.location = location;
                data.mainApp = mainApp;
                data.syndicate = syndicate;
                data.status = status;
            }
        },
        'columns': [
            { "name": 'id' },
            { "name": 'created' },
            { "organizationName": 'organizationName' },
            { "name": 'name' },
            { "name": 'mobile' },
            { "name": 'location' },
            { "name": 'mainApp' },
            { "name": 'syndicate' },
            { "name": 'status' },
            { "name": 'action' }

        ],
        "aoColumnDefs" : [
            {"aTargets" : [9], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets":9,
            "orderable": false
        },
        {
            "targets": 0,
            "orderable": false
        }]

    });

    $('#created').change(function(){
        dataTable.draw();
    });
    $('#organizationName').keyup(function(){
        dataTable.draw();
    });
    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#mobile').keyup(function(){
        dataTable.draw();
    });

    $('#location').keyup(function(){
        dataTable.draw();
    });

    $('#mainApp').change(function(){
        dataTable.draw();
    });

    $('#syndicate').change(function(){
        dataTable.draw();
    });

    $('#status').change(function(){
        dataTable.draw();
    });



});

