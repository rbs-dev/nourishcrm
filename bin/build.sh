#!/bin/sh


echo "Removing old cache if any"
rm -f composer.lock
rm -rf var/cache/*
rm -rf var/log/*

echo "Dumping assets"
bin/console  assets:install --symlink --relative
bin/console  assetic:dump --env=prod --no-debug

echo "Make directory writtable"
chmod -R 0777 var/cache var/log

composer update