<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Core\Setting;
use App\Entity\Core\SettingType;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\KpiBundle\Entity\EmployeeBoard;
use Terminalbd\KpiBundle\Entity\EmployeeBoardAttribute;


/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * @Route("/")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class DashboardController extends AbstractController
{
    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="dashboard")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $year = date("Y",strtotime("-1 year"));
        $currentMonth = date('m');
        if((int)$currentMonth>=3){
            $year = date("Y");
        }

        $selfKpi = $this->getDoctrine()->getRepository(EmployeeBoard::class)->findBy(['employee' => $this->getUser()]);
        $teamMembersKpi = $this->getDoctrine()->getRepository(EmployeeBoard::class)->getTeamMembersListByYear($this->getUser(), $year);
        $list = [];
        foreach ($selfKpi as $board) {
            if ($board->getApprovedBy() === null){
                $list['in-progress'][] = $board;
            }else{
                $list['approved'][] = $board;

            }
        }
        krsort($list);

        return $this->render('default/homepage.html.twig',[
            'selfKpi' => $list,
            'teamMembersKpi' => $teamMembersKpi,
            'selectedYear' => $year,
            'year' => $year,
        ]);
    }

    /**
     * @Route("{id}/{year}/team-member-grade-details/{mode}",defaults={"mode" = null}, name="team_member_grade_details_modal", options={"expose" = true})
     * @param User $employee
     * @param $year
     * @param $mode
     * @param Request $request
     * @return JsonResponse
     */
    public function teamMemberGradeDetailsModal(User $employee, $year, $mode, Request $request)
    {
        $grades = $this->getDoctrine()->getRepository(EmployeeBoard::class)->getTeamMemberGradeDetailsByYear($employee, $year);

        if ($mode === 'excel'){
            $html = $this->renderView('dashboard/team-member-grade-details-excel.html.twig', [
                'employee' => $employee,
                'grades' => $grades,
                'year' => $year,

            ]);

            $fileName = 'team-member-grade-details'. '_' . time() . '.xls';


            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        $html = $this->renderView('inc/_team-member-grade-details-modal.html.twig',[
            'employee' => $employee,
            'grades' => $grades,
            'year' => $year,
        ]);
        return new JsonResponse(['html' => $html]);
    }

    /**
     * @Route("{id}/{year}/team-member-mark-details/{mode}", defaults={"mode" = null}, name="team_member_mark_details", options={"expose" = true})
     * @param User $employee
     * @param $year
     * @param $mode
     * @param Request $request
     * @return Response
     */
    public function teamMemberMarkDetails(User $employee, $year, $mode, Request $request)
    {
        $marks = $this->getDoctrine()->getRepository(EmployeeBoardAttribute::class)->getTeamMemberMarksByYear($employee, $year);
        if ($mode === 'excel'){
            $html = $this->renderView('dashboard/team-member-mark-details-excel.html.twig', [
                'employee' => $employee,
                'year' => $year,
                'marks' => $marks,

            ]);

            $fileName = $request->get('_route') . '_' . time() . '.xls';


            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('dashboard/team-member-mark-details.html.twig',[
            'employee' => $employee,
            'year' => $year,
            'marks' => $marks,
        ]);
    }

    /**
     * @Route("team-member-details/refresh",methods={"POST"}, name="team_member_details_refresh", options={"expose" = true})
     * @param Request $request
     * @return JsonResponse
     */
    public function teamMemberDetailsRefresh(Request $request)
    {

        $year = $request->request->get('year');

        $teamMembersKpi = $this->getDoctrine()->getRepository(EmployeeBoard::class)->getTeamMembersListByYear($this->getUser(), $year);

        ksort($teamMembersKpi);
        $html = $this->renderView('inc/_team-member-details-refresh.html.twig',[
            'teamMembersKpi' => $teamMembersKpi,
            'year' => $year,
        ]);
        return new JsonResponse(['html' => $html]);

    }

    /**
     * @Route("{year}/team-member-details/excel", name="team_member_kpi_details_excel", options={"expose" = true})
     * @param Request $request
     * @return Response
     */
    public function teamMemberDetailsExcel(Request $request, $year)
    {

        $teamMembersKpi = $this->getDoctrine()->getRepository(EmployeeBoard::class)->getTeamMembersListByYear($this->getUser(), $year);

        return $this->render('dashboard/team-member-kpi-details-excel.html.twig',[
            'teamMembersKpi' => $teamMembersKpi,
            'year' => $year,
        ]);

    }


}
