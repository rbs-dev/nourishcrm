<?php


namespace App\Controller;


use App\Service\DatabaseBackup;
use App\Service\SchemaTruncate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DataCleanController
 * @package App\Controller
 * @Route("/data-clean", name="data_clean")
 * @Security("is_granted('ROLE_DEVELOPER')")
 */
class DataCleanController extends AbstractController
{
    /**
     * @Route("/crm", name="_crm")
     * @param Request $request
     * @param SchemaTruncate $truncate
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cleanCrmData(Request $request, SchemaTruncate $truncate)
    {
        $schemas = ['crm_agent_upgradation_report', 'crm_antibiotic_free_farm', 'crm_broiler_life_cycle', 'crm_cattle_farm_visit_details', 'crm_cattle_life_cycle', 'crm_cattle_life_cycle_details', 'crm_chick_life_cycle', 'crm_chick_life_cycle_details', 'crm_company_wise_feed_sale', 'crm_complain_different_product', 'crm_complain_different_product_details', 'crm_cost_benefit_analysis_for_less_costing_farm', 'crm_daily_chick_price', 'crm_daily_chick_price_details', 'crm_disease_mapping', 'crm_expence_purpose', 'crm_expence_vehicle', 'crm_expense', 'crm_expense_conveyance_details', 'crm_farmer_touch_report', 'crm_farmer_training_report', 'crm_farmer_training_report_details', 'crm_fcr', 'crm_fcr_details', 'crm_fcr_different_companies', 'crm_fish_company_species_wise_average_fcr', 'crm_fish_company_species_wise_average_fcr_details', 'crm_fish_feed_complain', 'crm_fish_life_cycle', 'crm_fish_life_cycle_detail_species', 'crm_fish_life_cycle_details', 'crm_fish_sales_price', 'crm_fish_tilapia_fry_sales', 'crm_lab_services', 'crm_layer_life_cycle', 'crm_layer_life_cycle_details', 'crm_layer_performance_details', 'crm_poultry_meat_egg_price', 'crm_visit', 'crm_visit_complain', 'crm_visit_complain_details', 'crm_visit_details'];

        $truncate->runQuery($request, $schemas, 'CRM');

        return $this->redirectToRoute("dashboard");
    }

    /**
     * @Route("/kpi", name="_kpi")
     * @param Request $request
     * @param SchemaTruncate $truncate
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cleanKpiData(Request $request, SchemaTruncate $truncate)
    {
        $schemas = ['kpi_agent_category','kpi_agent_doc_sale_collection','kpi_agent_doc_sale_collection_custom_format','kpi_agent_order','kpi_agent_order_check','kpi_agent_outstanding','kpi_agent_outstanding_custom_format','kpi_agent_sales_growth','kpi_board_attribute','kpi_board_subattribute','kpi_district_order','kpi_employee_board','kpi_import_document','kpi_location_sales_target','kpi_setup','kpi_setup_matrix'];

        $truncate->runQuery($request, $schemas, 'KPI');

        return $this->redirectToRoute("dashboard");

    }

    /**
     * @Route("/br", name="_br")
     * @param Request $request
     * @param SchemaTruncate $truncate
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cleanBrData(Request $request, SchemaTruncate $truncate)
    {
        $schemas = ['br_bank_transaction','br_import_file','br_reconciliation','br_sales_payment'];

        $truncate->runQuery($request, $schemas, 'BR');
        return $this->redirectToRoute("dashboard");

    }

    /**
     * @Route("/sms", name="_sms")
     * @param Request $request
     * @param SchemaTruncate $truncate
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cleanSmsData(Request $request, SchemaTruncate $truncate)
    {
        $schemas = ['sales_order_delivery','sales_order_delivery_amount','sales_order_delivery_batch','sms_campaign','sms_log'];
        $truncate->runQuery($request, $schemas, 'SMS');
        return $this->redirectToRoute("dashboard");

    }

    /**
     * @Route("/app-sync", name="_app_sync")
     * @param Request $request
     * @param SchemaTruncate $truncate
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cleanAppSyncData(Request $request, SchemaTruncate $truncate)
    {
        $schemas = ['crm_api','crm_api_details'];
        $truncate->runQuery($request, $schemas, 'APP SYNC');
        return $this->redirectToRoute("dashboard");

    }

    /**
     * @Route("/database-backup", name="_database_backup")
     * @param DatabaseBackup $backup
     * @param ParameterBagInterface $parameterBag
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function databaseBackup(DatabaseBackup $backup, ParameterBagInterface $parameterBag)
    {
        $host = $this->getDoctrine()->getConnection()->getHost();
        $user = $this->getDoctrine()->getConnection()->getUsername();
        $pass = $this->getDoctrine()->getConnection()->getPassword();
        $database = $this->getDoctrine()->getConnection()->getDatabase();
        $path = $parameterBag->get('projectRoot') . '/../DB_backup/';

        if (!file_exists($parameterBag->get('projectRoot') . '/../DB_backup/')){
           mkdir($parameterBag->get('projectRoot') . '/../DB_backup/', 0777);
        }

//        $backup->backup($host,$user,$pass,$database,'/var/www/html/');
        $fileName = $backup->backup($host,$user,$pass,$database,$path);

        if ($fileName){
            shell_exec("sh " . $parameterBag->get('projectRoot') . "/bin/run.sh"); //clear cache

            return $this->file($path.$fileName);

//            $response = new BinaryFileResponse($path . $fileName);
//            $response->headers->set('Content-Type', 'text/plain');
//            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);
//
//            return $response;
        }

        $this->addFlash('warning', 'Something wrong!');
        return $this->redirectToRoute('homepage');


    }



}