<?php

namespace App\Controller;

use App\Entity\Admin\Location;
use App\Entity\SalesOrderDelivery;
use App\Entity\SalesOrderDeliveryAmount;
use App\Entity\SalesOrderDeliveryBatch;
use App\Entity\User;
use App\Repository\SalesOrderDeliveryBatchRepository;
use App\Repository\SalesOrderDeliveryRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SalesOrderDeliveryController extends AbstractController
{
    /**
     * @Route("/sales/order/delivery/{orderDeliveryStatus}", name="sales_order_delivery", options={"expose"=true})
     * @param SalesOrderDeliveryBatchRepository $deliveryBatchRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param $orderDeliveryStatus
     * @return Response
     */
    public function index(SalesOrderDeliveryBatchRepository $deliveryBatchRepository, PaginatorInterface $paginator, Request $request, $orderDeliveryStatus='delivered'): Response
    {
        if(!in_array($orderDeliveryStatus, ['delivered', 'pending'])){
            $this->addFlash('error', 'This status is not supported.');
            return $this->redirectToRoute('sales_order_delivery');
        }
        $records = $deliveryBatchRepository->findBy(['orderDeliveryStatus'=>strtoupper($orderDeliveryStatus)]);
        $records = $paginator->paginate($records,$request->query->getInt('page', 1), /*page number*/
            25 /*limit per page*/);
        return $this->render('smsCenter/sales-order-delivery.html.twig',[
            'records' => $records,
            'orderDeliveryStatus'=>$orderDeliveryStatus
        ]);
    }

    /**
     * @Route("sales/order/sync/{orderDeliveryStatus}", name="sales_order_sync_api")
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @param HttpClientInterface $client
     * @param SalesOrderDeliveryBatchRepository $deliveryBatchRepository
     * @param $orderDeliveryStatus
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function syncSalesOrder(Request $request, ParameterBagInterface $parameterBag, HttpClientInterface $client, SalesOrderDeliveryBatchRepository $deliveryBatchRepository, $orderDeliveryStatus)
    {
        if(!in_array($orderDeliveryStatus, ['delivered', 'pending'])){
            $this->addFlash('error', 'This status is not supported.');
            return $this->redirectToRoute('sales_order_delivery');
        }

        set_time_limit(0);
        ini_set('memory_limit', '1024M');

        $requestDate = new \DateTime('now');
        $requestDate =  $requestDate->format('Y-m-d');
        if ($request->getMethod() == 'POST' && null != $request->request->get('request-date')){
            $requestDate = $request->request->get('request-date');
            $requestDate = new \DateTime($requestDate);
            $requestDate = $requestDate->format('Y-m-d');
        }
//        $findSalesOrderBatch = $deliveryBatchRepository->findBy(['syncDate' => new \DateTime($requestDate)]);
        $findSalesOrderBatch = $deliveryBatchRepository->getSalesOrderDeliveryBatchByDateRange($requestDate, $orderDeliveryStatus);
        if ($findSalesOrderBatch){
            $this->addFlash('error', 'This Date already synchronized!');
            return $this->redirectToRoute('sales_order_delivery');

        }



        if($orderDeliveryStatus=='pending'){
            $apiEndPoint = $parameterBag->get('salesDomain') . '/api/orders/pending/delivery';
        }else{
            $apiEndPoint = $parameterBag->get('salesDomain') . '/api/orders';
        }

        $response = $client->request('GET', $apiEndPoint, [
            'headers' => [
                'X-API-KEY' => $parameterBag->get('crm_api_key')
            ],
            'query' => [
                'request_date' => $requestDate
            ]
        ]);

        $records = $response->toArray();

        if ($records){
            $em = $this->getDoctrine()->getManager();
            $salesOrderBatch = new SalesOrderDeliveryBatch();
            $salesOrderBatch->setOrderDeliveryStatus(strtoupper($orderDeliveryStatus));
            $salesOrderBatch->setSyncDate(new \DateTime($requestDate . ' ' . date('H:i:s')));
            $em->persist($salesOrderBatch);
            $em->flush();

            foreach ($records as $record) {
                $findDistrict = $this->getDoctrine()->getRepository(Location::class)->findOneBy(['oldId' => $record['districtId']]);
                $findRegion = $this->getDoctrine()->getRepository(Location::class)->findOneBy(['oldId' => $record['regionId']]);

                $salesOrder = new SalesOrderDelivery();
                $salesOrder->setBatch($salesOrderBatch);
                $salesOrder->setCreatedAt(new \DateTimeImmutable('now'));
                $salesOrder->setStatus(false);
                $salesOrder->setItemId($record['itemId']);
                $salesOrder->setItemName($record['itemName']);
                $salesOrder->setDistrict($findDistrict);
                $salesOrder->setRegion($findRegion);
                $salesOrder->setTotalQuantity($record['totalQuantity']);
                $em->persist($salesOrder);
                $em->flush();
            }

            $managers = $this->getDoctrine()->getRepository(User::class)->getRegionalManagers();
            foreach ($managers as $userId => $manager) {
                $orders = $this->getDoctrine()->getRepository(SalesOrderDelivery::class)->getOrderByDistricts($manager['districtId'], $salesOrderBatch);
                if ($orders){
                    $totalQuantity = 0;
                    foreach ($orders as $itemName => $quantity) {
                        $totalQuantity += $quantity;
                    }
                    $sql = "INSERT INTO `sales_order_delivery_amount`(`batch_id`, `manager_id`, `total_amount`, `status`, `created_at`) VALUES (:batch_id, :manager_id, :total_amount, :status, :created_at)";
                    $stmt = $this->getDoctrine()->getConnection()->prepare($sql);
                    $stmt->bindValue('batch_id', $salesOrderBatch->getId());
                    $stmt->bindValue('manager_id', $manager['id']);
                    $stmt->bindValue('total_amount', $totalQuantity);
                    $stmt->bindValue('status', 0);
                    $stmt->bindValue('created_at', (new \DateTime('now'))->format('Y-m-d'));

                    $stmt->execute();
                }
            }

            $this->addFlash('success', 'Synchronisation completed!');
        }else{
            $this->addFlash('success', 'Nothing found to synchronize!');
        }
        return $this->redirectToRoute('sales_order_delivery', ['orderDeliveryStatus'=>$orderDeliveryStatus]);

    }

    /**
     * @Route("/sales/order/delivery/{id}/delete", name="delete_sales_order_delivery")
     * @param SalesOrderDeliveryBatch $batch
     * @return JsonResponse
     */
    public function deleteSalesOrderDeliveryBatch(SalesOrderDeliveryBatch $batch){
        $status = $this->getDoctrine()->getRepository(SalesOrderDeliveryBatch::class)->deleteRecord($batch);
        if ($status === true){
            return new JsonResponse([
                'status' => 200,
                'message' => 'success'
            ]);
        }else{
            return new JsonResponse([
                'status' => 404,
                'message' => 'failed'
            ]);

        }
    }

    /**
     * @Route("/{batch}/show/regional-managers/amount", name="show_regional_managers_amount", options={"expose"=true})
     * @param SalesOrderDeliveryBatch $batch
     * @param Request $request
     * @return JsonResponse
     */
    public function showRegionalManagersAmount(SalesOrderDeliveryBatch $batch, Request $request)
    {
        $mode = $request->query->get('mode');

        $salesDoSmsAdmins = $this->getDoctrine()->getRepository(User::class)->getSalesDoSmsAdmins();
        $records = $this->getDoctrine()->getRepository(SalesOrderDeliveryAmount::class)->findBy(['batch' => $batch]);
        $totalAmount = $this->getDoctrine()->getRepository(SalesOrderDelivery::class)->getTotalAmount($batch);

        if ($mode === 'pdf'){

            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial, sans-serif');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('smsCenter/sales-order-delivery-pdf.html.twig',[
                'batch' => $batch,
                'records' => $records,
                'salesDoSmsAdmins' => $salesDoSmsAdmins,
                'totalAmount' => $totalAmount,
            ]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
            $dompdf->setPaper('A4', 'portrait');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser (force download)
            $dompdf->stream($request->get('_route') . ".pdf", [
                "Attachment" => true
            ]);
            exit();
        }

        $html = $this->renderView('smsCenter/inc/_show-regional-managers-amount-modal.twig',[
            'batch' => $batch,
            'records' => $records,
            'salesDoSmsAdmins' => $salesDoSmsAdmins,
            'totalAmount' => $totalAmount,
        ]);
        return new JsonResponse(array('html'=>$html));
    }

    /**
     * @Route("/{orderDeliveryStatus}/regional-managers/amount/details", name="show_regional_managers_amount_details", options={"expose"=true})
     * @param Request $request
     * @param ParameterBagInterface $parameterBag
     * @param HttpClientInterface $client
     * @param string $orderDeliveryStatus
     * @return Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function salesOrderDeliveryDetails(Request $request, ParameterBagInterface $parameterBag, HttpClientInterface $client, $orderDeliveryStatus='delivered')
    {
        if(!in_array($orderDeliveryStatus, ['delivered', 'pending'])){
            $this->addFlash('error', 'This status is not supported.');
            return $this->redirectToRoute('sales_order_delivery', ['orderDeliveryStatus'=>$orderDeliveryStatus]);
        }
        set_time_limit(0);
        ini_set('memory_limit', '5122M');

        $mode = $request->query->get('mode');
        $requestDate = $request->query->get('request_date');
        $districtId = $request->query->get('district_id');



        if($orderDeliveryStatus=='pending'){
            $apiEndPoint = $parameterBag->get('salesDomain') . '/api/order/pending/delivery/details';
        }else{
            $apiEndPoint = $parameterBag->get('salesDomain') . '/api/order/details';
        }


        $response = $client->request('GET', $apiEndPoint, [
            'headers' => [
                'X-API-KEY' => $parameterBag->get('crm_api_key')
            ],
            'query' => [
                'request_date' => $requestDate,
                'district_id' => $districtId
            ]
        ]);

        $records = $response->toArray();

        if ($mode === 'pdf'){

            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial, sans-serif');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('smsCenter/sales-order-delivery-details-pdf.html.twig',[
                'records' => $records,
                'requestDate' => $requestDate,
                'districtId' => $districtId,
            ]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
            $dompdf->setPaper('A4', 'portrait');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser (force download)
            $dompdf->stream($request->get('_route') . ".pdf", [
                "Attachment" => true
            ]);
            exit();
        }
        return $this->render('smsCenter/sales-order-delivery-details.html.twig',[
            'records' => $records,
            'requestDate' => $requestDate,
            'districtId' => $districtId,
            'orderDeliveryStatus' => $orderDeliveryStatus,
        ]);
    }
}
