<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Core;

use App\Entity\Core\ItemKeyValue;
use App\Entity\Core\Setting;
use App\Form\Core\SettingFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/setting")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class SettingController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="core_setting")
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request): Response
    {
        $entitys = $this->getDoctrine()->getRepository(Setting::class)->findAll();
        return $this->render('core/setting/index.html.twig',['entities' => $entitys]);
    }

    /**
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="core_setting_new")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {

        $entity = new Setting();
        $data = $request->request->all();

        $form = $this->createForm(SettingFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setTerminal($this->getUser()->getTerminal()->getId());
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(ItemKeyValue::class)->insertSettingKeyValue($entity,$data);
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('core_setting_new');
            }
            return $this->redirectToRoute('core_setting');
        }
        return $this->render('core/setting/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="core_setting_edit")
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     * @param Request $request
     * @param Setting $entity
     * @return Response
     */
    public function edit(Request $request, Setting $entity): Response
    {
        $data = $request->request->all();
        $form = $this->createForm(SettingFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            $this->getDoctrine()->getRepository(ItemKeyValue::class)->insertSettingKeyValue($entity,$data);
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('core_setting_edit', ['id' => $entity->getId()]);
            }

            return $this->redirectToRoute('core_setting');
        }
        return $this->render('core/setting/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="core_setting_delete")
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     * @param $id
     * @return Response
     */

    public function delete($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()->getRepository(Setting::class)->find($id);
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $this->addFlash('success', 'post.deleted_successfully');
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->addFlash('error', 'Data has been relation another Table');

        }catch (\Exception $e) {
            $this->addFlash('error', 'Please contact system administrator further notification.');
        }
        return new Response($response);

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/meta-delete", methods={"GET"}, name="core_setting_meta_delete")
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     * @param $id
     * @return Response
     */
    public function metaDelete($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(ItemKeyValue::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }



}
