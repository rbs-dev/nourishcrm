<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Core;


use App\Entity\Admin\Location;
use App\Entity\Core\Agent;
use App\Entity\Core\Setting;
use App\Form\Core\AgentFormType;
use App\Form\Core\EditRegistrationFormType;
use App\Service\ConfigureManager;
use App\Service\FormValidationManager;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 * @Route("/core/agent")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class AgentController extends AbstractController
{
    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="core_agent")
     * @Route("/", methods={"GET"}, name="core_agent_index")
     */
    public function index(): Response
    {
        $businessType = $this->getDoctrine()->getRepository(Setting::class)->getChildRecords("agent-group");
        return $this->render('core/agent/index.html.twig', [
            'businessType' => $businessType,
        ]);

    }


    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE_AGENT') or is_granted('ROLE_CORE')")
     * @Route("/create", methods={"GET", "POST"}, name="core_agent_create")
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function create(Request $request , TranslatorInterface $translator): Response
    {
        $entity = new Agent();
        $locationRepo = $this->getDoctrine()->getRepository(Location::class);
        $form = $this->createForm(AgentFormType::class, $entity,array('locationRepo'=>$locationRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setTerminal($this->getUser()->getTerminal()->getId());
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($mobile);
            $em->persist($entity);
            $em->flush();
            $msg = $translator->trans('post.insert_successfully');
            return new Response($msg);
        }
        return $this->render('core/agent/new.html.twig', [
            'id' => 'postForm',
            'form' => $form->createView(),
            'actionUrl' => $this->generateUrl('core_agent_create'),
        ]);
    }


    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="core_agent_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE')")
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param $id
     * @return Response
     */
    public function edit(Request $request, TranslatorInterface $translator ,$id): Response
    {

        $entity = $this->getDoctrine()->getRepository(Agent::class)->find($id);
        $locationRepo = $this->getDoctrine()->getRepository(Location::class);
        $form = $this->createForm(AgentFormType::class, $entity,array('locationRepo'=>$locationRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($mobile);
            if($form->get('altMobile')->getData()){
                $altMobile = $confManager->specialExpClean($form->get('altMobile')->getData());
                $entity->setAltMobile($altMobile);
            }
            $this->getDoctrine()->getManager()->flush();
            $msg = $translator->trans('post.updated_successfully');
            return new Response($msg);
        }
        return $this->render('core/agent/new.html.twig', [
            'actionUrl' => $this->generateUrl('core_agent_edit',array('id'=> $entity->getId())),
            'post' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Check if mobile available for registering
     * @Route("/{mode}/available", methods={"GET"}, name="core_agent_process_available")
     * @param Request $request
     * @param string
     * @return Response
     */
    function isAvailable(Request $request , $mode) : Response
    {
        $data = $request->query->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $post = $this->getDoctrine()->getRepository(Agent::class)->checkAvailable($terminal,$mode,$data['agent_form']);
        return new Response($post);

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="core_agent_show")
     * @Security("is_granted('ROLE_CORE')")
     * @param $id
     * @return Response
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Agent::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
//        $em->remove($post);
//        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new JsonResponse(['id' =>$post->getAgentId(), 'agentId' => $post->getAgentId(), 'name' => $post->getName(), 'mobile' => $post->getMobile()]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="core_agent_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE')")
     * @param $id
     * @return Response
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Agent::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="core_agent_data_table")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE')")
     * @param Request $request
     * @return JsonResponse
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;

        $parameterName = isset($query['name']) && $query['name']!=''?$query['name']:'';
        $parameterAgentId = isset($query['agentId']) && $query['agentId']!=''?$query['agentId']:'';
        $parameterType = isset($query['type']) && $query['type']!=''?$query['type']:0;
        $parameterMobile = isset($query['mobile']) && $query['mobile']!=''?$query['mobile']:'';
        $parameterEmail = isset($query['email']) && $query['email']!=''?$query['email']:'';
        $parameterLocation = isset($query['location']) && $query['location']!=''?$query['location']:'';

        $parameterForTotal = [
            "agentId"=>$parameterAgentId,
            "name"=>$parameterName,
            "type"=>$parameterType,
            "mobile"=>$parameterMobile,
            "email"=>$parameterEmail,
            "location"=>$parameterLocation,
        ];


        $terminal = $this->getUser()->getTerminal()->getId();
        $iTotalRecords = $this->getDoctrine()->getRepository(Agent::class)->totalCount($parameterForTotal);
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;


        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc


        $parameter = array(
            "offset" => $iDisplayStart,
            'limit'=> $iDisplayLength,
            'orderBy' => $columnName,
            "order" => $columnSortOrder,
            "agentId"=>$parameterAgentId,
            "name"=>$parameterName,
            "type"=>$parameterType,
            "mobile"=>$parameterMobile,
            "email"=>$parameterEmail,
            "location"=>$parameterLocation,
        );

        $result = $this->getDoctrine()->getRepository(Agent::class)->findBySearchQuery($terminal,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Agent */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('core_agent_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('core_agent_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('core_agent_show',array('id'=> $post['id']));
            $outstanding = $this->generateUrl('kpi_agent_outstanding',array('id'=> $post['id']));
            $thana = "{$post['thana']},{$post['district']}";
            $records["data"][] = array(
                $id                 = $i,
                $agentId            = $post['agentId'],
                $name               = $post['name'],
                $businessType       = $post['businessType'],
                $location           = $thana,
                $mobile             = $post['mobile'],
                $email              = $post['email'],
                $action             ="<div class='dropdown'>
  <button type='button' class=' btn btn-notify dropbtn'><i class='fas fa-ellipsis-h'></i></button>
  <div class='dropdown-content'>
     <a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a>
    <a data-action='{$editUrl}' href='?process=postEdit-{$post['id']}&check=edit#modal' id='postEdit-{$post['id']}'>Edit</a>
    <a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a>
    <a   class='' data-id='{$post['id']}' href='{$outstanding}'>Outstanding</a>
  </div>
</div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE') or is_granted('ROLE_USER')")
     * @Route("/search", methods={"GET"}, name="core_agent_search_select2", options={"expose"=true})
     * @param Request $request
     * @return Response
     */
    public function searchSelect2(Request $request): Response
    {
        $data= $request->query->get('q');
        $agents = $this->getDoctrine()->getRepository(Agent::class)->getAgentSelect2($data);
        return new JsonResponse($agents);
    }

    /**
     * @param KernelInterface $kernel
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws Exception
     * @Route("/sync", name="sync_agent")
     */
    public function syncAgentFromSales(KernelInterface $kernel)
    {
        set_time_limit(0);
        ini_set('memory_limit', '5000M');

        //Update agent
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'app:update-agent'
        ]);

        $output = new NullOutput();

        $application->run($input, $output);
        //Update agent END

        $this->addFlash('success', 'Agent updated!');
        return $this->redirectToRoute('homepage');
    }

}
