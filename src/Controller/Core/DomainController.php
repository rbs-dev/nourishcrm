<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Core;


use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use App\Entity\Application\Production;
use App\Entity\Core\CompanyRegistration;
use App\Form\Core\CompanyFormType;
use App\Form\Core\DomainFormType;
use App\Form\Core\TerminalDomainType;
use App\Service\ConfigureManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/core/domain")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE_CUSTOMER')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class DomainController extends AbstractController
{

    /**
     * @Route("/", methods={"GET", "POST"}, name="core_domain")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE_CUSTOMER')")
     */
    public function edit(Request $request, TranslatorInterface $translator): Response
    {

        /* @var $entity Terminal */
        $entity = $this->getUser()->getTerminal();
        $form = $this->createForm(DomainFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('displayMobile')->getData());
            $entity->setDisplayMobile($mobile);
            if($form->get('ownerMobile')->getData()){
                $ownerMobile = $confManager->specialExpClean($form->get('ownerMobile')->getData());
                $entity->setOwnerMobile($ownerMobile);
            }
            if($form->get('phone')->getData()){
                $phone = $confManager->specialExpClean($form->get('phone')->getData());
                $entity->setPhone($phone);
            }
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->render('core/domain/new.html.twig', [
            'actionUrl' => $this->generateUrl('core_domain'),
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/main", methods={"GET", "POST"}, name="core_domain_terminal")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function domain(Request $request, TranslatorInterface $translator): Response
    {
        /* @var $entity Terminal */
        $entity = $this->getUser()->getTerminal();
        $form = $this->createForm(TerminalDomainType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('displayMobile')->getData());
            $entity->setDisplayMobile($mobile);
            $this->getDoctrine()->getManager()->flush();
            $msg = $translator->trans('post.updated_successfully');
            $this->getDoctrine()->getRepository(Terminal::class)->systemConfigUpdate($entity);
        }
        return $this->render('core/domain/domain.html.twig', [
            'actionUrl' => $this->generateUrl('core_domain_terminal'),
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/company", methods={"GET", "POST"}, name="core_domain_company")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function companyRegistration(Request $request, TranslatorInterface $translator): Response
    {
        /* @var $entity CompanyRegistration */

        $entity = $this->getUser()->getTerminal()->getCompany();

        $form = $this->createForm(CompanyFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            if($form->get('mobile')->getData()) {
                $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
                $entity->setMobile($mobile);
            }
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->render('core/domain/company.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/reset", methods={"GET", "POST"}, name="core_domain_reset")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function reset(Request $request, TranslatorInterface $translator): Response
    {
        /* @var $entity Terminal */

        $entity = $this->getUser()->getTerminal();

        $this->getDoctrine()->getRepository(Accounting::class)->reset($entity);
        $this->getDoctrine()->getRepository(Nbrvat::class)->reset($entity);
        $this->getDoctrine()->getRepository(Production::class)->reset($entity);
        $this->getDoctrine()->getRepository(Inventory::class)->reset($entity);

        exit;
    }



}
