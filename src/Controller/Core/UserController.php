<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Core;


use App\Entity\Core\Setting;
use App\Entity\User;
use App\Form\Core\PasswordUpdateFormType;
use App\Form\Core\RegistrationFormType;
use App\Form\Type\ChangePasswordType;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/core/user")
 * @Security("is_granted('ROLE_DEVELOPER')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class UserController extends AbstractController
{

    /**
     * Lists all Post entities.
     * @Route("/", name="core_user")
     * @Route("/", name="core_user_index")
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userGroup' => 8]);
        return $this->render('core/user/index.html.twig',[
            'users' => $users
        ]);
    }


    /**
     * @Route("/register", methods={"GET", "POST"}, name="core_user_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function register(Request $request , UserPasswordEncoderInterface $passwordEncoder,TranslatorInterface $translator): Response
    {
        $user = new User();
        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $form = $this->createForm(RegistrationFormType::class, $user, array('terminal' => $terminal,'userRepo'=>$userRepo));
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted()) {
            $roles = $form->get('roles')->getData();
            array_push($roles, 'ROLE_USER');

            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $user->setRoles($roles);
            $user->setEnabled(true);
            $user->setTerminal($terminal);
            $user->setUserGroup($this->getDoctrine()->getRepository(Setting::class)->findOneBy(['slug' => 'administrator']));
            $user->setUsername($form->get('email')->getData());

            try {
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', 'User Added!');

            }catch (\Exception $e){
                $this->addFlash('error', $e->getMessage());
                return $this->redirectToRoute('core_user_register');
            }
            return $this->redirectToRoute('core_user');
        }
        return $this->render('core/user/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="core_user_edit")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function edit(Request $request, User $user): Response
    {
        $prevRoles = $user->getRoles();

        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
//        $form = $this->createForm(EditRegistrationFormType::class, $user, array('terminal' => $terminal,'userRepo' => $userRepo))
//            ->add('SaveAndCreate', SubmitType::class);
        $form = $this->createForm(RegistrationFormType::class, $user, array('terminal' => $terminal,'userRepo' => $userRepo));
        $form->remove('password');
        $form->handleRequest($request);
      //  $errors = $this->getErrorsFromForm($form);
        if ($form->isSubmitted()) {
            $roles = $form->get('roles')->getData();
            array_push($roles, 'ROLE_USER');

            if (in_array('ROLE_DEVELOPER', $prevRoles)){
                array_push($roles, 'ROLE_DEVELOPER');
            }
            $user->setRoles($roles);

            try {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'User updated!');
            }catch (\Exception $e){
                $this->addFlash('error', $e->getMessage());
                return $this->redirectToRoute('core_user_edit', ['id' => $user->getId()]);
            }
            return $this->redirectToRoute('core_user');
        }
        return $this->render('core/user/editRegister.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }

    /**
     * Check if username available for registering
     * @Route("/register/username/available", methods={"GET"}, name="core_user_username_available")
     * @param   string
     * @return  bool
     */
    function isUsernameAvailable(Request $request) : Response
    {
        $data = $request->query->all();
        $username = $data['registration_form']['username'];
        $post = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username'=> $username]);
        if (empty($post)){
            $process = "true";
        }else{
            $process =  "false";
        }
        return new Response($process);

    }

    /**
     * Check if username available for registering
     * @Route("editable/username/available", methods={"GET"}, name="core_user_editable_username_available")
     * @param Request $request
     * @return Response
     */
    function isEditableUsernameAvailable(Request $request) : Response
    {
        $data = $request->query->all();
        $username = $data['registration_form']['username'];
        $post = $this->getDoctrine()->getRepository(User::class)->findBy(['username'=> $username]);
        if (count($post) > 1 ){
            $process = "true";
        }else{
            $process =  "false";
        }
        return new Response($process);

    }


    /**
     * @Route("/change-password", methods={"GET", "POST"}, name="user_change_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $form->get('newPassword')->getData()));
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('security_logout');
        }

        return $this->render('user/change_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="core_user_show", options={"expose"=true})
     * @param $id
     * @return Response
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $id]);
//        $em = $this->getDoctrine()->getManager();
//        $em->remove($post);
//        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response($post->getId());
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="core_user_delete", options={"expose" = true})
     * @param $id
     * @return Response
     */
    public function delete($id): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $id]);
        if ($user && $user->getUserMode() !== 'DEVELOPER'){
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            return new Response('Success');
        }else{
            return new Response('Invalid');
        }

    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="core_user_data_table")
     * @param Request $request
     * @param UserRepository $repository
     * @return JsonResponse
     */

    public function dataTable(Request $request, UserRepository $repository)
    {

        $query = $_REQUEST;
        $terminal = $this->getUser()->getTerminal();
        $iTotalRecords = $this->getDoctrine()->getRepository(User::class)->count(array('terminal'=> $terminal));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository(User::class)->findBySearchQuery($terminal->getId(),$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post User */

        foreach ($result as $post):

            $enabled = $post['enabled'] == 1 ? "Enabled" : "Disabled";
            $editUrl = $this->generateUrl('core_user_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('core_user_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('core_user_show',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                 = $i,
                $userGroup          = $post['userGroup'],
                $name               = $post['name'],
                $username           = $post['username'],
                $designation        = $post['designationName'],
                $mobile             = $post['mobile'],
                $email              = $post['email'],
                $department         = $post['departmentName'],
                $branch             = $post['branchName'],
                $status             = $enabled,
                $action             ="<div class='dropdown'>
<button type='button' class='btn btn-notify dropbtn'><i class='fas fa-ellipsis-h'></i></button>
<div class='dropdown-content'>
<a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a>
<a href='{$editUrl}'>Edit</a>
<a  data-action='{$deleteUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a>
</div></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Route("/{id}/update-password", name="user_password_update")
     * @param User $user
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function passwordUpdate(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(PasswordUpdateFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );

            try {
                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Password Updated!');
            }catch (\Exception $e){
                $this->addFlash('error', $e->getMessage());
            }
            return $this->redirectToRoute('core_user_index');
        }


        $html = $this->renderView('core/user/update_password.html.twig',[
            'form' => $form->createView(),
            'user' => $user,
        ]);
        return new JsonResponse(array('html'=>$html));

    }
}
