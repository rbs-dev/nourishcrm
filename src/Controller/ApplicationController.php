<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Core\Setting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage blog contents in the backend.
 *
 * Please note that the application backend is developed manually for learning
 * purposes. However, in your real Symfony application you should use any of the
 * existing bundles that let you generate ready-to-use backends without effort.
 *
 * @Route("/")
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ApplicationController extends AbstractController
{

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/admin", methods={"GET"}, name="app_admin")
     */
    public function index(): Response
    {

        return $this->render('dashboard/dashboard.html.twig');
    }


     /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE')")
     * @Route("/core", methods={"GET"}, name="app_core")
     */
    public function indexCore(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }


    /**
     * Lists all Post entities.
     * @Route("/accounting", methods={"GET"}, name="app_accounting")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT')")
     */
    public function indexAccounting(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }



    /**
     * Lists all Post entities.
     * @Route("/nbrvat", methods={"GET"}, name="app_nbrvat")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_NBRVAT')")
     */
    public function indexNbrvat(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/production", methods={"GET"}, name="app_production")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRODUCTION')")
     */
    public function indexProduction(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/bims", methods={"GET"}, name="app_bims")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_BUSINESS')")
     */
    public function indexBusiness(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }


    /**
     * Lists all Post entities.
     * @Route("/dms", methods={"GET"}, name="app_dms")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_DMS')")
     */
    public function indexDental(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/restaurant", methods={"GET"}, name="app_restaurant")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_RESTAURANT')")
     */
    public function indexRestaurant(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/prescription", methods={"GET"}, name="app_prescription")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PRESCRIPTION')")
     */
    public function indexPrescription(): Response
    {
        return $this->render('dashboard/dashboard.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/kpi-menu", methods={"GET"}, name="app_kpi_menu")
     */
    public function kpiMenu()
    {
        $entities = $this->getDoctrine()->getRepository(Setting::class)->getChildRecords('report-mode');
        return $this->render('@TerminalbdKpi/default/menu.html.twig',['modes'=>$entities]);
    }

    /**
     * Lists all Post entities.
     * @Route("/crm-menu", name="app_crm_menu")
     */
    public function crmMenu()
    {
        return $this->render('@TerminalbdCrm/default/menu.html.twig');
    }

    /**
     * Lists all Post entities.
     * @Route("/bank-reconciliation-menu", methods={"GET"}, name="app_bank_reconciliation_menu")
     */
    public function bankReconciliationMenu()
    {
        return $this->render('@TerminalbdBankReconciliation/default/menu.html.twig');
    }
    /**
     * Lists all Post entities.
     * @Route("/sms-center-menu", methods={"GET"}, name="app_sms_center_menu")
     */
    public function smsCenterMenu()
    {
        return $this->render('smsCenter/default/menu.html.twig');
    }

}
