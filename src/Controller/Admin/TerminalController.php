<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use App\Entity\Application\Production;
use App\Entity\Core\Customer;
use App\Entity\Core\Vendor;
use App\Service\ConfigureManager;
use App\Entity\Admin\AppModule;
use App\Entity\Admin\Syndicate;
use App\Entity\Admin\Terminal;
use App\Entity\User;
use App\Form\Admin\TerminalType;
use App\Repository\Admin\TerminalRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/admin/terminal")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_USER')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TerminalController extends AbstractController
{

    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="admin_terminal")
     * @Route("/", methods={"GET"}, name="admin_terminal_index")
     */
    public function index(): Response
    {
        $modules = $this->getDoctrine()->getRepository(AppModule::class)->findBy(array('status'=>1),array('name'=>"ASC"));
        $syndicates = $this->getDoctrine()->getRepository(Syndicate::class)->findBy(array('status'=>1),array('name'=>"ASC"));

        return $this->render('admin/terminal/index.html.twig', [
            'modules' => $modules,
            'syndicates' => $syndicates,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/signup", methods={"GET", "POST"}, name="admin_terminal_new")
     */
    public function register(Request $request ,TranslatorInterface $translator, UserPasswordEncoderInterface $encoder): Response
    {

        $entity = new Terminal();
        $form = $this->createForm(TerminalxType::class, $entity,array())
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $errors = $this->getErrorsFromForm($form);

        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            $mobile = $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($mobile);
            $uniqueKey = time();
            $entity->setUniqueKey($uniqueKey);
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(Terminal::class)->systemConfigUpdate($entity);
            $this->getDoctrine()->getRepository(User::class)->systemUserCreate($entity,$encoder);
            return $this->redirectToRoute('admin_terminal');
        }
        return $this->render('admin/terminal/new.html.twig', [
            'id' => 'postForm',
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="admin_terminal_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     */
    public function edit(Request $request, TranslatorInterface $translator ,$id): Response
    {

        $entity = $this->getDoctrine()->getRepository(Terminal::class)->find($id);
        $form = $this->createForm(TerminalType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($form->get('mobile')->getData());
            $entity->setMobile($confManager);
            $this->getDoctrine()->getManager()->flush();
            $msg = $translator->trans('post.updated_successfully');
            $this->getDoctrine()->getRepository(Terminal::class)->systemConfigUpdate($entity);
            return new Response($msg);
        }
        return $this->render('admin/terminal/edit.html.twig', [
            'actionUrl' => $this->generateUrl('admin_terminal_edit',array('id'=> $entity->getId())),
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/process", methods={"GET", "POST"}, name="admin_terminal_process")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     */
    public function process(Request $request, TranslatorInterface $translator ,$id): Response
    {

        $post = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=> $id]);
        $form = $this->createForm(TerminalType::class, $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $errors = $this->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $msg = $translator->trans('post.updated_successfully');
            return new Response($msg);
        }
        return $this->render('admin/terminal/new.html.twig', [
            'actionUrl' => $this->generateUrl('admin_terminal_edit',array('id'=> $post->getId())),
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }


    /**
     * Check if mobile available for registering
     * @Route("/{mode}/available", methods={"GET"}, name="admin_terminal_process_available")
     * @param   string
     * @return  bool
     */
    function isAvailable(Request $request , $mode) : Response
    {
        $data = $request->query->all();
        $post = $this->getDoctrine()->getRepository(Terminal::class)->checkAvailable($mode,$data['terminal']);
        return new Response($post);

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="admin_terminal_show")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Terminal::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="admin_terminal_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     */
    public function delete($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $post = $this->getDoctrine()->getRepository(Terminal::class)->findOneBy(['id'=> $id]);
        $customers = $this->getDoctrine()->getRepository(Customer::class)->findBy(['id'=> $id]);
        $vendors = $this->getDoctrine()->getRepository(Vendor::class)->findBy(['id'=> $id]);

        $this->getDoctrine()->getRepository(Accounting::class)->reset($post,'remove');
        $this->getDoctrine()->getRepository(Nbrvat::class)->reset($post,"remove");
        $this->getDoctrine()->getRepository(Production::class)->reset($post,"remove");
        $this->getDoctrine()->getRepository(Inventory::class)->reset($post,"remove");
        $em->remove($customers);
        $em->remove($vendors);
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="admin_terminal_data_table")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     */

    public function dataTable(Request $request,TerminalRepository $repository)
    {

        $query = $request->request->all();

        $iTotalRecords = $this->getDoctrine()->getRepository(Terminal::class)->recordCount();


        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order"=>$columnSortOrder);

        $result = $this->getDoctrine()->getRepository(Terminal::class)->findBySearchQuery($parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Terminal */

        foreach ($result as $post):
            $enabled = $post['status'] == 1 ? "Enabled" : "Disabled";
            $editUrl = $this->generateUrl('admin_terminal_edit',array('id' => $post['id']));
            $processUrl = $this->generateUrl('admin_terminal_delete',array('id'=> $post['id']));
            $viewUrl = $this->generateUrl('admin_terminal_show',array('id'=> $post['id']));
            $created = $post['created']->format('d-m-Y');
            $records["data"][] = array(
                $id                 = $i,
                $created            = $created,
                $organizationName   = $post['organizationName'],
                $name               = $post['name'],
                $mobile             = $post['mobile'],
                $location           = $post['locationName'],
                $mainApp            = $post['mainApp'],
                $syndicate          = $post['syndicateName'],
                $status             = $enabled,
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
 <li class='dropdown-item'> <a data-action='{$viewUrl}' href='?process=postView-{$post['id']}&check=show#modal' id='postView-{$post['id']}' >View</a></li>
<li class='dropdown-item'> <a data-action='' href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a  data-action='{$processUrl}' class='remove' data-id='{$post['id']}' href='javascript:'>Remove</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }
}
