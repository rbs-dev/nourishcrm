<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;


use App\Entity\Admin\Location;
use App\Entity\Core\Agent;
use App\Entity\Core\Setting;
use App\Form\Admin\LocationType;
use App\Form\Core\AgentFormType;
use App\Form\Core\EditRegistrationFormType;
use App\Repository\Admin\LocationRepository;
use App\Service\ConfigureManager;
use App\Service\FormValidationManager;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller used to manage current user.
 *
 * @Route("/admin/location")
 * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_CORE')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class LocationController extends AbstractController
{

    /**
     * Lists all Post entities.
     * @Route("/", methods={"GET"}, name="admin_location")
     * @Route("/", methods={"GET"}, name="admin_location_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $filterBy = $request->query->all();

        $records = $this->getDoctrine()->getRepository(Location::class)->getLocationFilterBy($filterBy);
        $records = $paginator->paginate($records, $request->get('page', 1), 25);

        //$entities = $this->getDoctrine()->getRepository(Location::class)->findAll();
        return $this->render('admin/location/index.html.twig',[
            'records' => $records,
            'filterBy' => $filterBy,
        ]);

    }


    /**
     * @Route("/create", methods={"GET", "POST"}, name="admin_location_new")
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function create(Request $request , TranslatorInterface $translator): Response
    {
        set_time_limit(0);
        $entity = new Location();
//        $locationRepo = $this->getDoctrine()->getRepository(Location::class);
//        $form = $this->createForm(LocationType::class, $entity,array('locationRepo'=>$locationRepo))
        $form = $this->createForm(LocationType::class, $entity)->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted()) {

            $parentId = $form->get('parent')->getViewData();
            $parent = null;
            if ($parentId) {
                $parent = $this->getDoctrine()->getRepository(Location::class)->find($parentId);
            }
                $form->getData()->setParent($parent);
                $entity->setName(ucfirst($entity->getName()));
                $em->persist($entity);
                $em->flush();
                $this->addFlash('success','Location Added!');
                return $this->redirectToRoute('admin_location');

/*
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('admin_location_new');
            }
            return $this->redirectToRoute('admin_location');*/
        }
        return $this->render('admin/location/new.html.twig', [
            'id' => 'postForm',
            'form' => $form->createView(),
            'actionUrl' => $this->generateUrl('admin_location_new'),
        ]);
    }


    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="admin_location_edit")
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param $id
     * @return Response
     */
    public function edit(Request $request, TranslatorInterface $translator ,$id): Response
    {
        set_time_limit(0);
        $entity = $this->getDoctrine()->getRepository(Location::class)->find($id);
        $form = $this->createForm(LocationType::class, $entity)->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_location');
        }
        return $this->render('admin/location/new.html.twig', [
            'post' => $entity,
            'form' => $form->createView(),
        ]);


    }


    /**
     * Check if mobile available for registering
     * @Route("/{mode}/available", methods={"GET"}, name="admin_location_process_available")
     * @param Request $request
     * @param string
     * @return Response
     */
    function isAvailable(Request $request , $mode) : Response
    {
        $data = $request->query->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $post = $this->getDoctrine()->getRepository(Agent::class)->checkAvailable($terminal,$mode,$data['agent_form']);
        return new Response($post);

    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="admin_location_show")
     * @param $id
     * @return Response
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Agent::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="admin_location_delete")
     * @param Location $location
     * @return Response
     */
    public function delete(Location $location): Response
    {
        $children = $this->getDoctrine()->getRepository(Location::class)->findBy(['parent' => $location]);
        if (!$children){
            $em = $this->getDoctrine()->getManager();
            $em->remove($location);
            $em->flush();
//        $this->addFlash('success', 'post.deleted_successfully');
            return new Response('Success');
        }else{
            return new Response('failed');
        }

    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="admin_location_data_table")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;

        $iTotalRecords = $this->getDoctrine()->getRepository(Location::class)->totalCount();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository(Location::class)->findBySearchQuery($parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Agent */

        foreach ($result as $post):

            $editUrl = $this->generateUrl('admin_location_edit',array('id' => $post['id']));
            $deleteUrl = $this->generateUrl('admin_location_delete',array('id'=> $post['id']));
            $records["data"][] = array(
                $id                 = $i,
                $name               = $post['name'],
                $district           = $post['district'],
                $action             ="<div class='btn-group card-option'><button type='button' class='btn btn-notify' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fas fa-ellipsis-h'></i></button>
<ul class='list-unstyled card-option dropdown-info dropdown-menu dropdown-menu-right'>
<li class='dropdown-item'> <a href='{$editUrl}'>Edit</a></li>
<li class='dropdown-item'> <a href='{$deleteUrl}'>Delete</a></li>
</ul></div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     *
     * @Route("/search", methods={"GET"}, name="district_search_select2", options={"expose"=true})
     * @param Request $request
     * @return Response
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DEVELOPER')")
     */
    public function districtSearchSelect2(Request $request): Response
    {
        $data= $request->query->get('q');
        $districts = $this->getDoctrine()->getRepository(Location::class)->getDistrictSelect2($data);

        return new JsonResponse($districts);
    }


    /**
     * @Route("location-by-level", name="location_by_level", options={"expose"=true})
     * @param Request $request
     */
    public function locationByLevel(Request $request)
    {
        $level = $request->query->get('locationTypeLevel') - 1; // parent level

        $locations = $this->getDoctrine()->getRepository(Location::class)->findBy(['level' => $level]);

        $data = [];

        foreach ($locations as $location) {
            $data[$location->getName()] = $location->getId();
        }
        ksort($data);
        return new JsonResponse([
            'level' => $level,
            'data' => $data,
            'total' => count($data),
        ]);

    }

    /**
     * @Route("/inline-update-location", name="inline_update_location", options={"expose"=true})
     * @param Request $request
     * @return JsonResponse
     */
    public function inlineUpdate(Request $request)
    {
        $location = $this->getDoctrine()->getRepository(Location::class)->find($request->request->get('pk'));

        if ($location){
            $location->setName($request->request->get('value'));
            $this->getDoctrine()->getManager()->persist($location);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(['status' => 202]);
        }else{
            return new JsonResponse(['status' => 404]);
        }
    }
}
