<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Admin\AppModule;
use App\Entity\Setting;
use App\Form\Admin\AppmoudleFormType;
use App\Form\SettingFormType;
use App\Form\SettingType;
use App\Repository\SettingRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/admin/appmodule")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 * @Security("is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN')")
 */
class AppmoduleController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="admin_appmodule")
     * @Security("is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN')")
     */
    public function index(Request $request): Response
    {
        return $this->render('admin/appmodule/index.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN')")
     * @Route("/new", methods={"GET", "POST"}, name="admin_appmodule_new")
     */
    public function new(Request $request): Response
    {

        $post = new AppModule();
        $form = $this->createForm(AppmoudleFormType::class , $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('saveAndContinue')->isClicked()) {
                return $this->redirectToRoute('admin_appmodule_new');
            }
            return $this->redirectToRoute('admin_appmodule');
        }
        return $this->render('core/setting/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id<\d+>}/edit",methods={"GET", "POST"}, name="admin_appmodule_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, AppModule $post): Response
    {

        $form = $this->createForm(AppmoudleFormType::class, $post)
            ->add('saveAndContinue', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('saveAndContinue')->isClicked()) {
                return $this->redirectToRoute('admin_appmodule_edit', ['id' => $post->getUniqueKey()]);
            }
            return $this->redirectToRoute('admin_appmodule');
        }
        return $this->render('core/setting/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="admin_appmodule_delete")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(AppModule::class)->findOneBy(['uniqueKey'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }
}
