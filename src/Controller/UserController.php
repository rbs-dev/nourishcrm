<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\Core\SwitchUserFormType;
use App\Form\Type\ChangePasswordType;
use App\Form\UserType;
use App\Service\SmsSender;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Terminalbd\KpiBundle\Entity\EmployeeDistrictHistory;

/**
 * Controller used to manage current user.
 *
 * @Route("/profile")
 *
 * @author Romain Monteil <monteil.romain@gmail.com>
 */
class UserController extends AbstractController
{
    /**
     * @Route("/edit", methods="GET|POST", name="user_edit")
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @return Response
     */
    public function edit(Request $request): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'user.updated_successfully');

            return $this->redirectToRoute('user_edit');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/change-password", methods="GET|POST", name="user_change_password")
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = $this->getUser();

        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $form->get('newPassword')->getData()));

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('security_logout');
        }

        return $this->render('user/change_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/my-profile", name="my_profile")
     * @IsGranted("ROLE_USER")
     */
    public function myProfile()
    {
        $districtHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->getDistricts($this->getUser());

        return $this->render('employee/employeeDetails.html.twig',[
            'user' => $this->getUser(),
            'districtHistory' => $districtHistory,
        ]);
    }

    /**
     * @Route("/forgot-password", name="forgot_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SmsSender $smsSender
     * @return JsonResponse
     */
    public function forgotPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder, SmsSender $smsSender)
    {
        $message = '';
        $employeeId = $request->request->get('id');
        $mobile = $request->request->get('mobile');

        $findEmployee = $this->getDoctrine()->getRepository(User::class)->findOneBy(['userId' => $employeeId]);
        if ($findEmployee){
            if ($findEmployee->getMobile() === $mobile){
                $findEmployee->setPassword($passwordEncoder->encodePassword($findEmployee, 123456));
                $this->getDoctrine()->getManager()->flush();
                $msg = 'Hello, ' . $findEmployee->getName() . '. Your password has been reset. New password is: 123456. Please change password after login!';
                $mobileNumber = str_replace('-','', $findEmployee->getMobile());
                $smsSender->sendSms($msg, $mobileNumber);
                $message = 'Password is reset and sent new password in your mobile number!';
            }else{
                $message = 'Mobile number does not match!';
            }
        }else{
            $message = 'Employee does not found for ID: ' . $employeeId;
        }


        return new JsonResponse(['message' => $message]);

    }

    /**
     * @Route("/switch-user", name="switch_user", methods="GET|POST")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function impersonate(Request $request, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ALLOWED_TO_SWITCH');

        $form = $this->createForm(SwitchUserFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userId =  $form->getData()['userId'];

            $user = $em->getRepository(User::class)->findOneBy(['userId'=>$userId]);

            if (!$user) {
                throw new UsernameNotFoundException(sprintf('User "%s" not found.', $userId));
            }

            // Get the security token storage
            $tokenStorage = $this->container->get('security.token_storage');

            // Get the original token
            $originalToken = $tokenStorage->getToken();

            if (!$request->getSession()->get('_switch_user')) {
                $request->getSession()->set('_switch_user', serialize($originalToken) );
            }

            // Impersonate the requested user
            $impersonationToken = new UsernamePasswordToken(
                $user,
                "main",
                "main",
                $user->getRoles()
            );
            // Check if the impersonation is successful
            if ($impersonationToken->getUser() === $user) {
                $tokenStorage->setToken($impersonationToken);
                return $this->redirectToRoute('homepage');
            } else {
                return $this->redirectToRoute('security_logout');
            }
        }

        return $this->render('user/switch-user.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/switch-back", name="switch_back")
     * @param Request $request
     * @return Response
     */
    /*public function unImpersonate(Request $request): Response
    {
        // Get the security token storage
        $tokenStorage = $this->container->get('security.token_storage');

        // Get the original token
        if ($request->getSession()->get('_switch_user')) {
            $originalToken = unserialize($request->getSession()->get('_switch_user'));
            // unset the original token from the session
            $request->getSession()->remove('_switch_user');
            $tokenStorage->setToken($originalToken);

            return $this->redirectToRoute('homepage');
        }

        return $this->redirectToRoute('security_logout');
    }*/
}
