<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Setting;
use App\Entity\User;
use App\Entity\UserHistory;
use App\Form\EmployeeExpenseChartFormType;
use App\Repository\Core\SettingRepository;
use App\Repository\UserRepository;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\CrmBundle\Entity\ExpenseChart;
use Terminalbd\CrmBundle\Entity\ExpenseChartDetail;
use Terminalbd\CrmBundle\Form\ExpenseChartFormType;
use Terminalbd\KpiBundle\Entity\EmployeeBoard;
use Terminalbd\KpiBundle\Entity\EmployeeDistrictHistory;
use Terminalbd\KpiBundle\Entity\EmployeeReportFormatHistory;
use App\Form\EditEmployeeFormType;
use App\Form\EmployeeFilterFormType;
use App\Form\EmployeeFormType;


/**
 * @Route("/employee")
 * @Security("is_granted('ROLE_CRM_POULTRY_ADMIN') or is_granted('ROLE_CRM_CATTLE_ADMIN') or is_granted('ROLE_CRM_AQUA_ADMIN') or is_granted('ROLE_CRM_SALES_MARKETING_ADMIN') or is_granted('ROLE_MANAGE_EMPLOYEE') or is_granted('ROLE_DEVELOPER')")
 */
class EmployeeController extends AbstractController
{

    /**
     * @Route("/list/{mode}", defaults={"mode" = null}, methods={"GET"}, name="employee_index")
     * @param Request $request
     * @param $mode
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, $mode, PaginatorInterface $paginator): Response
    {

        $entities = $this->getDoctrine()->getRepository(User::class)->getEmployees();

        $form = $this->createForm(EmployeeFilterFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $filterBy = $form->getData();
            if (count(array_keys($filterBy, null)) == count($filterBy)){
                $entities = [];
            } else{
                $entities = $this->getDoctrine()->getRepository(User::class)->getSearchedEmployee($filterBy);
            }
        }
        $data = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );

        if ($mode == 'excel'){
            return $this->render('employee/employee-excel.html.twig',[
                'entities' => $entities,
                'form' => $form->createView()
            ]);
        }

        return $this->render('employee/index.html.twig',[
            'entities' => $data,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/register", methods={"GET", "POST"}, name="employee_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @throws \Exception
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
//        $locationRepo = $this->getDoctrine()->getRepository(Location::class);
        $form = $this->createForm(EmployeeFormType::class, $user, array('terminal' => $terminal,'userRepo'=>$userRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
//        $errors = $this->getErrorsFromForm($form);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get('permanentDate')->getData()){
                $user->setIsPermanent(true);
            }else{
                $user->setIsPermanent(false);
            }

            $roles = $form->get('roles')->getData();
            array_push($roles, 'ROLE_USER');


            $encoded = $passwordEncoder->encodePassword($user, $form->get('password')->getData());
//            $this->get('kpi_bundle.user_manager')->setUserPassword($user, $form->get('password')->getData());
            $user->setRoles($roles);
            $user->setUsername($user->getEmail());
            $user->setPassword($encoded);
            $user->setUserMode('KPI');
            $user->setUserGroup($this->getDoctrine()->getRepository(Setting::class)->findOneBy(['slug' => 'employee']));
            $user->setTerminal($terminal);
            $user->setCrmLineManager($user->getLineManager()?$user->getLineManager():null);

            $getRequestLabs = $request->request->get('labs');
            if($getRequestLabs && sizeof($getRequestLabs)>0){
                $user->setLabs(json_encode($getRequestLabs));
            }else{
                $user->setLabs(null);
            }

            $em->persist($user);
            $em->flush();

            // add history
            $districtsArray = [];
            foreach ($user->getDistrict() as $district) {
                $districtsArray[$district->getId()] = $district->getName();
            }
            $joiningYear = $user->getJoiningDate()->format('Y');
            $currentYear = (new \DateTime('now'))->format('Y');

            for ($i = $joiningYear; $i <= $currentYear; $i++){
                for ($j = 1; $j <= 12; $j++){
                    $date = '01-' . $j . '-' . $i;

                    $history = new EmployeeDistrictHistory();
                    $history->setEmployee($user);
                    $history->setLineManager($user->getLineManager());
                    $history->setDistrict(json_encode($districtsArray));
                    $history->setMonth((new \DateTime($date))->format('F'));
                    $history->setYear((new \DateTime($date))->format('Y'));
                    $history->setCreatedAt(new \DateTime('now'));
                    $em->persist($history);
                    $em->flush();

                }
            }
            $userHistory = new UserHistory();
            $userHistory->setEmployee($user);
            $userHistory->setDesignation($user->getDesignation());
            $userHistory->setCreatedBy($this->getUser());
            $userHistory->setCreatedAt(new \DateTime('now'));
            $em->persist($userHistory);
            $em->flush();


            $this->addFlash('success', 'Employee added successfully!');
            return $this->redirectToRoute('employee_index');
        }
        $labs = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->findBy(['settingType'=>'LAB_NAME', 'status'=>1]);
        return $this->render('employee/register.html.twig', [
//            'id' => 'postForm',
//            'post' => $user,
            'labs' => $labs,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="employee_edit")
     * @param Request $request
     * @param User $employee
     * @return Response
     * @throws \Exception
     */
    public function edit(Request $request, User $employee): Response
    {
        $assignedLineManager = clone $employee->getLineManager(); // copy assigned districts
        $assignedDistricts = clone $employee->getDistrict(); // copy assigned districts
        $previousDesignation = clone $employee->getDesignation(); // copy assigned districts

        $assignedDistrictsArray = [];
        foreach ($assignedDistricts as $assignedDistrict) {
            $assignedDistrictsArray[$assignedDistrict->getId()] = $assignedDistrict->getName();
        }


        $terminal = $this->getUser()->getTerminal();
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $form = $this->createForm(EditEmployeeFormType::class, $employee, array('terminal' => $terminal,'userRepo' => $userRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->remove('phone');
        $form->handleRequest($request);



        if ($form->isSubmitted()) {

            if($employee->getLineManager() && $employee->getLineManager()->getLineManager() && $employee->getLineManager()->getLineManager()->getId()==$employee->getId()) {
                $this->addFlash('warning', 'This Line Manager Assign is not possible.');
                return $this->redirectToRoute('employee_edit',array('id'=> $employee->getId()));
            }

            if($employee->getCrmLineManager() && $employee->getCrmLineManager()->getCrmLineManager() && $employee->getCrmLineManager()->getCrmLineManager()->getId()==$employee->getId()) {
                $this->addFlash('warning', 'This CRM Line Manager Assign is not possible.');
                return $this->redirectToRoute('employee_edit',array('id'=> $employee->getId()));
            }

            $transferJoiningDate = null;
            $districts = null;

            if ($form['transferJoiningDate']->getData() != null){
//                $transferJoiningDate = new \DateTime($form['transferJoiningDate']->getData());
                $transferJoiningDate = $form['transferJoiningDate']->getData();

            }

            foreach ($form->getData()->getDistrict() as $district){
                $districts[$district->getId()]= $district->getName();
            }

            //return a warning when do district change without transfer date
            if ( is_null($transferJoiningDate) && $assignedDistrictsArray != $districts){
                $this->addFlash('warning', 'Transfer date is required when change districts');
                return $this->redirectToRoute('employee_edit',array('id'=> $employee->getId()));
            }
            //return a warning when do line manager change without transfer date
            if ( is_null($transferJoiningDate) && $employee->getLineManager()->getId() != $assignedLineManager->getId()){
                $this->addFlash('warning', 'Transfer date is required when change line manager');
                return $this->redirectToRoute('employee_edit',array('id'=> $employee->getId()));
            }

            $roles = $form->get('roles')->getData();
            array_push($roles, 'ROLE_USER');

            $em = $this->getDoctrine()->getManager();
            $employee->setUserGroup($this->getDoctrine()->getRepository(Setting::class)->findOneBy(['slug' => 'employee']));
            $employee->setRoles($roles);
            if ($form->get('permanentDate')->getData()){
                $employee->setIsPermanent(true);
            }else{
                $employee->setIsPermanent(false);
            }
            $lastAssignReportFormat = $this->getDoctrine()->getRepository(EmployeeReportFormatHistory::class)->findOneBy(['employee' => $employee], ['id' => 'DESC']);
            $reportFormat = $form->getData()->getReportMode();

            if ($transferJoiningDate){
                $findKpi=null;

                if($transferJoiningDate->format('F')=='January'){
                    $findKpi = $this->getDoctrine()->getRepository(EmployeeBoard::class)->findOneBy(['employee' => $employee, 'year' => ($transferJoiningDate->format('Y')-1)], ['id' => 'DESC']);
                }else{
                    $findKpi = $this->getDoctrine()->getRepository(EmployeeBoard::class)->findOneBy(['employee' => $employee, 'year' => $transferJoiningDate->format('Y')], ['id' => 'DESC']);
                }
                $monthName = $findKpi?$findKpi->getMonth():'';
                $yearName = $findKpi?$findKpi->getYear():'';

                $transferableMonth = date('F,Y', strtotime('01-'. $monthName .'-'. $yearName . ' +1 month'));

                //employee available for transferring immediate next month from last generated KPI
                //check permanent month & transfer month
                if (!$findKpi && $employee->isPermanent() && $transferJoiningDate->format('m-Y') != $employee->getPermanentDate()->format('m-Y')){
                    $this->addFlash('warning', "This employee kpi not found before month of {$transferJoiningDate->format('F,Y')}");
                    return $this->redirectToRoute('employee_edit',array('id'=> $employee->getId()));

                }

                // Discussed with Shafiq to remove next month check
                /*elseif ($findKpi && strcmp($transferableMonth, $transferJoiningDate->format('F,Y')) !== 0){
                    $this->addFlash('warning', "This employee only available for transferring from {$transferableMonth}");
                    return $this->redirectToRoute('employee_edit',array('id'=> $employee->getId()));
                }*/

                $months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

                $monthsArray = [];
                $yearArray = range($transferJoiningDate->format('Y'), date('Y'));

                for ($i = $transferJoiningDate->format('m'); $i <= 12; $i++){
                    $date = '01-'.$i.'-2021';
                    array_push($monthsArray, (new \DateTime($date))->format('F'));
                }

                $monthsArray = array_diff($months,$monthsArray );

                // update line manager for transferring month to end of the current year
                if ($employee->getLineManager()->getId() != $assignedLineManager->getId()){

                    $findHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->findBy(['employee' => $employee, 'year' => $yearArray]);

                    foreach ($findHistory as $history) {
                        if ($history->getYear() != $transferJoiningDate->format('Y') || ($history->getYear() == $transferJoiningDate->format('Y') && !in_array($history->getMonth(), $monthsArray))){
                            $history->setLineManager($form->getData()->getLineManager());
                            $history->setUpdatedAt(new \DateTime('now'));
                            $history->setUpdatedBy($this->getUser());
                            $em->persist($history);
                            $em->flush();
                        }

                    }
                }

                // if all previous month kpi are not generated and approved, employee can't be transfer
                $findKpis = $this->getDoctrine()->getRepository(EmployeeBoard::class)->findBy(['employee' => $employee, 'month' => $monthsArray, 'year' => $transferJoiningDate->format('Y'), 'process' => 'approved']);

                if (count($monthsArray) != count($findKpis) && $employee->isPermanent() && $transferJoiningDate->format('m-Y') != $employee->getPermanentDate()->format('m-Y')){
                    $this->addFlash('warning', 'Please generate and approve all previous months KPI.');
                    return $this->redirectToRoute('employee_edit', ['id' => $employee->getId()]);
                }

                $findHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->findBy(['employee' => $employee, 'year' => $yearArray]);

                // add history
                foreach ($findHistory as $history) {
                    if ($history->getYear() != $transferJoiningDate->format('Y') || ($history->getYear() == $transferJoiningDate->format('Y') && !in_array($history->getMonth(), $monthsArray))){
                        $history->setDistrict(json_encode($districts));
                        $history->setUpdatedBy($this->getUser());
                        $history->setUpdatedAt(new \DateTime('now'));
                        $em->persist($history);
                        $em->flush();
                    }

                }
            }

//            else{
//                //Assign previous districts
//                $employee->setDistrict($assignedDistricts);
//
////                $findCurrentMonthHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->findOneBy(['employee' => $employee, 'month' => date('F'), 'year' => date('Y')]);
////                $findCurrentMonthHistory->setDistrict(json_encode($districts));
////                $em->persist($findCurrentMonthHistory);
////                $em->flush();
//            }

            if ($lastAssignReportFormat == null || $lastAssignReportFormat->getReportFormat()->getId() != $reportFormat->getId()){
                $reportFormatHistory = new EmployeeReportFormatHistory();
                $date = new \DateTime('now');

                $reportFormatHistory->setEmployee($employee);
                $reportFormatHistory->setReportFormat($reportFormat);
                $reportFormatHistory->setMonth($date->format('F'));
                $reportFormatHistory->setYear($date->format('Y'));
                $reportFormatHistory->setUpdatedBy($this->getUser());
                $em->persist($reportFormatHistory);
                $em->flush();

            }

            if($previousDesignation && $previousDesignation->getId()!=$form->get('designation')->getData()->getId()){
                $userHistory = new UserHistory();
                $userHistory->setEmployee($employee);
                $userHistory->setDesignation($employee->getDesignation());
                $userHistory->setCreatedBy($this->getUser());
                $userHistory->setCreatedAt(new \DateTime('now'));
                $em->persist($userHistory);
            }

            $getRequestLabs = $request->request->get('labs');
            if($getRequestLabs && sizeof($getRequestLabs)>0){
                $employee->setLabs(json_encode($getRequestLabs));
            }else{
                $employee->setLabs(null);
            }

            $em->persist($employee);
            $em->flush();


            $this->addFlash('success', 'Employee details updated!');
            return $this->redirectToRoute('employee_edit',array('id'=> $employee->getId()));
        }
        $labs = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->findBy(['settingType'=>'LAB_NAME', 'status'=>1]);
        return $this->render('employee/editRegister.html.twig', [
            'user' => $employee,
            'labs' => $labs,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/reset-password", methods={"GET", "POST"}, name="employee_reset_password", options={"expose"=true})
     * @param User $user
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function changeUserPassword(User $user, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user->setPassword(
            $passwordEncoder->encodePassword($user, 123456)
        );
        $this->getDoctrine()->getManager()->flush();
        $this->addFlash('success', 'Set default password: 123456');

        return $this->redirectToRoute('employee_index');

    }


    /**
     * @Route("/{id}/reset-inline-password", methods={"GET", "POST"}, name="employee_inline_reset_password", options={"expose"=true})
     * @param User $user
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function changeInlineUserPassword(User $user, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $plainPassword = $request->request->get('value');

        $user->setPassword(
            $passwordEncoder->encodePassword($user,$plainPassword)
        );
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 200,
            'message' => 'success'
        ]);

    }



    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }

    /**
     * @Route("/designation-select", name="employee_designation_select", options={"expose"=true})
     * @param SettingRepository $settingRepository
     * @return JsonResponse
     */

    public function selectDesignation(SettingRepository $settingRepository)
    {
        $designations = $settingRepository->getDesignations();
        return New JsonResponse($designations);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @Route("/designation-inline-update/{id}", name="employee_designation_inline_update", options={"expose"=true})
     */
    public function inlineUpdateDesignation(Request $request, User $user)
    {

        $data = $request->request->all();
//        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['userId' => $userId]);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User');
        }
        if($user->getDesignation() && $user->getDesignation()->getId()==$data['value']){
            return new JsonResponse(['status' => 200]);
        }else{

            $designation = $this->getDoctrine()->getRepository(Setting::class)->find($data['value']);
            $user->setDesignation($designation);
            $this->getDoctrine()->getManager()->flush();

            $userHistory = new UserHistory();
            $userHistory->setEmployee($user);
            $userHistory->setDesignation($user->getDesignation());
            $userHistory->setCreatedBy($this->getUser());
            $userHistory->setCreatedAt(new \DateTime('now'));
            $this->getDoctrine()->getManager()->persist($userHistory);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(['status' => 200]);
        }

    }

    /**
     * @Route("/line-manager-select", name="employee_line_manager_select", options={"expose"=true})
     * @param UserRepository $repository
     * @return JsonResponse
     */

    public function selectLineManager(UserRepository $repository)
    {
        $lineManagers = $repository->getLineManagerForInlineUpdate();
        return new JsonResponse($lineManagers);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @Route("/line-manager-inline-update/{id}", name="employee_line_manager_inline_update", options={"expose"=true})
     */
    public function inlineUpdateLineManager(Request $request, User $user)
    {

        $data = $request->request->all();
//        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['userId' => $userId]);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User');
        }

        $lineManager = $this->getDoctrine()->getRepository(User::class)->find($data['value']);
        $user->setLineManager($lineManager);
        $this->getDoctrine()->getManager()->flush();

        // update line manager for current month to end of the year
        $applicableMonths = [];
        for ($i = date('m'); $i <= 12; $i++){
            $date = '01-'.$i.'-2021';
            array_push($applicableMonths, (new \DateTime($date))->format('F'));
        }
        $findHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->findBy(['employee' => $user, 'month' => $applicableMonths, 'year' => date('Y')]);
        foreach ($findHistory as $history) {
            $history->setLineManager($lineManager);
            $history->setUpdatedAt(new \DateTime('now'));
            $history->setUpdatedBy($this->getUser());
            $this->getDoctrine()->getManager()->persist($history);
            $this->getDoctrine()->getManager()->flush();
        }

        return new JsonResponse(['status' => 200]);

    }

    /**
     * @Route("/report-mode-select", name="employee_report_mode_select", options={"expose"=true})
     * @param SettingRepository $repository
     * @return JsonResponse
     */

    public function selectReportMode(SettingRepository $repository)
    {
        $reportModes = $repository->getReportModes();
        return new JsonResponse($reportModes);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @Route("/report-mode-inline-update/{id}", name="employee_report_mode_inline_update", options={"expose"=true})
     */
    public function inlineUpdateReportMode(Request $request, User $user)
    {

        $data = $request->request->all();
//        $user = $this->getDoctrine()->getRepository(User::class)->find();

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User');
        }

        $reportMode = $this->getDoctrine()->getRepository(Setting::class)->find($data['value']);

        $lastAssignReportFormat = $this->getDoctrine()->getRepository(EmployeeReportFormatHistory::class)->findOneBy(['employee' => $user], ['id' => 'DESC']);

        if ($lastAssignReportFormat == null || $lastAssignReportFormat->getReportFormat()->getId() != $reportMode->getId()){
            $reportFormatHistory = new EmployeeReportFormatHistory();
            $date = new \DateTime('now');

            $reportFormatHistory->setEmployee($user);
            $reportFormatHistory->setReportFormat($reportMode);
            $reportFormatHistory->setMonth($date->format('F'));
            $reportFormatHistory->setYear($date->format('Y'));
            $reportFormatHistory->setUpdatedBy($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($reportFormatHistory);
        }


        $user->setReportMode($reportMode);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 200]);

    }

    /**
     * @Route("/hierarchy/{mode}",defaults={"mode" = null}, name="employee_hierarchy")
     * @param UserRepository $repository
     * @param $mode
     * @param Request $request
     * @return Response
     */

    public function employeeHierarchy(UserRepository $repository, $mode, Request $request)
    {
        $data = $repository->getEmployeeHierarchy();

        if ($mode == 'pdf'){
            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial, sans-serif');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('employee/employeeHierarchy-pdf.html.twig',[
                'data' => $data,
            ]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'landscape'
            $dompdf->setPaper('legal', 'landscape');

            // Render the HTML as PDF
            $dompdf->render();

            // Output the generated PDF to Browser (force download)
            $fileName = $request->get('_route') . '-' . time();
            $dompdf->stream( $fileName .  ".pdf", [
                "Attachment" => true
            ]);
            die();
        }
        return $this->render('employee/employeeHierarchy.html.twig',[
            'data' => $data,
        ]);
    }

    /**
     * @Route("/{id}/details", name="employee_details", options={"expose"=true})
     * @param User $employee
     * @return JsonResponse
     */
    public function employeeDetails(User $employee)
    {
        $districtHistory = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->getDistricts($employee);

        $html = $this->renderView('/inc/_employee-details-modal.html.twig',[
            'employee' => $employee,
            'districtHistory' => $districtHistory,
        ]);
        return new JsonResponse(array('html'=>$html));
    }

    /**
     * @Route("/district/history/process", name="employee_district_history_process")
     * @Security("is_granted('ROLE_DEVELOPER')")
     */
    public function districtProcess()
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $users = $this->getDoctrine()->getRepository(User::class)->findBy(['userGroup' => [8,9]]);
        $data = null;
        foreach ($users as $user) {
            $districts = null;

           foreach ($user->getDistrict() as $district) {
                $districts[$district->getId()] = $district->getName();
            }
                for ($i = 1; $i <= 12; $i++){
                    $date = "01-$i-2021";
                    $month = (new \DateTime($date))->format('F');
                    $year = (new \DateTime('now'))->format('Y');

                    $exist = $this->getDoctrine()->getRepository(EmployeeDistrictHistory::class)->findOneBy(['employee' => $user, 'month' => $month, 'year' => $year]);
                    if ($exist){
                        $exist->setLineManager($user->getLineManager());
                        $exist->setDistrict($districts ? json_encode($districts) : null);
                        $exist->setUpdatedBy($this->getUser());
                        $exist->setUpdatedAt(new \DateTime('now'));
                        $this->getDoctrine()->getManager()->flush();
                    }else{
                        $sql = "INSERT INTO `kpi_employee_district_history`(`employee_id`,`line_manager_id`, `district`, `month`, `year`, `created_at`) VALUES (:employee_id, :line_manager_id, :district, :month, :year, :created_at)";
                        $stmt = $this->getDoctrine()->getConnection()->prepare($sql);
                        $stmt->bindValue('employee_id', $user->getId());
                        $stmt->bindValue('line_manager_id', $user->getLineManager() ? $user->getLineManager()->getId() : null);
                        $stmt->bindValue('district', $districts ? json_encode($districts) : null);
                        $stmt->bindValue('month', $month);
                        $stmt->bindValue('year', $year);
                        $stmt->bindValue('created_at', (new \DateTime('now'))->format('Y-m-d H:i:s'));
                        $stmt->execute();
                    }
                }
        }
        $this->addFlash('success', 'District history updated!');
        return $this->redirectToRoute('employee_index');
    }

    /**
     * @Route("/employee-expense-chart/{id}/add", name="employee_expense_chart")
     * @param User $employee
     * @return Response
     */
    public function employeeExpenseChartAdd(User $employee): Response
    {
        $exitingExpenseChart = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\ExpenseChart::class)->findOneBy(['employee' => $employee]);
        $monthlyExpenseParticulars = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->getMonthlyExpenseParticular();

        $dailyUserDefineExpenseParticulars = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->getUserDefineDailyExpenseParticular();

        if($exitingExpenseChart){
           $expenseChart = $exitingExpenseChart;
           $expenseChart->setTypeOfVehicle($employee && $employee->getTypeOfVehicle()? $employee->getTypeOfVehicle():null);
           $expenseChart->setDesignation($employee && $employee->getDesignation()? $employee->getDesignation():null);
           $expenseChart->setUpdatedBy($this->getUser());
           $expenseChart->setUpdatedAt(new \DateTime('now'));
           $this->getDoctrine()->getManager()->flush();
        }else{
            $expenseChart = new \Terminalbd\CrmBundle\Entity\ExpenseChart();
            $expenseChart->setEmployee($employee);
            $expenseChart->setTypeOfVehicle($employee && $employee->getTypeOfVehicle()? $employee->getTypeOfVehicle():null);
            $expenseChart->setDesignation($employee && $employee->getDesignation()? $employee->getDesignation():null);
            $expenseChart->setCreatedBy($this->getUser());
            $expenseChart->setCreatedAt(new \DateTime('now'));
            $this->getDoctrine()->getManager()->persist($expenseChart);
        }

        if ($expenseChart){

            if($monthlyExpenseParticulars && sizeof($monthlyExpenseParticulars)>0){
                foreach ($monthlyExpenseParticulars as $particular) {
                    $particularObj=$this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->find($particular['id']);
                    $existingExpenseChartDetail = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\ExpenseChartDetail::class)->findOneBy(['expenseChart' => $expenseChart, 'particular' => $particularObj]);
                    if(!$existingExpenseChartDetail){
                        $expenseParticular= new \Terminalbd\CrmBundle\Entity\ExpenseChartDetail();
                        $expenseParticular->setAmount(0);
                        $expenseParticular->setExpenseChart($expenseChart);
                        $expenseParticular->setParticular($particularObj?$particularObj:null);
                        $expenseParticular->setPaymentDuration('MONTHLY');
                        $this->getDoctrine()->getManager()->persist($expenseParticular);
                    }
                }
            }

            if($dailyUserDefineExpenseParticulars && sizeof($dailyUserDefineExpenseParticulars)>0){
                foreach ($dailyUserDefineExpenseParticulars as $particular) {
                    $particularObj=$this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->find($particular['id']);
                    $existingExpenseChartDetail = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\ExpenseChartDetail::class)->findOneBy(['expenseChart' => $expenseChart, 'particular' => $particularObj]);
                    if(!$existingExpenseChartDetail){
                        $expenseParticular= new \Terminalbd\CrmBundle\Entity\ExpenseChartDetail();
                        $expenseParticular->setAmount(0);
                        $expenseParticular->setExpenseChart($expenseChart);
                        $expenseParticular->setParticular($particularObj?$particularObj:null);
                        $expenseParticular->setPaymentDuration('DAILY');
                        $this->getDoctrine()->getManager()->persist($expenseParticular);
                    }
                }
            }

        }
        $this->getDoctrine()->getManager()->flush();

//        dd($expenseChart);
        return $this->redirectToRoute('employee_expense_chart_edit', [
            'employee' => $employee->getId(),
            'expenseChart' => $expenseChart->getId()
        ]);

    }

    /**
     * @Route("/employee-expense-chart/{employee}/{expenseChart}/edit", name="employee_expense_chart_edit")
     * @param User $employee
     * @param ExpenseChart $expenseChart
     * @return Response
     */
    public function employeeExpenseChartEdit( Request $request, User $employee, ExpenseChart $expenseChart): Response
    {

        $form = $this->createForm(EmployeeExpenseChartFormType::class, $expenseChart);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $expenseChart->setUpdatedBy($this->getUser());
            $expenseChart->setUpdatedAt(new \DateTime('now'));


            $data = $request->request->get('amount');

            if($data && sizeof($data)>0){
                foreach ($data as $expenseChartDetailId=>$amount) {

//                    $expenseChartDetail = $this->getDoctrine()->getRepository(ExpenseChartDetail::class)->find($expenseChartDetailId);

                    $existingExpenseChartDetail=$this->getDoctrine()->getRepository(ExpenseChartDetail::class)->findOneBy(['expenseChart'=>$expenseChart, 'id'=>$expenseChartDetailId]);

                    if($existingExpenseChartDetail){
                        $expenseParticular=$existingExpenseChartDetail;
                    }else{
                        $expenseParticular= new ExpenseChartDetail();
                    }
                    $expenseParticular->setAmount($amount);
                    $em->persist($expenseParticular);
                }
            }
            $em->flush();

        }

        $expenseChartDetails=[];
        if($expenseChart->getExpenseChartDetails() && sizeof($expenseChart->getExpenseChartDetails())>0){
            foreach ($expenseChart->getExpenseChartDetails() as $expenseChartDetail) {
                $expenseChartDetails[$expenseChartDetail->getPaymentDuration()][]=$expenseChartDetail;
            }
        }

        $fixedDailyExpenseParticulars = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->getFixedDailyExpenseParticular();
        $areas = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->findBy(['settingType'=>'AREA', 'status'=>1], ['sortOrder'=>'ASC']);


        return $this->render('employee/expenseChart.html.twig', [
            'employee' => $employee,
            'expenseChart' => $expenseChart,
            'form' => $form->createView(),
            'expenseChartDetails'=>$expenseChartDetails,
            'fixedDailyExpenseParticulars'=>$fixedDailyExpenseParticulars,
            'areas'=>$areas,
        ]);
    }

    /**
     * @Route("/expense-chart-details/{expenseChart}/add-particular", methods={"POST"}, name="employee_expense_chart_detail_add", options={"expose"=true})
     * @param ExpenseChart $expenseChart
     * @return Response
     */
    public function addEmployeeExpenseChartDetailsForFixedParticular( Request $request, ExpenseChart $expenseChart)
    {
        $amount = $request->request->get('amount');
        $particular = $request->request->get('particular_id');
        $area = $request->request->get('area_id');
        $particularObj = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->find($particular);
        $areaObj = $this->getDoctrine()->getRepository(\Terminalbd\CrmBundle\Entity\Setting::class)->find($area);

        $expenseChartDetail = $this->getDoctrine()->getRepository(ExpenseChartDetail::class)->findOneBy(['expenseChart'=>$expenseChart, 'particular'=>$particularObj, 'area'=>$areaObj]);

        if($expenseChartDetail){
            return new JsonResponse(['status' => 400, 'message' => 'Already exist']);
        }

        $expenseParticular= new ExpenseChartDetail();

        $expenseParticular->setAmount($amount);
        $expenseParticular->setArea($areaObj);
        $expenseParticular->setParticular($particularObj);
        $expenseParticular->setExpenseChart($expenseChart);
        $expenseChart->addExpenseChartDetail($expenseParticular);
        $this->getDoctrine()->getManager()->persist($expenseParticular);
        $this->getDoctrine()->getManager()->flush();

        $expenseChartDetails=[];
        if($expenseChart->getExpenseChartDetails() && sizeof($expenseChart->getExpenseChartDetails())>0){
            foreach ($expenseChart->getExpenseChartDetails() as $expenseChartDetail) {
                $expenseChartDetails[$expenseChartDetail->getPaymentDuration()][]=$expenseChartDetail;
            }
        }

        $html = $this->renderView('employee/fixedExpenseTbody.html.twig',[
            'expenseChartDetails' => $expenseChartDetails,
        ]);

        return new JsonResponse([ 'html'=> $html , 'status' => 200, 'message' => 'success']);
    }

    /**
     * @Route("/expense-chart-details/remove", methods={"POST", "GET"}, name="employee_expense_chart_detail_remove", options={"expose"=true})
     * @param Request $request
     * @return Response
     */
    public function removeExpenseChartDetails( Request $request)
    {
        $expenseDetailId = $request->request->get('expenseChartDetailId');
        $expenseChartDetailRepository = $this->getDoctrine()->getRepository(ExpenseChartDetail::class);
        $expenseChartDetail = $expenseChartDetailRepository->find($expenseDetailId);

        $expenseChartDetailRepository->delete($expenseChartDetail);

        return new JsonResponse([ 'data'=>$expenseChartDetail->getId(), 'status' => 200, 'message' => 'success']);

    }



}
