<?php


namespace App\Controller\Sms;

use App\Entity\Admin\Location;
use App\Entity\SalesOrderDelivery;
use App\Entity\SalesOrderDeliveryAmount;
use App\Entity\SalesOrderDeliveryBatch;
use App\Entity\Sms\SmsLog;
use App\Entity\User;
use App\Form\Sms\SmsUploadFormType;
use App\Service\SmsSender;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Sms\SmsCampaign;

/**
 * Class SmsProcessController
 * @package App\Controller\Sms
 * @Route("/sms-center")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SMS_USER') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_SMS_ADMIN')")
 */
class SmsProcessController extends AbstractController
{
    /**
     * @Route("/", name="sms_center_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $user = $this->getUser();
        $records = $this->getDoctrine()->getRepository(SmsCampaign::class)->findBy(['createdBy' => $user]);
        if (in_array('ROLE_ADMIN', $user->getRoles())){
            $records = $this->getDoctrine()->getRepository(SmsCampaign::class)->findAll();
        }
        $records = $paginator->paginate($records,$request->query->getInt('page', 1), /*page number*/
            25 /*limit per page*/);
        return $this->render('smsCenter/index.html.twig', [
            'records' => $records
        ]);
    }

    /**
     * @Route("/new", name="sms_center_new")
     * @param Request $request
     * @param SmsSender $smsSender
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request, SmsSender $smsSender)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        $allowFileType = ['xlsx'];

        $campaign = new SmsCampaign();
        $form = $this->createForm(SmsUploadFormType::class, $campaign);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {

            $mobileNumbers = $form->get('mobileNumbers')->getData();
            $smsText = $form->get('message')->getData();

            $smsExcel = $form['uploadFile']->getData();
            $campaign->setCreatedBy($this->getUser());
            $em->persist($campaign);
            $em->flush();

            if ($smsExcel != null){
                if (in_array($smsExcel->getClientOriginalExtension(), $allowFileType)){
                    $originalFilename = pathinfo($smsExcel->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $smsExcel->getClientOriginalExtension();
                    $uploadDir = $this->getParameter('projectRoot') . '/public/uploads/sms/';
                    $newFileName = $form['campaignName']->getData() . '_' . $originalFilename . '-' . date('d-m-Y') . '-' . time() . '.' . $smsExcel->getClientOriginalExtension();

                    $smsExcel->move(
                        $uploadDir,
                        $newFileName
                    );

                    $reader = new Xlsx();
                    $reader->setReadDataOnly(true);
//                    $reader->setReadEmptyCells(false); // remove empty rows

                    $spreadSheet = $reader->load($uploadDir . $newFileName);

                    $excelSheet = $spreadSheet->getActiveSheet();

                    $allData = $excelSheet->toArray();
                    array_shift($allData); //remove header
                    foreach ($allData as $value){
                        $log = new SmsLog();
                        $log->setCampaign($campaign);
                        $log->setName(trim($value[0]));
                        $log->setMobile(trim($value[1]));
                        if($campaign->getMessage()){
                            $log->setMessage($campaign->getMessage());
                        }else{
                            $log->setMessage(trim($value[2]));
                        }
                        $log->setStatus(false);
                        $em->persist($log);
                        $em->flush();

                        if (!$request->request->has('save')){
                            $response = $smsSender->sendSms($log->getMessage(), $log->getMobile());

                            if ($response){
                                $log->setStatus(true);
                                $em->persist($log);
                                $em->flush();
                            }
                        }
                    }
                }
            }elseif (!$smsExcel && $mobileNumbers && $smsText ){
                $mobileNumbers = explode(',', $mobileNumbers);
                $mobileNumbers = array_map("trim", $mobileNumbers);

                foreach ($mobileNumbers as $number){
                    $log = new SmsLog();
                    $log->setCampaign($campaign);
                    $log->setMobile($number);
                    $log->setMessage($smsText);
                    $log->setStatus(false);
                    $em->persist($log);
                    $em->flush();

                    if (!$request->request->has('save')){
                        $response = $smsSender->sendSms($log->getMessage(), $log->getMobile());

                        if ($response){
                            $log->setStatus(true);
                            $em->persist($log);
                            $em->flush();
                        }
                    }
                }
            }

            if ($request->request->has('save')){
                $this->addFlash('success', 'Campaign Saved successfully!');
            }else{
                $campaign->setStatus(true);
                $em->persist($campaign);
                $em->flush();
                $this->addFlash('success', 'Save and Sent!');
            }

            return $this->redirectToRoute('sms_center_new');
        }

        return $this->render('smsCenter/new.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{campaign}/pending-campaign/send", name="send_sms")
     * @param SmsCampaign $campaign
     * @param SmsSender $smsSender
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendSms(SmsCampaign $campaign, SmsSender $smsSender)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($campaign->getSmsLog() as $person) {
            if (!$person->getName()){
                $message = $person->getMessage();
            }else{
                $message = 'Dear Sir: ' . $person->getName() . ', ' . $person->getMessage();
            }
            $response = $smsSender->sendSms($message, $person->getMobile());

            if ($response){
                $person->setStatus(true);
                $em->persist($person);
                $em->flush();
            }
        }

        $campaign->setStatus(true);
        $em->persist($campaign);
        $em->flush();

        $this->addFlash('success', 'Message Sent!');
        return $this->redirectToRoute('sms_center_index');
    }

    /**
     * @param SmsCampaign $campaign
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{campaign}/log", name="campaign_log")
     */
    public function campaignLog(SmsCampaign $campaign, PaginatorInterface $paginator, Request $request)
    {
        $logs = $campaign->getSmsLog();
        $logs = $paginator->paginate($logs, $request->query->getInt('page', 1), /*page number*/
            25 /*limit per page*/);
        return $this->render('smsCenter/logs.html.twig', [
            'logs' => $logs
        ]);
    }

    /**
     * @param SmsCampaign $campaign
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{campaign}/delete", name="delete_campaign")
     */
    public function deleteCampaign(SmsCampaign $campaign)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($campaign);
        $em->flush();
        $this->addFlash('success', 'Campaign has been removed successfully!');
        return $this->redirectToRoute('sms_center_index');
    }

    /**
     * @Route("/{batch}/region-manager/send", name="send_sms_to_regional_managers")
     * @param Request $request
     * @param SmsSender $smsSender
     * @param SalesOrderDeliveryBatch $batch
     * @return JsonResponse
     */
    public function sendSmsToRegionalManagers(Request $request, SmsSender $smsSender, SalesOrderDeliveryBatch $batch)
    {
        $managers = $this->getDoctrine()->getRepository(User::class)->findBy(['id' => $request->query->get('ids')]);
        $totalAmount = $this->getDoctrine()->getRepository(SalesOrderDelivery::class)->getTotalAmount($batch);
        $em = $this->getDoctrine()->getManager();

        foreach ($managers as $manager) {
            if ($manager->getUserGroup()->getSlug() != 'administrator'){
                $record = $this->getDoctrine()->getRepository(SalesOrderDeliveryAmount::class)->findOneBy(['batch' => $batch, 'manager' => $manager]);
                $message = 'Dear Concern, ' . $batch->getSyncDate()->format('jS M. Y') . ' till '. $batch->getSyncDate()->format('h:i A') .' your assigned area delivered product volume is ' . ($record->getTotalAmount() / 1000) . ' Ton';
                if ($manager->getMobile()){
                    $smsSender->sendSms($message, str_replace('-', '', $manager->getMobile()));
                    $record->setStatus(true);
                    $em->persist($record);
                    $em->flush();
                }
            }else{
                $message = 'Dear Concern, ' . $batch->getSyncDate()->format('jS M. Y') . ' till '. $batch->getSyncDate()->format('h:i A') .' your assigned area delivered product volume is ' . ($totalAmount / 1000) . ' Ton';

                if ($manager->getMobile()){
                    $smsSender->sendSms($message, str_replace('-', '', $manager->getMobile()));
                }
            }
        }
        
        $this->addFlash('success', 'Message Sent!');
        return new JsonResponse(['route' => 'sales_order_delivery']);
    }
}