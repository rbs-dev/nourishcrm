<?php

namespace App\Controller;

use App\Entity\Version;
use App\Form\VersionFormType;
use App\Repository\VersionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VersionController
 * @package App\Controller
 * @Route("/v")
 */
class VersionController extends AbstractController
{
    /**
     * @Route("/", name="version_index")
     * @param VersionRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(VersionRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $records = $repository->findBy([], ['id' => 'DESC']);
        $records = $paginator->paginate($records,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $this->render('version/index.html.twig', [
            'records' => $records,
        ]);
    }

    /**
     * @Route("/new", name="version_new")
     * @param Request $request
     * @param VersionRepository $repository
     * @return Response
     */
    public function new(Request $request, VersionRepository $repository)
    {
        $version = new Version();
        $form = $this->createForm(VersionFormType::class, $version)->remove('createdAt')->remove('updatedAt');
        $form->handleRequest($request);
        if ($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $previousVersions = $repository->findBy(['status' => 1]);
            if ($previousVersions){
                foreach ($previousVersions as $previousVersion) {
                    $previousVersion->setStatus(false);
                    $em->persist($previousVersion);
                    $em->flush();
                }
            }
            $em->persist($version);
            $em->flush();
            $this->addFlash('success', 'New version added successfully!');
            return $this->redirectToRoute('version_index');
        }

        return $this->render('version/new.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/delete", name="version_delete")
     * @param Version $id
     * @return JsonResponse
     */
    public function delete(Version $id)
    {
        $version = $id;
        $this->getDoctrine()->getManager()->remove($version);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'status' => 200,
            'message' => 'success'
        ]);
    }
}
