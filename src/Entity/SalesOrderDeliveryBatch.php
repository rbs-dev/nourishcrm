<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SalesOrderDeliveryBatchRepository;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=SalesOrderDeliveryBatchRepository::class)
 */
class SalesOrderDeliveryBatch
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SalesOrderDelivery", mappedBy="batch")
     */
    private $salesOrder;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $syncDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = false;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    //DELIVERED (shipped + partial shipped)
    // and PENDING (ready + partial shipped pending)
    /**
     * @ORM\Column(type="string", nullable=true, options={"default" : "DELIVERED"})
     */
    private $orderDeliveryStatus;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    public function __construct()
    {
        $this->salesOrder = new ArrayCollection();
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getSalesOrder(): ArrayCollection
    {
        return $this->salesOrder;
    }

    /**
     * @param ArrayCollection $salesOrder
     */
    public function setSalesOrder(ArrayCollection $salesOrder): void
    {
        $this->salesOrder = $salesOrder;
    }

    /**
     * @return mixed
     */
    public function getSyncDate()
    {
        return $this->syncDate;
    }

    /**
     * @param mixed $syncDate
     */
    public function setSyncDate($syncDate): void
    {
        $this->syncDate = $syncDate;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getOrderDeliveryStatus()
    {
        return $this->orderDeliveryStatus;
    }

    /**
     * @param mixed $orderDeliveryStatus
     */
    public function setOrderDeliveryStatus($orderDeliveryStatus): void
    {
        $this->orderDeliveryStatus = $orderDeliveryStatus;
    }

}
