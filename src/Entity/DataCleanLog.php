<?php

namespace App\Entity;

use App\Repository\DataCleanLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DataCleanLogRepository::class)
 */
class DataCleanLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $schemaName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $moduleName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $deviceIp;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $cleanedBy;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchemaName(): ?string
    {
        return $this->schemaName;
    }

    public function setSchemaName($schemaName): self
    {
        $this->schemaName = $schemaName;

        return $this;
    }

    public function getModuleName(): ?string
    {
        return $this->moduleName;
    }

    public function setModuleName(string $moduleName): self
    {
        $this->moduleName = $moduleName;

        return $this;
    }

    public function getDeviceIp(): ?string
    {
        return $this->deviceIp;
    }

    public function setDeviceIp(string $deviceIp): self
    {
        $this->deviceIp = $deviceIp;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCleanedBy()
    {
        return $this->cleanedBy;
    }

    /**
     * @param mixed $cleanedBy
     */
    public function setCleanedBy($cleanedBy): void
    {
        $this->cleanedBy = $cleanedBy;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
