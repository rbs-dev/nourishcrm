<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Entity\Admin\Bank;
use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\ItemKeyValue;
use App\Entity\Core\Profile;
use App\Entity\Core\Setting;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="core_user")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class User implements UserInterface, \Serializable, EquatableInterface
{
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->enabled !== $user->enabled) {
            return false;
        }
        return true;
    }

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\Profile", mappedBy="user", cascade={"persist", "remove"} )
     */
    protected $profile;

    /**
     * @ORM\OneToOne(targetEntity="Terminalbd\CrmBundle\Entity\ExpenseChart", mappedBy="employee")
     */
    protected $expenseChart;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\KpiBundle\Entity\EmployeeDistrictHistory", mappedBy="employee", cascade={"persist", "remove"} )
     */
    protected $districtHistories;


    /**
     * Many Users have Many Location.
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\Location", inversedBy="user")
     * @ORM\JoinTable(name="users_upozilas")
     *
     */
    protected $upozila;
    /**
     * @param Location $upozila
     */
    public function addUpozila(Location $upozila): void
    {
        if ($this->upozila->contains($upozila)){
            return;
        }
        $this->upozila->add($upozila);
        $upozila->addUser($this);
    }

    /**
     * Many Users have Many Location.
     * @ORM\ManyToMany(targetEntity="App\Entity\Admin\Location", inversedBy="userDistricts")
     * @ORM\JoinTable(name="users_districts",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="district_id", referencedColumnName="id")}
     *      )
     */
    private $district;

    public function __construct()
    {
        $this->upozila = new ArrayCollection();
        $this->district = new ArrayCollection();
        $this->fileUpload = new ArrayCollection();
    }


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Location")
     */
    protected $regional;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Location")
     */
    protected $zonal;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    protected $designation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    protected $department;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    protected $branch;

    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    protected $serviceMode;

    /**
     * @var Setting
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    protected $responsibleOf;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    protected $userGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    protected $reportMode;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $lineManager;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\ItemKeyValue", mappedBy="user" )
     */
    protected $itemKeyValues;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Terminal", inversedBy="users" , cascade={"detach","merge"} )
     * @ORM\JoinColumn(name="terminal_id", referencedColumnName="id", onDelete="cascade")
     **/
    protected $terminal;


    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="user.blank_content")
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    private $userId;


     /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $mobile;


    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $area;
    /**
     * @var array
     *
     * @ORM\Column(type="json" , nullable=true)
     */
    private $appRoles = [];


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $path;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\Email()
     */
    private $email;


    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $otp;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $joiningDate;

     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $dateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $effectiveDate;


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $salary;


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $typeOfVehicle;


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $vehicleNo;


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $speciality;


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $educationalQualification;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $trainingSkill;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $responsibility;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $assets;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $leaveStatus;


    /**
     * @var Bank
     * @ORM\ManyToOne(targetEntity="App\Entity\Admin\Bank")
     */
    protected $bank;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $bankBranch;


     /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $accountNumber;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $labs;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $userMode;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $termsCondition = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $enabled = true;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isDelete = false;


    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $lastPromotionDate;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $resignDate;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $longitude;

    /**
     * @var $fileUpload
     * @ORM\OneToMany(targetEntity="Terminalbd\BankReconciliationBundle\Entity\FileUpload", mappedBy="uploadedBy")
     * @ORM\JoinColumn(nullable= true)
     */
    private $fileUpload;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isPermanent = false;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $permanentDate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SalesDepot")
     */
    private $depot;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $crmLineManager;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable= true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable = true)
     */
    private $updatedAt;

    public function getId()
    {
        return $this->id;
    }


    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        // See "Do you need to use a Salt?" at https://symfony.com/doc/current/cookbook/security/entity_provider.html
        // we're using bcrypt in security.yml to encode the password, so
        // the salt value is built-in and you don't have to generate one

        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
        // if you had a plainPassword property, you'd nullify it here
        // $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        return serialize([$this->id, $this->username, $this->password]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        [$this->id, $this->username, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }



    /**
     * @return bool
     */
    public function isTermsCondition()
    {
        return $this->termsCondition;
    }

    /**
     * @param bool $termsCondition
     */
    public function setTermsCondition($termsCondition)
    {
        $this->termsCondition = $termsCondition;
    }

    /**
     * @return string
     */
    public function getOtp()
    {
        return $this->otp;
    }

    /**
     * @param string $otp
     */
    public function setOtp($otp)
    {
        $this->otp = $otp;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


    /**
     * @return string
     */
    public function getMobile(): ? string
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile(string $mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return array
     */
    public function getAppRoles(): ? array
    {
        $appRoles = $this->appRoles;

        // guarantees that a user always has at least one role for security
        if (empty($appRoles)) {
            $appRoles[] = 'ROLE_USER';
        }
        return array_unique($appRoles);
    }

    /**
     * @param string $appRoles
     */
    public function setAppRoles(array $appRoles) : void
    {
        $this->appRoles = $appRoles;
    }



    /**
     * @return string
     */
    public function getAppPassword(): ? string
    {
        return $this->appPassword;
    }

    /**
     * @param string $appPassword
     */
    public function setAppPassword(string $appPassword)
    {
        $this->appPassword = $appPassword;
    }

    /**
     * @return bool
     */
    public function isDelete(): ? bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete)
    {
        $this->isDelete = $isDelete;
    }


    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @return mixed
     */
    public function getExpenseChart()
    {
        return $this->expenseChart;
    }

    /**
     * @return Terminal
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param Terminal $terminal
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
    }

    /**
     * @return Setting
     */
    public function getDesignation(): ?Setting
    {
        return $this->designation;
    }

    /**
     * @param Setting|null $designation
     * @return void
     */
    public function setDesignation(?Setting $designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Setting
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param Setting $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getJoiningDate()
    {
        return $this->joiningDate;
    }

    /**
     * @param mixed $joiningDate
     */
    public function setJoiningDate($joiningDate): void
    {
        $this->joiningDate = $joiningDate;
    }

    /**
     * @return mixed
     */
    public function getPermanentDate()
    {
        return $this->permanentDate;
    }

    /**
     * @param mixed $permanentDate
     */
    public function setPermanentDate($permanentDate): void
    {
        $this->permanentDate = $permanentDate;
    }

    /**
     * @return mixed
     */
    public function getDepot()
    {
        return $this->depot;
    }

    /**
     * @param mixed $depot
     */
    public function setDepot($depot): void
    {
        $this->depot = $depot;
    }


    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getUserMode()
    {
        return $this->userMode;
    }

    /**
     * @param string $userMode
     */
    public function setUserMode($userMode)
    {
        $this->userMode = $userMode;
    }


    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return Setting
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }

    /**
     * @param Setting $userGroup
     */
    public function setUserGroup($userGroup)
    {
        $this->userGroup = $userGroup;
    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues()
    {
        return $this->itemKeyValues;
    }

    /**
     * @return string
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param string $dateOfBirth
     */
    public function setDateOfBirth(string $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return string
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @param string $effectiveDate
     */
    public function setEffectiveDate(string $effectiveDate)
    {
        $this->effectiveDate = $effectiveDate;
    }

    /**
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param string $salary
     */
    public function setSalary(string $salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getTypeOfVehicle()
    {
        return $this->typeOfVehicle;
    }

    /**
     * @param string $typeOfVehicle
     */
    public function setTypeOfVehicle(string $typeOfVehicle)
    {
        $this->typeOfVehicle = $typeOfVehicle;
    }

    /**
     * @return string
     */
    public function getVehicleNo()
    {
        return $this->vehicleNo;
    }

    /**
     * @param string $vehicleNo
     */
    public function setVehicleNo($vehicleNo)
    {
        $this->vehicleNo = $vehicleNo;
    }

    /**
     * @return string
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * @param string $speciality
     */
    public function setSpeciality(string $speciality)
    {
        $this->speciality = $speciality;
    }

    /**
     * @return string
     */
    public function getEducationalQualification()
    {
        return $this->educationalQualification;
    }

    /**
     * @param string $educationalQualification
     */
    public function setEducationalQualification(string $educationalQualification)
    {
        $this->educationalQualification = $educationalQualification;
    }

    /**
     * @return string
     */
    public function getTrainingSkill()
    {
        return $this->trainingSkill;
    }

    /**
     * @param string $trainingSkill
     */
    public function setTrainingSkill(string $trainingSkill)
    {
        $this->trainingSkill = $trainingSkill;
    }

    /**
     * @return string
     */
    public function getResponsibility()
    {
        return $this->responsibility;
    }

    /**
     * @param string $responsibility
     */
    public function setResponsibility(string $responsibility)
    {
        $this->responsibility = $responsibility;
    }

    /**
     * @return string
     */
    public function getAssets()
    {
        return $this->assets;
    }

    /**
     * @param string $assets
     */
    public function setAssets(string $assets)
    {
        $this->assets = $assets;
    }

    /**
     * @return string
     */
    public function getLeaveStatus()
    {
        return $this->leaveStatus;
    }

    /**
     * @param string $leaveStatus
     */
    public function setLeaveStatus(string $leaveStatus)
    {
        $this->leaveStatus = $leaveStatus;
    }

    /**
     * @return Bank
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param Bank $bank
     */
    public function setBank(Bank $bank)
    {
        $this->bank = $bank;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber(string $accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }


    /**
     * @return ArrayCollection
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }

    /**
     * @return Location
     */
    public function getRegional()
    {
        return $this->regional;
    }

    /**
     * @param Location $regional
     */
    public function setRegional($regional)
    {
        $this->regional = $regional;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getBankBranch()
    {
        return $this->bankBranch;
    }

    /**
     * @param string $bankBranch
     */
    public function setBankBranch(string $bankBranch)
    {
        $this->bankBranch = $bankBranch;
    }

    /**
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param string $area
     */
    public function setArea(string $area)
    {
        $this->area = $area;
    }

    /**
     * @return Location
     */
    public function getUpozila()
    {
        return $this->upozila;
    }

    /**
     * @param Location $upozila
     */
    public function setUpozila($upozila)
    {
        $this->upozila = $upozila;
    }

    /**
     * @return Location
     */
    public function getZonal()
    {
        return $this->zonal;
    }

    /**
     * @param Location $zonal
     */
    public function setZonal($zonal)
    {
        $this->zonal = $zonal;
    }

    /**
     * @return Setting
     */
    public function getServiceMode()
    {
        return $this->serviceMode;
    }

    /**
     * @param Setting $serviceMode
     */
    public function setServiceMode($serviceMode)
    {
        $this->serviceMode = $serviceMode;
    }

    /**
     * @return Setting
     */
    public function getResponsibleOf()
    {
        return $this->responsibleOf;
    }

    /**
     * @param Setting $responsibleOf
     */
    public function setResponsibleOf($responsibleOf)
    {
        $this->responsibleOf = $responsibleOf;
    }

    /**
     * @return mixed
     */
    public function getReportMode()
    {
        return $this->reportMode;
    }

    /**
     * @param mixed $reportMode
     */
    public function setReportMode($reportMode)
    {
        $this->reportMode = $reportMode;
    }

    /**
     * @return User
     */
    public function getLineManager()
    {
        return $this->lineManager;
    }

    /**
     * @param User $lineManager
     */
    public function setLineManager($lineManager)
    {
        $this->lineManager = $lineManager;
    }

    /**
     * @return string
     */
    public function getNameDesignation()
    {
        $name= $this->getName();
        $designation= $this->getDesignation()->getName();
        return $name .' ('.$designation.')';
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLastPromotionDate()
    {
        return $this->lastPromotionDate;
    }

    /**
     * @param mixed $lastPromotionDate
     */
    public function setLastPromotionDate($lastPromotionDate): void
    {
        $this->lastPromotionDate = $lastPromotionDate;
    }

    /**
     * @return mixed
     */
    public function getResignDate()
    {
        return $this->resignDate;
    }

    /**
     * @param mixed $resignDate
     */
    public function setResignDate($resignDate): void
    {
        $this->resignDate = $resignDate;
    }


    /**
     * @return mixed
     */
    public function getDistrictHistories()
    {
        return $this->districtHistories;
    }

    /**
     * @return mixed
     */
    public function getFileUpload(): Collection
    {
        return $this->fileUpload;
    }

    /**
     * @param mixed $fileUpload
     */
    public function setFileUpload($fileUpload): void
    {
        $this->fileUpload = $fileUpload;
    }

    /**
     * @return bool
     */
    public function isPermanent(): bool
    {
        return $this->isPermanent;
    }

    /**
     * @param bool $isPermanent
     */
    public function setIsPermanent(bool $isPermanent): void
    {
        $this->isPermanent = $isPermanent;
    }

    /**
     * @return string
     */
    public function getLabs()
    {
        if($this->labs){
            return json_decode($this->labs);
        }
        return $this->labs;
    }

    /**
     * @param string $labs
     */
    public function setLabs($labs): void
    {
        $this->labs = $labs;
    }

    public function getCrmLineManager(): ?self
    {
        return $this->crmLineManager;
    }

    public function setCrmLineManager(?self $crmLineManager): self
    {
        $this->crmLineManager = $crmLineManager;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     */
    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }



}
