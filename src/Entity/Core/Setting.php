<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Core;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\SettingRepository")
 * @ORM\Table(name="core_setting")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Setting
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="terminal", type="integer", nullable=true)
     */
    private $terminal;


    /**
     * @var SettingType
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\SettingType" , inversedBy="settings")
     */
    private $settingType;

    /**
     * @var ItemKeyValue
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Core\ItemKeyValue" , mappedBy="setting")
     */
    private $itemKeyValues;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $nameBn;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false, nullable=true)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getNameBn()
    {
        return $this->nameBn;
    }

    /**
     * @param string $nameBn
     */
    public function setNameBn($nameBn)
    {
        $this->nameBn = $nameBn;
    }

    public function getNameBanglaEnglish(){

        return $this->nameBn.' - '.$this->name;
    }

    /**
     * @return SettingType
     */
    public function getSettingType()
    {
        return $this->settingType;
    }

    /**
     * @param SettingType $settingType
     */
    public function setSettingType(SettingType $settingType)
    {
        $this->settingType = $settingType;
    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues()
    {
        return $this->itemKeyValues;
    }

    /**
     * @return integer
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @param integer $terminal
     */
    public function setTerminal(int $terminal)
    {
        $this->terminal = $terminal;
    }



}
