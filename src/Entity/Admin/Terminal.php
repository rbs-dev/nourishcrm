<?php

namespace App\Entity\Admin;


use App\Entity\Core\Company;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * GlobalOption
 * @UniqueEntity(fields="domain",message="This domain is already in use.")
 * @UniqueEntity(fields="mobile",message="This mobile is already in use.")
 * @UniqueEntity(fields="subDomain",message="This sub-domain is already in use.")
 * @ORM\Table("terminal")
 * @ORM\Entity(repositoryClass="App\Repository\Admin\TerminalRepository")
 */
class Terminal
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppModule", inversedBy="appDomains" , cascade={"detach","merge"} )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected $mainApp;

    /**
     * @ORM\ManyToMany(targetEntity="AppModule", inversedBy="terminals")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected $appModules;


    /**
     * @return AppModule
     */
    public function getMainApp()
    {
        return $this->mainApp;
    }

    /**
     * @param AppModule $mainApp
     */
    public function setMainApp($mainApp)
    {
        $this->mainApp = $mainApp;
    }

    /**
     * @return AppModule
     */
    public function getAppModules()
    {
        return $this->appModules;
    }

    /**
     * @param AppModule $appModules
     */
    public function setAppModules($appModules)
    {
        $this->appModules = $appModules;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }


}
