<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Admin;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\CountryRepository")
 * @ORM\Table(name="core_country")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Country
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $nickname;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $iso;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $iso3;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $numcode;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $phoneCode;


    /**
     * @Gedmo\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false)
     */
    private $slug;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }



    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return string
     */
    public function getIso3(): string
    {
        return $this->iso3;
    }

    /**
     * @param string $iso3
     */
    public function setIso3(string $iso3)
    {
        $this->iso3 = $iso3;
    }

    /**
     * @return string
     */
    public function getNumcode(): string
    {
        return $this->numcode;
    }

    /**
     * @param string $numcode
     */
    public function setNumcode(string $numcode)
    {
        $this->numcode = $numcode;
    }

    /**
     * @return string
     */
    public function getPhoneCode(): string
    {
        return $this->phoneCode;
    }

    /**
     * @param string $phoneCode
     */
    public function setPhoneCode(string $phoneCode)
    {
        $this->phoneCode = $phoneCode;
    }

    /**
     * @return string
     */
    public function getIso(): string
    {
        return $this->iso;
    }

    /**
     * @param string $iso
     */
    public function setIso(string $iso)
    {
        $this->iso = $iso;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }


}
