<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity\Admin;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Admin\BankRepository")
 * @ORM\Table(name="core_bank")
 * @UniqueEntity(fields={"name"}, message="Bank already exists,Please try again.")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Bank
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;


    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @Doctrine\ORM\Mapping\Column(length=255, unique=false, nullable=true)
     */
    private $slug;
//    /**
//     * @Gedmo\Slug(fields={"name"})
//     * @Doctrine\ORM\Mapping\Column(length=255)
//     */
//    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankBranch", mappedBy="bank")
     */
    private $branches;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankTransaction", mappedBy="bank")
     */
    private $transactions;

    /**
     * @ORM\OneToMany(targetEntity="Terminalbd\BankReconciliationBundle\Entity\BankAccountCode", mappedBy="bank")
     */
    private $accountCode;



    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $deductionPercentage;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updatedAt;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    public function __construct()
    {
        $this->branches = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->accountCode = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }



    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return ArrayCollection
     */
    public function getBranches()
    {
        return $this->branches;
    }

    /**
     * @param ArrayCollection $branches
     */
    public function setBranches(ArrayCollection $branches): void
    {
        $this->branches = $branches;
    }

    /**
     * @return ArrayCollection
     */
    public function getTransactions(): ArrayCollection
    {
        return $this->transactions;
    }

    /**
     * @param ArrayCollection $transactions
     */
    public function setTransactions(ArrayCollection $transactions): void
    {
        $this->transactions = $transactions;
    }

    /**
     * @return ArrayCollection
     */
    public function getAccountCode(): ArrayCollection
    {
        return $this->accountCode;
    }

    /**
     * @param ArrayCollection $accountCode
     */
    public function setAccountCode(ArrayCollection $accountCode): void
    {
        $this->accountCode = $accountCode;
    }


    /**
     * @return string
     */
    public function getDeductionPercentage()
    {
        return $this->deductionPercentage;
    }

    /**
     * @param string $deductionPercentage
     */
    public function setDeductionPercentage( $deductionPercentage)
    {
        $this->deductionPercentage = $deductionPercentage;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


}
