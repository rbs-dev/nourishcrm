<?php


namespace App\Entity\Sms;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Sms\SmsCampaignRepository")
 * @ORM\Table(name="sms_campaign")
 */
class SmsCampaign
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var $smsLog
     * @ORM\OneToMany(targetEntity="App\Entity\Sms\SmsLog", mappedBy="campaign", cascade={"remove"})
     */
    private $smsLog;

    /**
     * @var $createdBy
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="smscampaign")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $campaignName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $message;
    

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable = true)
     */
    private $status = false;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->smsLog = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSmsLog()
    {
        return $this->smsLog;
    }

    /**
     * @param mixed $smsLog
     */
    public function setSmsLog($smsLog): void
    {
        $this->smsLog = $smsLog;
    }


    /**
     * @return mixed
     */
    public function getCampaignName()
    {
        return $this->campaignName;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }


    /**
     * @param mixed $campaignName
     */
    public function setCampaignName($campaignName): void
    {
        $this->campaignName = $campaignName;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

}