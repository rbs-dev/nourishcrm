<?php

namespace App\Repository;

use App\Entity\SalesOrderDelivery;
use App\Entity\SalesOrderDeliveryBatch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SalesOrderDeliveryBatch|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalesOrderDeliveryBatch|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalesOrderDeliveryBatch[]    findAll()
 * @method SalesOrderDeliveryBatch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalesOrderDeliveryBatchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesOrderDeliveryBatch::class);
    }


    public function deleteRecord(SalesOrderDeliveryBatch $batch)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $query = $qb->delete(SalesOrderDelivery::class, 's')->where('s.batch = ?1')->setParameter(1, $batch)->getQuery();
        if($query->execute()){
            $em->remove($batch);
            $em->flush();
            return true;
        }else{
            return false;
        }
    }

    public function getSalesOrderDeliveryBatchByDateRange($requestDate, $orderDeliveryStatus){

        $startDate =$requestDate.' 00:00:00';
        $endDate = $requestDate.' 23:59:59';

        $qb = $this->createQueryBuilder('e');

        $qb->where('e.syncDate >= :startDate')->setParameter('startDate', $startDate);
        $qb->andWhere('e.syncDate <= :endDate')->setParameter('endDate', $endDate);
        $qb->andWhere('e.orderDeliveryStatus = :orderDeliveryStatus')->setParameter('orderDeliveryStatus', $orderDeliveryStatus);

       return $qb->getQuery()->getOneOrNullResult();
    }
}
