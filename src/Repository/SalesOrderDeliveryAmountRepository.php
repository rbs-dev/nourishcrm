<?php

namespace App\Repository;

use App\Entity\SalesOrderDeliveryAmount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SalesOrderDeliveryAmount|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalesOrderDeliveryAmount|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalesOrderDeliveryAmount[]    findAll()
 * @method SalesOrderDeliveryAmount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalesOrderDeliveryAmountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesOrderDeliveryAmount::class);
    }

}
