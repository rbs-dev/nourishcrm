<?php

namespace App\Repository;

use App\Entity\DataCleanLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DataCleanLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataCleanLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataCleanLog[]    findAll()
 * @method DataCleanLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataCleanLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataCleanLog::class);
    }

    // /**
    //  * @return DataCleanLog[] Returns an array of DataCleanLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DataCleanLog
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
