<?php


namespace App\Repository\Sms;
use Doctrine\ORM\EntityRepository;


class SmsLogRepository extends EntityRepository
{
    public function getSmsLog()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.campaign', 'campaign');
        $qb->select('campaign.campaignName');
        $qb->addSelect('e.name', 'e.mobile', 'e.message', 'e.status');

        $results = $qb->getQuery()->getArrayResult();
        $data = [];
        foreach ($results as $result) {
            $data[$result['campaignName']][] = $result;
        }
        return $data;
    }
}