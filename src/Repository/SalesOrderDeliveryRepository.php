<?php

namespace App\Repository;

use App\Entity\SalesOrderDelivery;
use App\Entity\SalesOrderDeliveryBatch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SalesOrderDelivery|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalesOrderDelivery|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalesOrderDelivery[]    findAll()
 * @method SalesOrderDelivery[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalesOrderDeliveryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesOrderDelivery::class);
    }

    public function getOrderByDistricts($districtId, SalesOrderDeliveryBatch $batch)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.district', 'district');
        $qb->join('e.batch', 'batch');
        $qb->select('e.itemName', 'SUM(e.totalQuantity) AS totalQuantity');
//        $qb->addSelect('district.id AS districtId', 'district.name AS districtName');
        $qb->where('batch.id = :batchId')->setParameter('batchId', $batch->getId());
//        $qb->where("e.status = false");
        $qb->andWhere('district.id IN (:districtId)')->setParameter('districtId', $districtId);
        $qb->groupBy('e.itemName');

        $results =  $qb->getQuery()->getArrayResult();

        $data = [];
        foreach ($results as $result){
            $data[$result['itemName']] = $result['totalQuantity'];
        }

        return $data;
    }

    public function getTotalAmount(SalesOrderDeliveryBatch $batch)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.totalQuantity)');
        $qb->where('e.batch = :batch')->setParameter('batch', $batch);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
