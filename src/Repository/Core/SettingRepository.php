<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;

use App\Entity\Core\Setting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class SettingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Setting::class);
    }

    public function getChildRecords($parent)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.settingType','p');
        $qb->select('e.id as id','e.name as name','e.slug as slug');
        $qb->where('p.slug = :slug')->setParameter('slug',$parent);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getDesignations()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.name')
            ->join("e.settingType","st")
            ->where("st.slug ='designation'")
            ->orderBy('e.name', 'ASC');
        $results = $qb->getQuery()->getArrayResult();
        $returnArray=[];
        foreach ($results as $result){
            $returnArray[$result['id']]=$result['name'];
        }
        return $returnArray;
    }

    public function getReportModes()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.name');
        $qb->join("e.settingType","st")
            ->where("st.slug ='report-mode'")
            ->orderBy('e.name', 'ASC');
        $results = $qb->getQuery()->getArrayResult();
        $returnArray=[];
        foreach ($results as $result){
            $returnArray[$result['id']]=$result['name'];
        }
        return $returnArray;
    }

}
