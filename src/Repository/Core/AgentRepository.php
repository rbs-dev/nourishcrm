<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Core;
use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Agent;
use App\Entity\Core\Setting;
use App\Entity\User;
use App\Service\ConfigureManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Terminalbd\CrmBundle\Entity\CrmVisit;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class AgentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Agent::class);
    }

    public function getAgentSelect2($data)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.agentGroup','g');
        $qb->select('e.id as id','e.name as name','e.agentId as agentId');
        $qb->addSelect('g.name as groupName');

        if(!empty($data)){
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like("e.name", "'%$data%'"),
                    $qb->expr()->like("e.agentId", "'$data%'")
                )

            );
        }
        $qb->orderBy('e.name', 'ASC');

        $results = $qb->getQuery()->getArrayResult();

        $returnArray = [];
        foreach ($results as $result){
            $returnArray[$result['id']]= '('.$result['agentId'].') '.$result['name']. '('.$result['groupName'].')';
        }
        return $returnArray;

    }


    public function getAgentSelect2ForReconciliation($data,$modeObj)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.agentGroup','g');
        $qb->select('e.id as id','e.name as name','e.agentId as agentId');
        $qb->addSelect('g.name as groupName');

        if(!empty($data)){
            $qb->where("e.agentGroup = :agentGroup")->setParameter('agentGroup',$modeObj);
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like("e.name", "'%$data%'"),
                    $qb->expr()->like("e.agentId", "'$data%'")
                )
            );
        }

        $qb->orderBy('e.name', 'ASC');

        $results = $qb->getQuery()->getArrayResult();

        $returnArray = [];
        foreach ($results as $result){
            $returnArray[$result['id']]= '('.$result['agentId'].') '.$result['name']. '('.$result['groupName'].')';
        }
        return $returnArray;

    }

    public function getLocationWise(User $user)
    {

        $arrs = array();
        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->join('e.agentGroup','g');
        $qb->select('e.id as id','e.name as name');
        $qb->addSelect('g.name as groupName');
        $qb->where('g.slug != :slug')->setParameter('slug','other-agent');
        $qb->andWhere('g.slug != :subAgentSlug')->setParameter('subAgentSlug','sub-agent');
        $qb->andWhere('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function getVisitingLocationWise(User $user, CrmVisit $crmVisit)
    {

        $arrs = array();
        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->join('e.agentGroup','g');
        $qb->select('e.id as id','e.name as name','e.agentId');
        $qb->addSelect('g.name as groupName');
        $qb->where('g.slug != :slug')->setParameter('slug','other-agent');
        $qb->andWhere('g.slug != :subAgentSlug')->setParameter('subAgentSlug','sub-agent');
        $qb->andWhere('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $qb->andWhere('location.id = :visitingUpozila')->setParameter('visitingUpozila',$crmVisit->getLocation()->getId());
        $qb->orderBy('e.name', 'ASC');

        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function getLocationAndGroupWise(User $user, $agentGroup)
    {

        $arrs = array();
        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->join('e.agentGroup','g');
        $qb->select('e.id as id','e.name as name');
        $qb->addSelect('g.name as groupName');
        $qb->where('g.slug = :slug')->setParameter('slug',$agentGroup);
        $qb->andWhere('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function getLocationWiseForOptions(User $user)
    {

        $arrs = array();
        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->join('e.agentGroup','g');
        $qb->select('e.id as id','e.name as name');
        $qb->addSelect('g.name as groupName');
        $qb->andWhere('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $qb->orderBy('e.name', 'ASC');
        $results = $qb->getQuery()->getArrayResult();
        $arrayData = array();

        foreach ($results as $result) {
            $arrayData[$result['name']] = $result['id'];
        }

        return $arrayData;

    }


    public function getLocationWiseAgentForm(User $user)
    {

        $arrs = array();

        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }
//        dd($arrs);
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->where('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function getLocationWiseAgentWithoutOtherAgentForm(User $user)
    {

        $arrs = array();

        foreach ($user->getUpozila() as $location){
            $arrs[] = $location->getId();
        }
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.upozila','location');
        $qb->join('e.agentGroup','g');
        $qb->where('g.slug != :slug')->setParameter('slug','other-agent');
        $qb->andWhere('g.slug != :subAgentSlug')->setParameter('subAgentSlug','sub-agent');

        $qb->andWhere('location.id IN (:upozils)')->setParameter('upozils',$arrs);
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getResult();
        return $result;

    }


    public function totalCount($parameter)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.agentGroup','bt');
        $qb->leftJoin('e.upozila','l');
        $qb->leftJoin('l.parent','p');
        $qb->select('COUNT(e.id) as count');

        if($parameter['name']!=''){
            $qb->andWhere(
                $qb->expr()->like('e.name', ':name')
            )->setParameter(':name', '%' . $parameter['name'] . '%');
        }

        if($parameter['type']>0){
            $qb->andWhere('bt.id = :typeId')->setParameter('typeId', $parameter['type']);
        }else{
            $qb->andWhere('bt.id > :typeId')->setParameter('typeId', 0);
        }

        if($parameter['email']!=''){
            $qb->andWhere(
                $qb->expr()->like('e.email', ':email')
            )->setParameter(':email', $parameter['email'] . '%');
        }

        if($parameter['mobile']!=''){
            $qb->andWhere(
                $qb->expr()->like('e.mobile', ':mobile')
            )->setParameter(':mobile', '%' . $parameter['mobile'] . '%');
        }

        if($parameter['location']!=''){
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->like('l.name', ':locationName'),
                $qb->expr()->like('p.name', ':locationName')
            ))->setParameter(':locationName', $parameter['location'] . '%');
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function checkAvailable($terminal , $mode,$data)
    {
        $process = "true";
        $mobile             = isset($data['mobile']) ? $data['mobile'] :'';

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->where('e.terminal =:terminal')->setParameter("terminal",$terminal);
        if(!empty($mobile)){
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($mobile);
            $qb->andWhere('e.mobile =:mobile')->setParameter("mobile",$mobile);
        }
        $count = $qb->getQuery()->getOneOrNullResult();
        if($mode == "creatable"){
            if ($count['count'] == 1 ){
                $process="false";
            }
        }elseif($mode == "editable") {
            if ($count['count'] > 1 ){
                $process="false";
            }
        }
        return $process;

    }

    protected function handleSearchBetween($qb,$data)
    {

        $name = isset($data['name'])? $data['name'] :'';
        $username = isset($data['username'])? $data['username'] :'';
        $email = isset($data['email'])? $data['email'] :'';
        $mobile = isset($data['mobile'])? $data['mobile'] :'';

        if(!empty($name)){
            $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
        }
        if(!empty($username)){
            $qb->andWhere($qb->expr()->like("e.username", "'%$username%'"));
        }
        if(!empty($email)){
            $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
        }
        if(!empty($mobile)){
            $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
        }

    }

    public function systemDefaultCustomer( Terminal $terminal){

        $em = $this->_em;
        $user = new Agent();
        $user->setName("Default");
        $user->setMobile($terminal->getMobile());
        if($terminal->getEmail()){
            $user->setEmail($terminal->getEmail());
        }
        $em->persist($user);
        $em->flush();
    }


    /**
     * Agent
     */
    public function findBySearchQuery( $terminal, $parameter , $data ): array
    {


        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }

        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.agentGroup','bt');
        $qb->leftJoin('e.upozila','l');
        $qb->leftJoin('l.parent','p');
        $qb->select('e.id as id','e.name as name','e.mobile as mobile','e.email as email','e.name as companyName','e.agentId as agentId');
        $qb->addSelect('bt.name as businessType');
        $qb->addSelect('l.name as thana');
        $qb->addSelect('p.name as district');
       // $qb->where('e.terminal = :terminal')->setParameter('terminal',$terminal);
        if($parameter['name']!=''){
            $qb->andWhere(
                $qb->expr()->like('e.name', ':name')
            )->setParameter(':name', '%' . $parameter['name'] . '%');
        }
        if($parameter['agentId']!=''){
            $qb->andWhere(
                $qb->expr()->like('e.agentId', ':agentId')
            )->setParameter(':agentId', $parameter['agentId'] . '%');
        }

        if($parameter['email']!=''){
            $qb->andWhere(
                $qb->expr()->like('e.email', ':email')
            )->setParameter(':email', $parameter['email'] . '%');
        }

        if($parameter['mobile']!=''){
            $qb->andWhere(
                $qb->expr()->like('e.mobile', ':mobile')
            )->setParameter(':mobile', '%' . $parameter['mobile'] . '%');
        }

        if($parameter['location']!=''){
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->like('l.name', ':locationName'),
                $qb->expr()->like('p.name', ':locationName')
            ))->setParameter(':locationName', $parameter['location'] . '%');
        }

        if($parameter['type']>0){
            $qb->andWhere('bt.id = :typeId')->setParameter('typeId', $parameter['type']);
        }else{
            $qb->andWhere('bt.id > :typeId')->setParameter('typeId', 0);
        }
        //    $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.id', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function apiInsert($data)
    {
        $em = $this->_em;
        foreach ($data as $key => $value){
            $date = new \DateTime($data[$key]['created']);
            $entity = new Agent();
            $group=null;
            if($data[$key]['agentType']){
                $slug = strtolower($data[$key]['agentType']);
                $group = $em->getRepository(Setting::class)->findOneBy(array('slug' => $slug));
            }
//            $exitAgent = $this->findOneBy(array('oldId'=>$data[$key]['id']));
            $exitAgent = $this->findOneBy(array('agentId'=>$data[$key]['agentCodeForDatatable'], 'agentGroup'=>$group));

            if($exitAgent){
                $entity=$exitAgent;
            }

            $entity->setOldId($data[$key]['id']);
            $entity->setAgentId($data[$key]['agentCodeForDatatable']);
            $entity->setName($data[$key]['fullName']);
            $entity->setAddress($data[$key]['address']);
            $entity->setMobile($data[$key]['phone']);
            $entity->setEmail($data[$key]['email']);
            $entity->setCreated($date);
            if($data[$key]['agentType']){
                $slug = strtolower($data[$key]['agentType']);
                $group = $em->getRepository(Setting::class)->findOneBy(array('slug' => $slug));
                $entity->setAgentGroup($group);
            }
            if($data[$key]['upozila']){
                $upozila = trim($data[$key]['upozila']);
                $location = $em->getRepository(Location::class)->findOneBy(array('oldId' => $upozila));
                $entity->setUpozila($location);
                $entity->setDistrict($location->getParent());
            }
            $em->persist($entity);
            $em->flush();
        }
    }

    public function getAllAgents()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.upozila', 'upozila');
        $qb->leftJoin('e.district', 'district');
        $qb->leftJoin('e.agentGroup', 'agentGroup');
        $qb->select('e.name AS agentName', 'e.email AS agentEmail', 'e.mobile AS agentMobile');
        $qb->addSelect('agentGroup.name AS agentGroupName');
        $qb->addSelect('upozila.name AS agentUpozilaName');
        $qb->addSelect('district.name AS agentDistrictName');

        return $qb->getQuery()->getArrayResult();
    }
    public function getSearchAgent($filterBy)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.upozila', 'upozila');
        $qb->leftJoin('e.district', 'district');
        $qb->leftJoin('e.agentGroup', 'agentGroup');
        $qb->select('e.name AS agentName', 'e.email AS agentEmail', 'e.mobile AS agentMobile');
        $qb->addSelect('agentGroup.name AS agentGroupName');
        $qb->addSelect('upozila.name AS agentUpozilaName');
        $qb->addSelect('district.name AS agentDistrictName');
        if (isset($filterBy['agentId'])){
            $qb->andWhere('e.id = :id')->setParameter('id', $filterBy['agentId']);
        }
        if (isset($filterBy['agentMobile'])){
            $qb->andWhere('e.mobile LIKE :mobile')->setParameter('mobile', '%'. $filterBy['agentMobile'] .'%');
        }
        if (isset($filterBy['agentEmail'])){
            $qb->andWhere('e.email LIKE :email')->setParameter('email', '%'. $filterBy['agentEmail'] .'%');
        }
        return $qb->getQuery()->getArrayResult();
    }

    public function getAgentByIds($ids)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id as id','e.agentId', 'e.name as agentName');
        $qb->where('e.id IN (:ids)')->setParameter('ids',$ids);
        $qb->andWhere('e.status = :status')->setParameter('status', 1);
        $qb->orderBy('e.name', 'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }
}
