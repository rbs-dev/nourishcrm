<?php

namespace App\Repository\Core;
use App\Entity\Core\ItemKeyValue;
use App\Entity\Core\Setting;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;


/**
 * ItemKeyValueRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ItemKeyValueRepository extends EntityRepository
{

    public function insertUserKeyValue(User $reEntity,$data)
    {

        $em = $this->_em;
        $i=0;
        if(isset($data['metaKey']) OR isset($data['metaValue']) ){
            foreach ($data['metaKey'] as $value) {
                $metaId = isset($data['metaId'][$i]) ? $data['metaId'][$i] : '' ;
                $itemKeyValue = $this->findOneBy(array('user' => $reEntity,'id' => $metaId));
                if(!empty($metaId) and !empty($itemKeyValue)){
                    $this->updateMetaAttribute($itemKeyValue,$data['metaKey'][$i],$data['metaValue'][$i]);
                }elseif(($data['metaKey'][$i] != "" and empty($metaId))){
                    $entity = new ItemKeyValue();
                    $entity->setMetaKey($data['metaKey'][$i]);
                    $entity->setMetaValue($data['metaValue'][$i]);
                    $entity->setUser($reEntity);
                    $em->persist($entity);
                    $em->flush($entity);
                }
                $i++;
            }
        }
    }

    public function insertSettingKeyValue(Setting $reEntity,$data)
    {

        $em = $this->_em;
        $i=0;
        if(isset($data['metaKey']) OR isset($data['metaValue']) ){
            foreach ($data['metaKey'] as $value) {
                $metaId = isset($data['metaId'][$i]) ? $data['metaId'][$i] : '' ;
                $itemKeyValue = $this->findOneBy(array('setting'=>$reEntity,'id' => $metaId));
                if(!empty($metaId) and !empty($itemKeyValue)){
                    $this->updateMetaAttribute($itemKeyValue,$data['metaKey'][$i],$data['metaValue'][$i]);
                }elseif(($data['metaKey'][$i] != "" and empty($metaId))){
                    $entity = new ItemKeyValue();
                    $entity->setMetaKey($data['metaKey'][$i]);
                    $entity->setMetaValue($data['metaValue'][$i]);
                    $entity->setSetting($reEntity);
                    $em->persist($entity);
                    $em->flush($entity);
                }
                $i++;
            }
        }
    }

    public function insertBatchKeyValue(Setting $reEntity,$data)
    {

        $em = $this->_em;
        $i=0;
        if(isset($data['metaKey']) OR isset($data['metaValue']) ){
            foreach ($data['metaKey'] as $value) {
                $metaId = isset($data['metaId'][$i]) ? $data['metaId'][$i] : '' ;
                $itemKeyValue = $this->findOneBy(array('setting'=>$reEntity,'id' => $metaId));
                if(!empty($metaId) and !empty($itemKeyValue)){
                    $this->updateMetaAttribute($itemKeyValue,$data['metaKey'][$i],$data['metaValue'][$i]);
                }elseif(($data['metaKey'][$i] != "" and empty($metaId))){
                    $entity = new ItemKeyValue();
                    $entity->setMetaKey($data['metaKey'][$i]);
                    $entity->setMetaValue($data['metaValue'][$i]);
                    $entity->setSetting($reEntity);
                    $em->persist($entity);
                    $em->flush($entity);
                }
                $i++;
            }
        }
    }

    public function updateMetaAttribute(ItemKeyValue $itemKeyValue , $key , $value ='')
    {
        $em = $this->_em;
        $itemKeyValue->setMetaKey($key);
        $itemKeyValue->setMetaValue($value);
        $em->flush();
    }

    public function setDivOrdering($data)
    {
        $i = 1;
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        foreach ($data as $key => $value){
            $qb->update('TerminalbdInventoryBundle:ItemKeyValue', 'mg')
                ->set('mg.sorting', $i)
                ->where('mg.id = :id')
                ->setParameter('id', $key)
                ->getQuery()
                ->execute();
            $i++;

        }

    }

}
