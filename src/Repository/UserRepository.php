<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;


use App\Entity\Admin\AppModule;
use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\Setting;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use setasign\Fpdi\PdfParser\Filter\LzwException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use function Doctrine\ORM\QueryBuilder;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    protected function handleSearchBetween($qb,$data)
    {

        $name = isset($data['name'])? $data['name'] :'';
        $username = isset($data['username'])? $data['username'] :'';
        $email = isset($data['email'])? $data['email'] :'';
        $mobile = isset($data['mobile'])? $data['mobile'] :'';

        if(!empty($name)){
            $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
        }
        if(!empty($username)){
            $qb->andWhere($qb->expr()->like("e.username", "'%$username%'"));
        }
        if(!empty($email)){
            $qb->andWhere($qb->expr()->like("e.email", "'%$email%'"));
        }
        if(!empty($mobile)){
            $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
        }

    }

    /**
     * User Login Validation
     */

    public function checkLoginUser($username)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.username = :user')->setParameter('user', $username);
        $qb->andWhere('u.enabled = :enabled')->setParameter('enabled', 1);
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }


    public function systemuserCreate( Terminal $terminal, UserPasswordEncoderInterface $encoder ){

            $em = $this->_em;
            $user = new User();
            $user->setTerminal($terminal);
            $user->setName($terminal->getOrganizationName());
            $user->setUsername($terminal->getMobile());
            $user->setMobile($terminal->getMobile());
            if($terminal->getEmail()){
                $user->setEmail($terminal->getEmail());
            }else{
                $email = $terminal->getMobile()."@gmail.com";
                $user->setEmail($email);
            }
            $user->setPassword($encoder->encodePassword($user,"*148148#"));
            $user->setRoles(array('ROLE_DOMAIN'));
            $em->persist($user);
            $em->flush();
    }


    /**
     * @return User[]
     */
    public function findBySearchQuery($domain , $parameter , $data ): array
    {

        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }

        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.designation','dg');
        $qb->leftJoin('e.department','dp');
        $qb->leftJoin('e.userGroup','ug');
        $qb->select('e.id as id','e.name as name','e.email as email','e.username as username');
        $qb->addSelect('e.mobile as mobile','e.enabled as enabled');
        $qb->addSelect('b.name as branchName');
        $qb->addSelect('dg.name as designationName');
        $qb->addSelect('dp.name as departmentName');
        $qb->addSelect('ug.name as userGroup');
        $qb->where('e.terminal =:domain')->setParameter('domain',$domain);
    //    $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.id', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function getAccessRoleGroup(Terminal $terminal){


        $modules = $terminal->getAppModules();
        $arrSlugs = array();
        if (!empty($terminal->getAppModules()) and !empty($modules)) {
            /* @var $mod AppModule */
            foreach ($terminal->getAppModules() as $mod) {
                if (!empty($mod->getModuleClass())) {
                    $arrSlugs[] = $mod->getSlug();
                }
            }
        }


        $array = array();

        $array['KPI'] = array(
            'User'                               => 'ROLE_KPI_USER',
//            'CSO/SPO'                            => 'ROLE_SPO_CSO',
            'Officer'                            => 'ROLE_KPI_OFFICER',
//            'Regional Manager'                   => 'ROLE_KPI_REGIONAL',
//            'Zonal Manager'                      => 'ROLE_KPI_ZONAL',
            'Admin'                              => 'ROLE_KPI_ADMIN',
            'Office Executive'                   => 'ROLE_KPI_EXECUTIVE',
//            'Line Manager'                       => 'ROLE_KPI_LINE_MANAGER',
        );

        $array['CRM'] = array(
//            'CRM'                                => 'ROLE_CRM',
            'User (Poultry)'                      => 'ROLE_CRM_POULTRY_USER',
            'User (Cattle)'                       => 'ROLE_CRM_CATTLE_USER',
            'User (Aqua)'                         => 'ROLE_CRM_AQUA_USER',
            'User (Sales & Marketing)'            => 'ROLE_CRM_SALES_MARKETING_USER',

            'Report'                              => 'ROLE_CRM_REPORT',
//            'CSO/SPO'                            => 'ROLE_CRM_SPO_CSO',
//            'Officer'                            => 'ROLE_CRM_OFFICER',
//            'Regional Manager'                    => 'ROLE_CRM_REGIONAL',
//            'Zonal Manager'                       => 'ROLE_CRM_ZONAL',

//            'Admin'                              => 'ROLE_CRM_ADMIN',
            'Admin (Poultry)'                     => 'ROLE_CRM_POULTRY_ADMIN',
            'Admin (Cattle)'                      => 'ROLE_CRM_CATTLE_ADMIN',
            'Admin (Aqua)'                        => 'ROLE_CRM_AQUA_ADMIN',
            'Admin (Sales & Marketing)'           => 'ROLE_CRM_SALES_MARKETING_ADMIN',

        );

        $array['BR'] = array(
            'User'                               => 'ROLE_BR_USER',
            'Admin'                              => 'ROLE_BR_ADMIN',
        );

        $array['SMS'] = array(
            'User'                               => 'ROLE_SMS_USER',
            'Admin'                              => 'ROLE_SMS_ADMIN',
        );

        $array['USERS'] = array(
//            'User'                               => 'ROLE_USER',
//            'Admin'                              => 'ROLE_ADMIN',
            'Allow To Switch'                    => 'ROLE_ALLOWED_TO_SWITCH',
            'Manage Employee'                    => 'ROLE_MANAGE_EMPLOYEE',
            'Line Manager'                       => 'ROLE_LINE_MANAGER',
            'Regional Manager'                   => 'ROLE_REGIONAL_MANAGER',
            'Zonal Manager'                      => 'ROLE_ZONAL_MANAGER',
            'Core'                               => 'ROLE_CORE'
        );
        $array['SALES'] = array(
            'DO Sms'                    => 'ROLE_SALES_DO_SMS', //send sms to responsible person daily total DO amount
        );
        return $array;
    }

    public function getAndroidRoleGroup(){

        $array = array();
        $array['Android Apps'] = array(
            'CRM'                                => 'ROLE_CRM',
            'CSO/SPO'                            => 'ROLE_CRM_SPO_CSO',
            'Officer'                            => 'ROLE_CRM_OFFICER',
            'Regional Manager'                   => 'ROLE_CRM_REGIONAL',
            'Zonal Manager'                      => 'ROLE_CRM_ZONAL',
            'Admin'                              => 'ROLE_CRM_ADMIN',
        );


        return $array;
    }

    public function getLineManager()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.lineManager' , 'lineManager');
        $qb->select('lineManager.id', 'lineManager.name', 'lineManager.userId')
            ->where("e.enabled =1");

        $results = $qb->getQuery()->getArrayResult();
        $returnArray=[];
        foreach ($results as $result){
            $returnArray['('. $result['userId'] .') ' . $result['name']]=$result['id'];
        }
        return $returnArray;
    }

    public function getLineManagerForInlineUpdate()
    {
        $qb = $this->createQueryBuilder('e');
//        $qb->join('e.lineManager' , 'lineManager');
        $qb->select('e.id', 'e.name')
            ->where("e.enabled = 1")
            ->andWhere('e.isDelete = 0')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like("e.roles", ':lineManager'),
                $qb->expr()->like("e.roles", ':admin')
            ))
            ->setParameters([
                'lineManager' => '%ROLE_LINE_MANAGER%',
                'admin' => '%ROLE_KPI_ADMIN%'
            ]);

        $results = $qb->getQuery()->getArrayResult();
        $returnArray=[];
        foreach ($results as $result){
            $returnArray[$result['id']]=$result['name'];
        }
        return $returnArray;
    }

    public function getFilteredEmployee($filter)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.designation', 'designation');
        $qb->select('e');
        $qb->where("designation.slug = :slug ")->setParameter('slug', $filter);
        $qb->andWhere("e.userMode = 'KPI'");
        return $qb->getQuery()->getResult();
    }

    public function getSearchedKpiEmployee($filterBy, User $user)
    {
        $qb = $this->createQueryBuilder('e');

        $qb->leftJoin('e.districtHistories', 'district_histories');
        $qb->leftJoin('e.designation', 'designation');
        $qb->leftJoin('e.lineManager', 'lineManager');
        $qb->leftJoin('e.reportMode', 'reportMode');
//        $qb->leftJoin('e.department', 'department');

        $qb->select('e.id','e.userId', 'e.name AS employeeName', 'e.mobile', 'e.email', 'e.enabled', 'e.permanentDate');
        $qb->addSelect('district_histories.district');
        $qb->addSelect('designation.id AS designationId', 'designation.name AS designationName');
        $qb->addSelect('reportMode.id AS reportModeId', 'reportMode.name AS reportModeName');
//        $qb->addSelect('department.id AS departmentId', 'department.name AS departmentName');
        $qb->addSelect('lineManager.id AS lineManagerId', 'lineManager.name AS lineManagerName');

        $qb->where("e.userMode = 'KPI'");
        $qb->andWhere("e.enabled = '1'");
        $qb->andWhere('district_histories.month = :currentMonth')->setParameter('currentMonth', date('F'));
        $qb->andWhere('district_histories.year = :currentYear')->setParameter('currentYear', date('Y'));





/*        $qb->leftJoin('e.lineManager', 'lineManager');
        $qb->leftJoin('e.designation', 'designation');
        $qb->leftJoin('e.reportMode', 'reportMode');
        $qb->leftJoin('e.department', 'department');
        $qb->select('e');
        $qb->where("e.userMode = 'KPI'");*/
        if (isset($filterBy['employee'])){
            $qb->andWhere('e.id = :id')->setParameter('id', $filterBy['employee']->getId());
        }
        if (isset($filterBy['designation'])){
            $qb->andWhere('designation.id = :designationId')->setParameter('designationId', $filterBy['designation']->getId());
        }
        if (isset($filterBy['kpiFormat'])){
            $qb->andWhere('reportMode.id = :reportModeId')->setParameter('reportModeId', $filterBy['kpiFormat']->getId());
        }
        if (isset($filterBy['lineManager'])){
            $qb->andWhere('lineManager.id = :lineManagerId')->setParameter('lineManagerId', $filterBy['lineManager']);
        }
        if (isset($filterBy['employeeMobile'])){
            $qb->andWhere('e.mobile LIKE :mobile')->setParameter('mobile', '%'.trim($filterBy['employeeMobile']).'%');
        }
/*        if (isset($filterBy['department'])){
            $qb->andWhere('department.id = :departmentId')->setParameter('departmentId', $filterBy['department']);
        }*/
        if (!in_array('ROLE_KPI_ADMIN', $user->getRoles())){
            $qb->andWhere('lineManager.id = :lineManager')->setParameter('lineManager', $user->getId());
        }
        return $qb->getQuery()->getArrayResult();
    }

    public function getSearchedEmployee($filterBy)
    {
        $qb = $this->createQueryBuilder('e');

        $qb->leftJoin('e.districtHistories', 'district_histories');
        $qb->leftJoin('e.designation', 'designation');
        $qb->leftJoin('e.lineManager', 'lineManager');
        $qb->leftJoin('e.reportMode', 'reportMode');
        $qb->leftJoin('e.serviceMode', 'serviceMode');
//        $qb->leftJoin('e.department', 'department');
        $qb->join('e.userGroup', 'userGroup');

        $qb->select('e.id','e.userId', 'e.name AS employeeName', 'e.mobile', 'e.email', 'e.enabled', 'e.permanentDate', 'e.isPermanent', 'e.otp');
        $qb->addSelect('district_histories.district');
        $qb->addSelect('designation.id AS designationId', 'designation.name AS designationName');
        $qb->addSelect('reportMode.id AS reportModeId', 'reportMode.name AS reportModeName');
        $qb->addSelect('serviceMode.id AS serviceModeId', 'serviceMode.name AS serviceModeName');
//        $qb->addSelect('department.id AS departmentId', 'department.name AS departmentName');
        $qb->addSelect('lineManager.id AS lineManagerId', 'lineManager.name AS lineManagerName','lineManager.userId AS lineManagerUserId');

//        $qb->where("e.userMode = 'KPI'");
        $qb->where("userGroup.slug = 'employee'");
        $qb->andWhere('district_histories.month = :currentMonth')->setParameter('currentMonth', date('F'));
        $qb->andWhere('district_histories.year = :currentYear')->setParameter('currentYear', date('Y'));


/*        $qb->leftJoin('e.lineManager', 'lineManager');
        $qb->leftJoin('e.designation', 'designation');
        $qb->leftJoin('e.reportMode', 'reportMode');
        $qb->leftJoin('e.department', 'department');
        $qb->select('e');
        $qb->where("e.userMode = 'KPI'");*/
        if (isset($filterBy['employee'])){
            $qb->andWhere('e.id = :id')->setParameter('id', $filterBy['employee']->getId());
        }
        if (isset($filterBy['designation'])){
            $qb->andWhere('designation.id = :designationId')->setParameter('designationId', $filterBy['designation']->getId());
        }
        if (isset($filterBy['kpiFormat'])){
            $qb->andWhere('reportMode.id = :reportModeId')->setParameter('reportModeId', $filterBy['kpiFormat']->getId());
        }
        if (isset($filterBy['lineManager'])){
            $qb->andWhere('lineManager.id = :lineManagerId')->setParameter('lineManagerId', $filterBy['lineManager']);
        }
        if (isset($filterBy['employeeMobile'])){
            $qb->andWhere('e.mobile LIKE :mobile')->setParameter('mobile', '%'.trim($filterBy['employeeMobile']).'%');
        }
        if (isset($filterBy['lineManager'])){
            $qb->andWhere('lineManager.id = :lineManagerId')->setParameter('lineManagerId', $filterBy['lineManager']->getId());
        }
        if (isset($filterBy['status'])){
            $status = $filterBy['status'] == 'enable' ? 1 : 0;
            $qb->andWhere('e.enabled = :status')->setParameter('status', $status);
        }
        if (isset($filterBy['serviceMode'])){
            $qb->andWhere('e.serviceMode = :serviceMode')->setParameter('serviceMode', $filterBy['serviceMode']);
        }

        if (isset($filterBy['permanentStatus'])){
            $permanentStatus = $filterBy['permanentStatus'] == 'yes' ? 1 : 0;
            $qb->andWhere('e.isPermanent = :permanentStatus')->setParameter('permanentStatus', $permanentStatus);
        }

        return $qb->getQuery()->getArrayResult();
    }


    public function getEmployeeHierarchy()
    {
        $qb = $this->createQueryBuilder('e');
//        $qb->join('e.designation', 'designation');
        $qb->join('e.reportMode', 'reportMode');
        $qb->select('e.id AS employeeId', 'e.name AS employeeName', 'reportMode.name AS reportModeName');
//        $qb->addSelect('designation.name AS designationName');
//        $qb->where('designation.slug IN (:slug)')->setParameter('slug', ['junior-agm', 'agm']);
        $qb->where('reportMode.slug IN (:slug)')->setParameter('slug', 'agm-kpi');
        $agm = $qb->getQuery()->getArrayResult();
        $returnArray=[];
        $agmId=[];
        foreach ($agm as $item){
//            $returnArray['agm'][$item['employeeId']]=$item;
            $returnArray['agm-format'][$item['employeeId']]=$item;
            $agmId[]=$item['employeeId'];
        }


        $qb = $this->createQueryBuilder('e');
//        $qb->join('e.designation', 'designation');
        $qb->join('e.reportMode', 'reportMode');
        $qb->join('e.lineManager', 'lineManager');
        $qb->select('e.id AS employeeId', 'e.name AS employeeName');
//        $qb->addSelect('designation.name AS designationName');
        $qb->addSelect('lineManager.id AS lineManagerId', 'reportMode.name AS reportModeName');
        $qb->where('e.lineManager IN (:lineManagerId)')->setParameter('lineManagerId', $agmId);
        $rsm = $qb->getQuery()->getArrayResult();
        $rsmId=[];
        foreach ($rsm as $item){
            $returnArray['agm-format'][$item['lineManagerId']]['rsm-format'][$item['employeeId']]=$item;
            $rsmId[]=$item['employeeId'];
        }


        $qb = $this->createQueryBuilder('e');
        $qb->join('e.reportMode', 'reportMode');
        $qb->join('e.lineManager', 'lm');
        $qb->join('lm.lineManager', 'parentLm');
        $qb->select('e.id AS employeeId', 'e.name AS employeeName');
        $qb->addSelect('reportMode.name AS reportModeName');
        $qb->addSelect('lm.id AS lineManagerId');
        $qb->addSelect('parentLm.id AS lineManagerParentId');
        $qb->where('e.lineManager IN (:lineManagerId)')->setParameter('lineManagerId', $rsmId);
        $employee = $qb->getQuery()->getArrayResult();
        $csoId=[];
        foreach ($employee as $item){
            $returnArray['agm-format'][$item['lineManagerParentId']]['rsm-format'][$item['lineManagerId']]['employee'][$item['employeeId']]=$item;
            $returnArray['agm-format'][$item['lineManagerParentId']]['count'][] = $item;
//            $rsmId[]=$item['employeeId'];
        }
        return $returnArray;
    }

    public function getKpiEmployees(User $user)
    {
        $qb = $this->createQueryBuilder('e');

        $qb->leftJoin('e.districtHistories', 'district_histories');
        $qb->leftJoin('e.designation', 'designation');
        $qb->leftJoin('e.lineManager', 'lineManager');
        $qb->leftJoin('e.reportMode', 'reportMode');
//        $qb->leftJoin('e.department', 'department');

        $qb->select('e.id','e.userId', 'e.name AS employeeName', 'e.mobile', 'e.email', 'e.enabled', 'e.joiningDate', 'e.permanentDate', 'e.isPermanent');
        $qb->addSelect('district_histories.district');
        $qb->addSelect('designation.id AS designationId', 'designation.name AS designationName');
        $qb->addSelect('reportMode.id AS reportModeId', 'reportMode.name AS reportModeName');
//        $qb->addSelect('department.id AS departmentId', 'department.name AS departmentName');
        $qb->addSelect('lineManager.id AS lineManagerId', 'lineManager.name AS lineManagerName');

        $qb->where("e.userMode = 'KPI'");
        $qb->andWhere("e.enabled = '1'");
        $qb->andWhere('district_histories.month = :currentMonth')->setParameter('currentMonth', date('F'));
        $qb->andWhere('district_histories.year = :currentYear')->setParameter('currentYear', date('Y'));
        if ($user->getUserGroup()->getSlug() != 'administrator'){
//            $qb->join('e.lineManager', 'lineManager')
            $qb->andWhere('lineManager.id = :lineManagerId')->setParameter('lineManagerId', $user->getId());
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getLineManagers()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.userId', 'e.name');
        $qb->where('e.enabled = 1');
        $qb->andWhere('e.userMode = :mode')->setParameter('mode', 'KPI');
        $qb->andWhere('e.roles LIKE :role')->setParameter('role', '%"ROLE_LINE_MANAGER"%');
        return $qb->getQuery()->getArrayResult();
    }

    public function getLineManagerTeamMember($lineManagersId, $roles)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.lineManager', 'lineManager');
        $qb->select('e.id','e.name AS employeeName', 'e.userId AS employeeUserId', 'e.isPermanent');
        $qb->addSelect('lineManager.id AS lineManagerAutoIncId','lineManager.userId AS lineManagerUserId','lineManager.name AS lineManagerName');
        $qb->where('e.enabled = 1');
        $qb->andWhere("e.userMode = 'KPI'");
        $qb->andWhere('lineManager.userId IN (:lineManagerId)')->setParameter('lineManagerId', $lineManagersId);

        if (!empty($roles)){
            $query = '';
            foreach ($roles as $key => $role) {
                if ($key !== 0){
                    $query .= " OR ";
                }
                $query .= "e.roles LIKE '%" . $role . "%'";

            }
            $qb->andWhere($query);
        }

        $results =  $qb->getQuery()->getArrayResult();
        $data = [];
        foreach ($results as $result) {
            $data[$result['lineManagerUserId']]['lineManager'] = [
                'id' => $result['lineManagerAutoIncId'],
                'userId' => $result['lineManagerUserId'],
                'name' => $result['lineManagerName'],
            ];
            $data[$result['lineManagerUserId']]['teamMember'][$result['employeeUserId']]['id'] = $result['id'];
            $data[$result['lineManagerUserId']]['teamMember'][$result['employeeUserId']]['employeeName'] = $result['employeeName'];
            $data[$result['lineManagerUserId']]['teamMember'][$result['employeeUserId']]['userId'] = $result['employeeUserId'];
            $data[$result['lineManagerUserId']]['teamMember'][$result['employeeUserId']]['permanentStatus'] = $result['isPermanent'];
        }
        return $data;
    }


    public function getRegionalManagers()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.reportMode', 'reportMode');
        $qb->join('e.district', 'district');
        $qb->join('e.regional', 'region');
        $qb->select('e.id','e.mobile', 'e.name', 'district.id AS districtId', 'e.userId', 'region.name AS regionName');
        $qb->where("e.enabled = 1");
        $qb->andWhere($qb->expr()->like('e.roles', ':role'))->setParameter('role', '%ROLE_SALES_DO_SMS%');

//        $qb->andWhere("reportMode.slug = 'rsm-arsm-kpi'");

        $results =  $qb->getQuery()->getArrayResult();

        $data = [];
        foreach ($results as $result) {
            $data[$result['userId']]['id'] = $result['id'];
            $data[$result['userId']]['name'] = $result['name'];
            $data[$result['userId']]['mobile'] = $result['mobile'];
            $data[$result['userId']]['regionName'] = $result['regionName'];
            $data[$result['userId']]['districtId'][] = $result['districtId'];
        }
        return $data;
    }


    public function getEmployees()
    {
        $userGroup = $this->_em->getRepository(Setting::class)->findOneBy(['slug' => 'employee']);
        $qb = $this->createQueryBuilder('e');

        $qb->leftJoin('e.districtHistories', 'district_histories');
        $qb->leftJoin('e.designation', 'designation');
        $qb->leftJoin('e.lineManager', 'lineManager');
        $qb->leftJoin('e.reportMode', 'reportMode');
        $qb->leftJoin('e.department', 'department');
        $qb->leftJoin('e.serviceMode', 'service_mode');
        $qb->leftJoin('e.responsibleOf', 'responsibleOf');
        $qb->leftJoin('e.zonal', 'zonal');
        $qb->leftJoin('e.regional', 'regional');
        $qb->leftJoin('e.upozila', 'upozila');

        $qb->select('e.id','e.userId', 'e.name AS employeeName', 'e.mobile', 'e.email', 'e.enabled', 'e.joiningDate', 'e.typeOfVehicle', 'e.vehicleNo', 'e.permanentDate', 'e.isPermanent');
        $qb->addSelect('district_histories.district');
        $qb->addSelect('designation.id AS designationId', 'designation.name AS designationName');
        $qb->addSelect('service_mode.id AS serviceModeId', 'service_mode.name AS serviceModeName');
        $qb->addSelect('responsibleOf.id AS responsibleOfId', 'responsibleOf.name AS responsibleOfName');
        $qb->addSelect('zonal.id AS zonalId', 'zonal.name AS zonalName');
        $qb->addSelect('regional.id AS regionalId', 'regional.name AS regionalName');
        $qb->addSelect('upozila.id AS upozilaId', 'upozila.name AS upozilaName');
        $qb->addSelect('reportMode.id AS reportModeId', 'reportMode.name AS reportModeName');
        $qb->addSelect('department.id AS departmentId', 'department.name AS departmentName');
        $qb->addSelect('lineManager.id AS lineManagerId', 'lineManager.userId AS lineManagerUserId', 'lineManager.name AS lineManagerName');

        $qb->where('e.userGroup = :userGroup')->setParameter('userGroup', $userGroup);
        $qb->andWhere('district_histories.month = :currentMonth')->setParameter('currentMonth', date('F'));
        $qb->andWhere('district_histories.year = :currentYear')->setParameter('currentYear', date('Y'));

        $results = $qb->getQuery()->getArrayResult();

        $data = [];
        foreach ($results as $result) {
            $data[$result['id']] = $result;
            $upozila[$result['id']][] = $result['upozilaName'];
            $data[$result['id']]['upozilaName'] = $upozila[$result['id']];
        }
        return $data;
    }


    public function getSalesDoSmsAdmins()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.userGroup', 'user_group');

        $qb->select('e.id','e.name', 'e.mobile');

        $qb->where('user_group.slug = :slug')->setParameter('slug', 'administrator');
        $qb->andWhere($qb->expr()->like('e.roles', ':role'))->setParameter('role', '%ROLE_SALES_DO_SMS%');

        return $qb->getQuery()->getArrayResult();
    }


    public function getServiceModeWiseEmployee($lineManagersId, $roles)
    {
        $qb = $this->createQueryBuilder('e');

        $qb->join('e.userGroup', 'userGroup');
        $qb->where("userGroup.slug = 'employee'");
        $qb->andWhere("e.enabled = 1");

        $query = '';
        foreach ($roles as $key => $role) {
            if ($key !== 0){
                $query .= " OR ";
            }
            $query .= "e.roles LIKE '%" . $role . "%'";

        }
        $qb->andWhere($query);

        $qb->orderBy('e.name');

        return $qb->getQuery()->getResult();

    }


    public function getAdminWiseLineManagers($roles)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.userId', 'e.name', 'e.roles');
        $qb->where('e.enabled = 1');
        $qb->andWhere('e.userMode = :mode')->setParameter('mode', 'KPI');
        $qb->andWhere('e.roles LIKE :role')->setParameter('role', '%"ROLE_LINE_MANAGER"%');
        $query = '';
        foreach ($roles as $key => $role) {
            if ($key !== 0) {
                $query .= " OR ";
            }
            $query .= "e.roles LIKE '%" . $role . "%'";

        }
        $qb->andWhere($query);
        return $qb->getQuery()->getArrayResult();
    }

    public function getRoleWiseEmployees($roles)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.lineManager','lineManager');
        $qb->select('e.id', 'e.userId', 'e.name', 'e.roles');
        $qb->addSelect('lineManager.id as lineManagerId','lineManager.userId as lineManagerUserId', 'lineManager.name as lineManagerName');
        $qb->where('e.enabled = 1');
        $qb->andWhere('e.userMode = :mode')->setParameter('mode', 'KPI');
//        $qb->andWhere('e.roles LIKE :role')->setParameter('role', '%"ROLE_LINE_MANAGER"%');
        if(sizeof($roles)>0) {
            $query = '';
            foreach ($roles as $key => $role) {
                if ($key !== 0) {
                    $query .= " OR ";
                }
                $query .= "e.roles LIKE '%" . $role . "%'";

            }
            $qb->andWhere($query);
        }
        $results = $qb->getQuery()->getArrayResult();
        $returnArray=[];
        if($results){
            foreach ($results as $result) {
                $returnArray['lineManager'][$result['lineManagerUserId']]=$result['lineManagerName'];
                $returnArray['employee'][]=$result;
            }
        }
        return $returnArray;
    }

    public function getEmployeesByEmployeeIds(User $loggedInUser)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.lineManager','lineManager');
        $qb->select('e.id', 'e.userId', 'e.name', 'e.roles');
        $qb->addSelect('lineManager.id as lineManagerId','lineManager.userId as lineManagerUserId', 'lineManager.name as lineManagerName');
        $qb->where('e.enabled = 1');
        $qb->andWhere('e.userMode = :mode')->setParameter('mode', 'KPI');

        $employeeIdsByLineManager = $this->getEmployeesByLineManager($loggedInUser);

        $employeeIs=[];
        if($employeeIdsByLineManager && sizeof($employeeIdsByLineManager)>0){
            $employeeIs=$employeeIdsByLineManager;
        }
        $qb->andWhere('e.id IN (:employeeIds)')->setParameter('employeeIds', $employeeIs);
        $results = $qb->getQuery()->getArrayResult();
        $returnArray=[];
        if($results){
            foreach ($results as $result) {
                    $returnArray['lineManager'][$result['lineManagerUserId']]=$result['lineManagerName'];
                if(in_array('ROLE_CRM_SALES_MARKETING_USER',$result['roles'])
                    || in_array('ROLE_CRM_POULTRY_USER',$result['roles'])
                    || in_array('ROLE_CRM_CATTLE_USER',$result['roles'])
                    || in_array('ROLE_CRM_AQUA_USER',$result['roles'])){
                    $returnArray['employee'][]=$result;
                }
            }
        }
        return $returnArray;
    }

    public function getPermanentTeamMemberByLineManager(User $user){
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.lineManager','lineManager');
        $qb->select('e.id', 'e.userId', 'e.name');
        $qb->where('e.enabled = 1');
        $qb->andWhere('e.permanentDate IS NOT NULL');
        $qb->andWhere('lineManager.id = :lineManager_id')->setParameter('lineManager_id', $user->getId());
        $results = $qb->getQuery()->getArrayResult();
        $arrayData = [];
        if($results){
            foreach ($results as $result) {
                $arrayData[$result['id']] = $result;
            }
        }

        return $arrayData;

    }

    public function getEmployeesByLineManager(User $user){
        $sql="WITH RECURSIVE cte (userId, name, user_id, crm_line_manager_id, Children)
AS
(
SELECT id, name, user_id, crm_line_manager_id,  CAST('' AS CHAR(255))
        FROM core_user AS LastGeneration
        WHERE enabled=1   
    UNION ALL
    SELECT PrevGeneration.id, PrevGeneration.name, PrevGeneration.user_id, PrevGeneration.crm_line_manager_id,
    CAST(CASE WHEN Child.Children = ''
        THEN(Child.userId)
        ELSE(CONCAT(Child.Children,',',CAST((Child.userId) AS CHAR(255))))
    END AS CHAR(255))
        FROM core_user AS PrevGeneration
        INNER JOIN cte AS Child ON PrevGeneration.id = Child.crm_line_manager_id  Where enabled=1  
)
SELECT userId, name, user_id, crm_line_manager_id, GROUP_CONCAT(DISTINCT Children) as childrenIds
    FROM cte WHERE userId = :userId group by userId order by userId";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('userId', $user->getId());
        $stmt->execute();
        $employeesByLineManager = $stmt->fetch();

        $returnArray = [];
        if($employeesByLineManager && isset($employeesByLineManager['childrenIds']) && $employeesByLineManager['childrenIds']!=''){
            $employeeIds=explode(',',$employeesByLineManager['childrenIds']);
            $removeEmptyValue = array_filter($employeeIds, function($a) {return $a !== "";});
            $returnArray=array_unique($removeEmptyValue);
            sort($returnArray);
            return $returnArray;
        }

        return $returnArray;
    }

}
