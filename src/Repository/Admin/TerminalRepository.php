<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository\Admin;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Accounting;
use App\Entity\Application\Inventory;
use App\Entity\Application\Nbrvat;
use App\Entity\Application\Production;
use App\Entity\User;
use App\Service\ConfigureManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class TerminalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Terminal::class);
    }

    public function recordCount()
    {
        return $count = $this->createQueryBuilder('e')
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function checkAvailable($mode,$data)
    {
        $process = "true";
        $organizationName   = isset($data['organizationName']) ? $data['organizationName'] :'';
        $mobile             = isset($data['mobile']) ? $data['mobile'] :'';
        $domain             = isset($data['domain']) ? $data['domain'] :'';
        $subDomain          = isset($data['subDomain']) ? $data['subDomain'] :'';

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        if(!empty($organizationName)){
            $qb->andWhere('e.organizationName =:main')->setParameter("main",$organizationName);
        }
        if(!empty($mobile)){
            $confManager = new ConfigureManager();
            $confManager->specialExpClean($mobile);
            $qb->andWhere('e.mobile =:mobile')->setParameter("mobile",$mobile);
        }
        if(!empty($domain)){
            $qb->andWhere('e.domain =:domain')->setParameter("domain",$domain);
        }
        if(!empty($subDomain)){
            $qb->andWhere('e.subDomain =:subDomain')->setParameter("subDomain",$subDomain);
        }
        $count = $qb->getQuery()->getOneOrNullResult();
        if($mode == "creatable"){
            if ($count['count'] == 1 ){
                $process="false";
            }
        }elseif($mode == "editable") {
            if ($count['count'] > 1 ){
                $process="false";
            }
        }
        return $process;

    }

    protected function handleSearchBetween($qb,$data)
    {

        $created = isset($data['created'])? $data['created'] :'';
        $organizationName = isset($data['organizationName'])? $data['organizationName'] :'';
        $name = isset($data['name'])? $data['name'] :'';
        $mobile = isset($data['mobile'])? $data['mobile'] :'';
        $location = isset($data['location'])? $data['location'] :'';
        $mainApp = isset($data['mainApp'])? $data['mainApp'] :'';
        $syndicate = isset($data['syndicate'])? $data['syndicate'] :'';
        if(!empty($name)){
            $qb->andWhere($qb->expr()->like("e.name", "'%$name%'"));
        }
        if(!empty($organizationName)){
            $qb->andWhere($qb->expr()->like("e.organizationName", "'%$organizationName%'"));
        }
        if(!empty($mobile)){
            $qb->andWhere($qb->expr()->like("e.mobile", "'%$mobile%'"));
        }
        if(!empty($location)){
            $qb->andWhere($qb->expr()->like("l.name", "'%$location%'"));
        }
        if(!empty($mainApp)){
            $qb->andWhere('e.mainApp =:main')->setParameter("main",$mainApp);
        }
        if(!empty($syndicate)){
            $qb->andWhere('e.syndicate =:syndicate')->setParameter("syndicate",$syndicate);
        }
        if (!empty($created) ) {
            $datetime = new \DateTime($created);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate")->setParameter('startDate', $startDate);
            $date = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate")->setParameter('endDate', $date);
        }

    }

    /**
     * @return Terminal[]
     */
    public function findBySearchQuery($parameter, $data ): array
    {

        if (!empty($parameter['orderBy'])) {
            $sortBy = $parameter['orderBy'];
            $order = $parameter['order'];
        }


        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.location','l');
        $qb->leftJoin('e.mainApp','a');
        $qb->leftJoin('e.syndicate','s');
        $qb->select('e.id as id','e.created as created','e.name as name','e.organizationName as organizationName','e.mobile as mobile','e.status as status');
        $qb->addSelect('l.name as locationName');
        $qb->addSelect('a.name as mainApp');
        $qb->addSelect('s.name as syndicateName');
        $this->handleSearchBetween($qb,$data);
        $qb->setFirstResult($parameter['offset']);
        $qb->setMaxResults($parameter['limit']);
        if ($parameter['orderBy']){
            $qb->orderBy($sortBy, $order);
        }else{
            $qb->orderBy('e.id', 'DESC');
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function systemConfigUpdate(Terminal $terminal )
    {

        $em = $this->_em;

        $accounting = $em->getRepository(Accounting::class)->findOneBy(array('terminal'=> $terminal->getId()));
        if(empty($accounting)){
            $config = new Accounting();
            $config->setTerminal($terminal->getId());
            $em->persist($config);
        }

        $inventory = $em->getRepository(Inventory::class)->findOneBy(array('terminal'=>$terminal->getId()));
        if(empty($inventory)){
            $config = new Inventory();
            $config->setTerminal($terminal->getId());
            $em->persist($config);
        }

        $nrbvat = $em->getRepository(Nbrvat::class)->findOneBy(array('terminal'=>$terminal->getId()));
        if(empty($nrbvat)){
            $config = new Nbrvat();
            $config->setTerminal($terminal->getId());
            $em->persist($config);
        }

        $production = $em->getRepository(Production::class)->findOneBy(array('terminal'=>$terminal->getId()));
        if(empty($production)){
            $config = new Production();
            $config->setTerminal($terminal->getId());
            $em->persist($config);
        }

        $em->flush();

    }


}
