<?php


namespace App\Service;


use App\Entity\DataCleanLog;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Security;

class SchemaTruncate
{
    public function __construct(EntityManagerInterface $em, FlashBagInterface $flashBag, Security $security)
    {
        $this->em = $em;
        $this->flashBag = $flashBag;
        $this->security = $security;
    }

    public function runQuery($request, array $schemas, string $module)
    {
        $tables = [];
        $log = new DataCleanLog();
        $log->setCreatedAt(new \DateTimeImmutable('now'));
        $log->setDeviceIp($request->getClientIp());
        $log->setModuleName($module);
        $log->setCleanedBy($this->security->getUser());

        foreach ($schemas as $schema) {
            $checkSchema = "SHOW TABLES LIKE '$schema'";
            $stmt = $this->em->getConnection()->prepare($checkSchema);
            $stmt->executeQuery();

            if ($stmt->fetchAll()) {

                $sql = "SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE " . $schema . "; SET FOREIGN_KEY_CHECKS = 1";

                $stmt = $this->em->getConnection()->prepare($sql);

                array_push($tables, $schema);
                $stmt->executeQuery();
//                $stmt->fetchAll();
                $stmt->closeCursor();
            }
        }

        $log->setSchemaName($tables);

        $this->em->persist($log);
        $this->em->flush();
        $this->flashBag->add('success', $module . ' Data Clean Successfully!');

    }
}