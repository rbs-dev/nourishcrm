<?php

namespace App\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;


class SmsSender
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var SmsGateWay
     */
    private $smsGateWay;
    
    public function __construct(SmsGateWay $smsGateWay)
    {
        $this->smsGateWay = $smsGateWay;
    }

    public function sendSms($msg, $cellPhone)
    {
        $mobile = '88' . $cellPhone;
       return $this->smsGateWay->send($msg, $mobile);
    }
}