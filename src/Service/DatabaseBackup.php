<?php


namespace App\Service;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;


class DatabaseBackup
{
    public function __construct(FlashBagInterface $flashBag)
    {
        $this->flashBag = $flashBag;
    }

    public function backup($host, $user, $pass, $database, $path)
    {

        $fileName = $database . '_' .date('d-m-Y'). '_' . time() .'.sql';
        $command='mysqldump --user='. $user .' --password='.$pass .' --host='. $host .' '. $database .' > ' . $path . $fileName;
        try {
            exec($command);
//            $this->flashBag->add('success', 'Backup stored in \''. $path . $database . '_' .date('d-m-Y'). '_' . time() .'.sql\'');
            return $fileName;


        }catch (\Exception $e){
//            $this->flashBag->add('error', $e->getMessage());
            return;
        }
    }
}