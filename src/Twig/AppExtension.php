<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Twig;

use NumberFormatter;
use Symfony\Component\Intl\Locales;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * See https://symfony.com/doc/current/templating/twig_extension.html.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 * @author Julien ITARD <julienitard@gmail.com>
 */
class AppExtension extends AbstractExtension
{
    private $localeCodes;
    private $locales;

    public function __construct(string $locales)
    {
        $localeCodes = explode('|', $locales);
        sort($localeCodes);
        $this->localeCodes = $localeCodes;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('week', [$this, 'formatWeek']),
            new TwigFilter('multiArrayLength', [$this, 'multiDimensionalArrayCount']),
            new TwigFilter('json_decode', [$this, 'jsonDecode']),
            new TwigFilter('ksort', array($this, 'ksort')),
            new TwigFilter('arraySum', array($this, 'arraySum')),
            new TwigFilter('arraySumColWise', array($this, 'arraySumColWise')),
            new TwigFilter('multiDimensionalArrayCol', array($this, 'multiDimensionalArrayCol')),
        ];
    }

    public function formatWeek($number)
    {
        $locale = 'en_US';
        $nf = new NumberFormatter($locale, NumberFormatter::ORDINAL);
        return $nf->format($number);
    }

    public function multiDimensionalArrayCount($multiDimensionalArrayCount)
    {
        $totalMultiElements = array_sum(array_map("count", $multiDimensionalArrayCount));
        return $totalMultiElements;
    }

    public function jsonDecode($str) {
        return json_decode($str, true);
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('locales', [$this, 'getLocales']),
        ];
    }

    /**
     * Takes the list of codes of the locales (languages) enabled in the
     * application and returns an array with the name of each locale written
     * in its own language (e.g. English, Français, Español, etc.).
     */
    public function getLocales(): array
    {
        if (null !== $this->locales) {
            return $this->locales;
        }

        $this->locales = [];
        foreach ($this->localeCodes as $localeCode) {
            $this->locales[] = ['code' => $localeCode, 'name' => Locales::getName($localeCode, $localeCode)];
        }

        return $this->locales;
    }
    public function ksort($array) {
        ksort($array);
        return $array;
    }
    public function arraySum($array) {
        $sum=array_sum($array);
        return $sum;
    }
    public function arraySumColWise($array, $column_name) {
        $sum = array_sum(array_column($array,$column_name));
        return $sum;
    }

    public function multiDimensionalArrayCol($array, $column_name) {
        return array_column($array,$column_name);
    }
}
