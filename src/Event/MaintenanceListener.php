<?php


namespace App\Event;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;

class MaintenanceListener
{
    private $isMaintenance;
    private $twig;

    public function __construct($isMaintenance, Environment $twig)
    {
        $this->isMaintenance = $isMaintenance;
        $this->twig = $twig;
    }

    public function onKernelMaintenance(RequestEvent $event)
    {

        if ( ! $this->isMaintenance){
            return;
        }

        $event->setResponse(new Response(
            $this->twig->render('maintenance.html.twig'),
            Response::HTTP_SERVICE_UNAVAILABLE
        ));

        $event->stopPropagation();
    }
}