<?php

namespace App\Command;

use App\Entity\Admin\Location;
use App\Entity\Core\Agent;
use App\Entity\Core\Setting;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UpdateAgentCommand extends Command
{
    protected static $defaultName = 'app:update-agent';
    protected static $defaultDescription = 'This command for agent update.';
    private $client;
    private $router;
    private $em;
    private $parameterBag;


    public function __construct(HttpClientInterface $client, RouterInterface $router, EntityManagerInterface $em, ParameterBagInterface $parameterBag)
    {
        parent::__construct();

        $this->client = $client;
        $this->router = $router;
        $this->em = $em;
        $this->parameterBag = $parameterBag;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
            ->addOption('last7Days', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $last7DaysOption=$input->getOption('last7Days');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        $this->updateAgent($last7DaysOption);

//        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
        $io->success('Agents are updated!');

        return Command::SUCCESS;
    }

    private function updateAgent($last7DaysOption)
    {
        set_time_limit(0);
        ignore_user_abort(true);

        if($last7DaysOption){
            $apiEndPoint = $this->parameterBag->get('salesDomain') . '/api/agents?last7Days=true';
        }else{
            $apiEndPoint = $this->parameterBag->get('salesDomain') . '/api/agents';
        }

//        $apiEndPoint = 'http://120.50.17.10:81/api/agents';
        $response = $this->client->request('GET', $apiEndPoint, [
            'headers' => [
                'X-API-KEY' => $this->parameterBag->get('crm_api_key')
            ]
        ]);

        $records = $response->toArray();

        foreach ($records as $record) {
            $slug = strtolower($record['agentType']);

            $district = $this->em->getRepository(Location::class)->findOneBy(['name' => $record['districtName'], 'level'=>4]);
            if (!$district){
                $district = $this->em->getRepository(Location::class)->findOneBy(['oldId' => $record['districtId']]);
            }

            $upozila = $this->em->getRepository(Location::class)->findOneBy(['name' => $record['upozillaName'], 'parent' => $district, 'level'=>5]);
            if (!$upozila){
                $upozila = $this->em->getRepository(Location::class)->findOneBy(['oldId' => $record['upozilaId']]);
            }
            $agentGroup = $this->em->getRepository(Setting::class)->findOneBy(['slug' => $slug]);
            $findAgent = $this->em->getRepository(Agent::class)->findOneBy(['agentGroup' => $agentGroup, 'agentId' => $record['agentId']]);

            if ($findAgent){

                $sql = "UPDATE `core_agent` SET upozila_id = :upozila_id , district_id = :district_id, name = :name, mobile = :mobile, email = :email, address = :address, status = :status WHERE id = :id";

                $stmt = $this->em->getConnection()->prepare($sql);

                $stmt->bindValue('id', $findAgent->getId());
                $stmt->bindValue('name', $record['name']);
                $stmt->bindValue('mobile', $record['mobile']);
                $stmt->bindValue('email', $record['email']);
                $stmt->bindValue('address', $record['address']);
                $stmt->bindValue('upozila_id', $upozila ? $upozila->getId() : null);
                $stmt->bindValue('district_id', $district ? $district->getId() : null);
                $stmt->bindValue('status', true);

            }else{
                $createdAt = new \DateTime('now');

                $sql = "INSERT INTO `core_agent` (`agent_group_id`, `upozila_id`, `district_id`, `name`, `agentId`, `mobile`, `email`, `address`, `oldId`, `created`, `status`) VALUES (:agent_group_id, :upozila_id, :district_id, :name, :agentId, :mobile, :email, :address, :oldId, :created, :status)";

                $stmt = $this->em->getConnection()->prepare($sql);

                $stmt->bindValue('name', $record['name']);
                $stmt->bindValue('mobile', $record['mobile']);
                $stmt->bindValue('email', $record['email']);
                $stmt->bindValue('address', $record['address']);
                $stmt->bindValue('agent_group_id', $agentGroup->getId());
                $stmt->bindValue('upozila_id', $upozila ? $upozila->getId() : null);
                $stmt->bindValue('district_id', $district ? $district->getId() : null);
                $stmt->bindValue('agentId', $record['agentId']);
                $stmt->bindValue('oldId', $record['oldId']);
                $stmt->bindValue('created', $createdAt->format('Y-m-d'));
                $stmt->bindValue('status', true);
            }

            $stmt->executeQuery();

//            try {
//                $stmt->execute();
//            }catch (\Exception $e){
//                return new JsonResponse([
//                    'status' => 500,
//                    'message' => $e->getMessage(),
//                ]);
//            }
        }
        return new JsonResponse([
            'status' => 200,
            'message' => 'success',
        ]);
    }
}
