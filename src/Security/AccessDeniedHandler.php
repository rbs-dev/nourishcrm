<?php

namespace App\Security;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        //$content = $this->get('app.error')->redirectToError(true, 'Hello world');
//        return new Response('Error Failed', 403);
       // return $this->render('@TerminalbdKpi/default/bank-satement.html.twig');
     //   return new RedirectResponse($this->router->generate('app_admin'));
        $content="You are not authorized to access this page !";
        return new Response($this->twig->render('bundles/TwigBundle/Exception/error403.html.twig',
            array("message" =>$content)),
            403);

    }
}