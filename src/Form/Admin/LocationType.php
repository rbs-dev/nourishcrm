<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Form\Admin;



use App\Entity\Admin\Location;
use App\Entity\Core\AccountInformation;
use App\Entity\Core\Agent;
use App\Entity\Core\Agents;
use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use App\Repository\Admin\LocationRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class LocationType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'attr' => [
                    'class' => 'span12 select2'
                ],
                'choices' => [
                    'Sector' => 1,
                    'Zone' => 2,
                    'Region' => 3,
                    'District' => 4,
                    'Upozila' => 5,
                ],
                'mapped' => false,
                'required' => false,
                'placeholder' => '- Select Type -'
            ])
            ->add('parent', ChoiceType::class, [
                'attr' => [
                    'class' => 'span12 select2'
                ],
                'choices' => [],
                'placeholder' => '- Select Parent -',
                'mapped' => false,
                'required' => false
            ])
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('code', TextType::class, [
                'attr' => [
                    'class' => 'span12',
                    'placeholder' => '- Code -'
                ],
                'required' => true

            ])
        ;
/*            ->add('parent', EntityType::class, [
                'class' => Location::class,
                'attr'=>['class'=>'span12 select2'],
                'placeholder' => 'Choose a name',
                'choice_label' => 'nestedLabel',
                'choices'   => $options['locationRepo']->getFlatLocationTree()
            ])*/

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
//            'locationRepo' => LocationRepository::class,
        ]);
    }
}
