<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Core;



use App\Entity\Admin\Location;
use App\Entity\Core\AccountInformation;
use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use App\Entity\Core\Vendor;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class VendorFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'mobileLocal'],
                'required' => true
            ])
             ->add('altMobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>' mobileLocal'],
                'required' => false
            ])
            ->add('companyName', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('registrationNo', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => "Enter company registration no "],
                'required' => false
            ])
            ->add('binNo', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => "Enter BIN no "],
                'required' => false
            ])
            ->add('nid', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => "Enter nid no"],
                'required' => false
            ])
            ->add('address', TextType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => "Enter vendor address"],
                'required' => false
            ])
            ->add('location', EntityType::class, [
                'class' => Location::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a location',
            ])
            ->add('businessType', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'business-type')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a business type',
            ])
            ->add('vendorType', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'vendor-type')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a vendor type',
            ])
            ->add('phone', TextType::class, [
                'attr' => ['autofocus' => true],
                 'required' => false
            ])
             ->add('email', TextType::class, [
                'attr' => ['autofocus' => true],
                 'required' => false
            ]);
     //   $builder->add('accountInfo', AccountInformationFormType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Vendor::class,
        ]);
    }
}
