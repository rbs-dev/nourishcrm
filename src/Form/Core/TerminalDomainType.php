<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace App\Form\Core;

use App\Entity\Admin\AppModule;
use App\Entity\Admin\Location;
use App\Entity\Admin\Syndicate;
use App\Entity\Admin\Terminal;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class TerminalDomainType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('organizationName', TextType::class, [
                'attr' => [
                    'autofocus' => true,
                    'data-placement' => 'top' ,
                    'data-toggle' => 'tooltip',
                    'data-trigger'=> "focus",
                    'data-container' => "body",
                    'data-original-title' => 'Enter organization name'],
                'required' => true,
            ])
            ->add('email', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false,
            ])
            ->add('displayMobile', TextType::class, [
                'attr' => ['autofocus' => true,'class' => 'mobileLocal'],
                'required' => false,
            ])
            ->add('domain', TextType::class, [
               'attr' => ['autofocus' => true],
               'required' => false,
            ])
            ->add('subDomain', TextType::class, [
                   'attr' => ['autofocus' => true],
                   'required' => false,
            ])
            ->add('binNo', TextType::class, [
                   'attr' => ['autofocus' => true],
                   'required' => false,
            ])
            ->add('phone', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'phoneLocal'],
                'required' => false
            ])
            ->add('location', EntityType::class, [
               'class' => Location::class,
               'required' => true,
               'query_builder' => function (EntityRepository $er) {
                   return $er->createQueryBuilder('e')
                       ->orderBy('e.name', 'ASC');
               },
               'constraints' => [
                   new NotBlank([
                       'message' => 'Please choose a location',
                   ]),
               ],
               'attr'=>['class'=>'span12 select2'],
               'choice_label' => 'name',
               'placeholder' => 'Choose a location',
            ])
            ->add('mainApp', EntityType::class, [
               'class' => AppModule::class,
               'required' => true,
               'query_builder' => function (EntityRepository $er) {
                   return $er->createQueryBuilder('e')
                       ->orderBy('e.name', 'ASC');
               },
               'constraints' => [
                   new NotBlank([
                       'message' => 'Please choose a main app',
                   ]),
               ],
               'attr'=>['class'=>'span12 select2'],
               'choice_label' => 'name',
               'placeholder' => 'Choose a main application',
            ])
            ->add('syndicate', EntityType::class, [
                'class' => Syndicate::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please choose a business group',
                    ]),
                ],
                'attr'=>['class'=>'span12 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a business group',
            ])
            ->add('appModules', EntityType::class, [
                   'class' => AppModule::class,
                   'required'      => true,
                   'expanded'      =>true,
                   'multiple'      =>true,
                   'constraints' => [
                       new NotBlank([
                           'message' => 'Please select application module',
                       ]),
                   ],
                   'query_builder' => function (EntityRepository $er) {
                       return $er->createQueryBuilder('e')
                           ->andWhere("e.status = 1")
                           ->orderBy('e.name', 'ASC');
                   },
                   'attr'=>['class'=>''],
                   'choice_label' => 'name',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Terminal::class,
        ]);
    }

}