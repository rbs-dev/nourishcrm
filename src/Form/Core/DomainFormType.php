<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form\Core;



use App\Entity\Admin\Location;
use App\Entity\Admin\Terminal;
use App\Entity\Core\AccountInformation;
use App\Entity\Core\Customer;
use App\Entity\Core\Setting;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class DomainFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('vatSlave', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType', 'st')
                        ->where('st.slug =:slug')->setParameter("slug",'vat-slave')
                        ->orderBy('e.noteNo', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a economic activity',
            ])
            ->add('organizationName', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
             ->add('ownerName', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                'required' => flush()
            ])
            ->add('ownerMobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'mobileLocal'],
                'required' => true
            ])
            ->add('ownerDesignation', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('address', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('phone', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'phoneLocal'],
                 'required' => false
            ])
            ->add('binNo', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => false
            ])
            ->add('tinNo', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => false
            ])
            ->add('nid', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>''],
                 'required' => false
            ])
            ->add('displayMobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'mobileLocal'],
                 'required' => true
            ])
             ->add('email', TextType::class, [
                'attr' => ['autofocus' => true],
                 'required' => true
            ])
            ->add('signaturePerson', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'','placeholder'=>"Signature Person Name"],
                'required' => true
            ])
            ->add('signatureDesignation', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'','placeholder'=>"Signature Designation"],
                'required' => true
            ])
            ->add('signatureMobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'mobileLocal','placeholder'=>"Signature mobile no"],
                'required' => true
            ])
            ->add('signatureEmail', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'','placeholder'=>"Signature Email Address"],
                'required' => true
            ])
            ->add('signatureNid', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder'=>"Signature NID/Passport No"],
                'required' => true
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Terminal::class,
        ]);
    }
}
