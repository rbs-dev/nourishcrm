<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace App\Form\Core;


use App\Entity\Admin\Terminal;
use App\Entity\SalesDepot;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationFormType extends AbstractType
{

    /** @var  TranslatorInterface */

    public  $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('userId', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'col-md-12', 'placeholder' => 'Employee ID'],
                'required' => true,

            ])

            ->add('name', TextType::class, [
                'attr' => [
                    'autofocus' => true],
                'required' => true,
            ])

            ->add('email', EmailType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
            ])

            ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'mobileLocal'],
                'required' => true,
            ])

            ->add('depot', EntityType::class, array(
                'required'    => false,
                'class' => SalesDepot::class,
                'placeholder' => '- Select Depot -',
                'choice_label' => 'name',
                'attr'=>[
                    'class'=>'select2'
                ],
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        'max' => 20,
                    ]),
                ],
            ])

            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices'   => $options['userRepo']->getAccessRoleGroup($options['terminal'])
            ])

            ->add('enabled',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
                'label' => false
            ])

            ->add('SaveAndCreate', SubmitType::class)
        ;

     }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'terminal' => Terminal::class,
            'userRepo' => UserRepository::class,
//            'locationRepo' => LocationRepository::class,
        ]);
    }


}