<?php
/**
 * Created by PhpStorm.
 * User: Sohel
 * Date: 01/12/2022
 * Time: 3:30 PM
 */
namespace App\Form;


use App\Entity\Core\Setting;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeFilterFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

            $builder
                ->add('employee', EntityType::class, [
                'class' => User::class,
                'choice_label' => function($user){
                    return '(' . $user->getUserId() . ') ' . $user->getName();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.userGroup', 'userGroup')
                        ->andWhere("userGroup.slug = :slug")->setParameter('slug', 'employee')
                        ->orderBy('e.name', 'ASC');

                },
                'attr'=>[
                    'class'=>'select2'
                ],
                'placeholder' => '- Select Employee -',
                'required' => false,

            ])
                ->add('designation', EntityType::class,[
                'class' => Setting::class,
                'placeholder' => '- Select Designation -',
                'choice_label' => 'name',
                'query_builder' => function(EntityRepository $repository){
                    return $repository->createQueryBuilder('e')
                        ->join('e.settingType', 'settingType')
                        ->where("settingType.slug = 'designation'")
                        ->orderBy('e.name', 'ASC')
                        ;
                },
                'required' => false,
                'attr'=>[
                    'class'=>'select2'
                ],

            ])
                ->add('employeeMobile', TextType::class,[
                'attr' => [
                    'placeholder' => '- Mobile Number -'
                ],
                'required' => false
            ])
                ->add('lineManager', EntityType::class, array(
                'required'    => false,
                'class' => User::class,
                'placeholder' => '- Select Line Manager -',
                'choice_label' => function($lineManager){
                    /** @var User $lineManager */
                    return  '('. $lineManager->getUserId() .') ' . $lineManager->getName();
                },
                'attr'=>array('class'=>'span12 m-wrap select2'),
                'query_builder' => function(EntityRepository $er){
                    $qb = $er->createQueryBuilder('e');
                    $qb->where("e.enabled = 1")
                        ->andWhere('e.isDelete = 0')
                        ->andWhere($qb->expr()->orX(
                            $qb->expr()->like("e.roles", ':lineManager'),
                            $qb->expr()->like("e.roles", ':admin')
                        ))
                        ->setParameters([
                            'lineManager' => '%ROLE_LINE_MANAGER%',
                            'admin' => '%ROLE_KPI_ADMIN%'
                        ])
                        ->orderBy('e.name', 'ASC');
                    return $qb;
                },
            ))

/*            ->add('department', EntityType::class, [
                'class' => Setting::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','settingType')
                        ->where("settingType.slug =:slug")->setParameter('slug','department')
                        ->orderBy('e.name', 'ASC');

                },
                'attr'=>[
                    'class'=>'select2'
                ],
                'placeholder' => '- Select Department -',
                'required' => false,

            ])*/
                ->add('status', ChoiceType::class, [
                    'choices' => [
                        'Enable' => 'enable',
                        'Disable' => 'disable',
                    ],
                    'attr'=>[
                        'class'=>'select2'
                    ],
                    'placeholder' => '- Select Status -',
                    'required' => false,

                ])
                ->add('serviceMode', EntityType::class, array(
                'required'    => false,
                'class' => Setting::class,
                'placeholder' => '- Select Service Mode -',
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2 span12 m-wrap'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType","st")
                        ->where("st.slug ='service-mode'")
                        ->andWhere("e.status = 1")
                        ->orderBy('e.name', 'ASC');
                },
                ))
                ->add('kpiFormat', EntityType::class, [
                'class' => Setting::class,
                'choice_label' => 'name',
                'placeholder' => 'All Formats',
                'query_builder' => function(EntityRepository $repository){
                    return $repository->createQueryBuilder('e')
                        ->join('e.settingType', 'settingType')
                        ->where('settingType.slug = :slug')->setParameter('slug', 'report-mode');
                },
                'required' => false,
                'attr'=>[
                    'class'=>'select2'
                ],

                ])
                ->add('permanentStatus', ChoiceType::class, [
                'choices' => [
                    'Permanent' => 'yes',
                    'Not Permanent' => 'no',
                ],
                'placeholder' => '- Select Permanent Status -',
                'required' => false,

                ])
            ->setMethod('get')
        ;
     }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }


}