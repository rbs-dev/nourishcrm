<?php
/**
 * Created by PhpStorm.
 * User: hasan
 * Date: 9/8/19
 * Time: 4:43 PM
 */

namespace App\Form\Sms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Sms\SmsCampaign;

class SmsUploadFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('campaignName', TextType::class, [
                'attr' => [
                    'placeholder' => 'Campaign name'
                ]
            ])
            ->add('uploadFile', FileType::class, [
                'help' => 'Here you can upload only excel file with specific structure to upload SMS *Please check the image.',
                'mapped' => false,
                'required' => false
            ])
            ->add('mobileNumbers', TextareaType::class,[
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'placeholder' => '017********,019********',
                    'rows' => 1
                ],
                'help' => '* Add mobile numbers separated by comma(,)'
            ])
            ->add('message', TextareaType::class,[
                'required' => false,
                'attr' => [
                    'placeholder' => 'SMS Message'
                ],
                'help' => '* If excel found, this message will replace excel message.'
            ])
            ->add('Submit', SubmitType::class)
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SmsCampaign::class,
        ]);
    }


}